include os.mk

supported_targets=win64 linux64
ifeq (,$(filter $(target),$(supported_targets)))
$(error target $(target) is not supported (supported: $(supported_targets)))
endif

ifndef V
V=0
endif

ifeq ($(V),0)
Q=@
else
$(info building target-arch: $(target))
endif

### PYTHON ###
CYTHON_BUILD_EXT=setup.py build_ext --inplace
# removed --compiler=msvc -f
#
GUI_PATH=simulator/gui
GUI_BUILDER = update_gui.py

### variables ###
# might need some tweaking
ifeq ($(target),win64)
PYTHON_PATH="%HOMEDRIVE%%HOMEPATH%\\Anaconda3"
#PYTHON_PATH="d:\\Programmieren\\Anaconda3"
PYTHON_ACTIVATE="Scripts\\activate.bat"
PYTHON_ENVIRONMENT=DGA3.7
PYTHON=python
VCVARSALL_PATH="C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\VC\\vcvarsall.bat"
#VCVARSALL_PATH="D:\\Programmieren\\Microsoft Visual Studio 14.0\\VC\\vcvarsall.bat"
CYTHON_BUILD_EXT_CMD=cmd.exe //u //q //s //c build_cython.cmd 

else ifeq ($(target),linux64)
PYTHON_PATH=$(HOME)/anaconda3/bin
EXPORT=. $(HOME)/anaconda3/etc/profile.d/conda.sh
PYTHON_ACTIVATE=$(EXPORT) && conda activate 
PYTHON_ENVIRONMENT=DGA3.7
PYTHON=python
endif
######################

#ifeq ($V,0)
#CYTHON_BUILD_EXT += --quiet
#else
#CYTHON_BUILD_EXT += --verbose
#endif

### path to DGA fit library
DGA_FIT_FOLDER=DGA_fit_library
#path to cython module
PLS2_FOLDER=simulator/pls2
LC3_FOLDER=LaserController3-C/LaserController3-Cython

# actual make stuff..

.PHONY: clean all echo print fit_library cython


all :  fit_library cython gui

fit_library :
	$(info building fit_library)
ifeq ($(target),win64)
	$(info tbd for win64)
else ifeq ($(target),linux64)
	$(MAKE) -C $(DGA_FIT_FOLDER) target=$(target) V=$V
endif

ifeq ($(target),win64)
cython:
	$(info building cython)
	$(CYTHON_BUILD_EXT_CMD) $(LC3_FOLDER) $(PYTHON_PATH) $(PYTHON_ACTIVATE) $(PYTHON_ENVIRONMENT) $(PYTHON) $(VCVARSALL_PATH) $(CYTHON_BUILD_EXT) 
	$(CYTHON_BUILD_EXT_CMD) $(LC3_FOLDER) $(PYTHON_PATH) $(PYTHON_ACTIVATE) $(PYTHON_ENVIRONMENT) $(PYTHON) $(VCVARSALL_PATH) copy_pyd.py
	$(CYTHON_BUILD_EXT_CMD) $(PLS2_FOLDER) $(PYTHON_PATH) $(PYTHON_ACTIVATE) $(PYTHON_ENVIRONMENT) $(PYTHON) $(VCVARSALL_PATH) $(CYTHON_BUILD_EXT) 
else ifeq ($(target),linux64)
cython:
	$(info building cython)
	cd $(LC3_FOLDER)  && $(PYTHON_ACTIVATE) $(PYTHON_ENVIRONMENT) && $(PYTHON) $(CYTHON_BUILD_EXT) && $(PYTHON) copy_pyd.py
	cd $(PLS2_FOLDER) && $(PYTHON_ACTIVATE) $(PYTHON_ENVIRONMENT) && $(PYTHON) $(CYTHON_BUILD_EXT)
endif

gui: 
ifeq ($(target),win64)
	$(CYTHON_BUILD_EXT_CMD) $(GUI_PATH) $(PYTHON_PATH) $(PYTHON_ACTIVATE) $(PYTHON_ENVIRONMENT) $(PYTHON) $(VCVARSALL_PATH) $(GUI_BUILDER)
else ifeq ($(target),linux64)
	cd $(GUI_PATH) && $(PYTHON_ACTIVATE) $(PYTHON_ENVIRONMENT) && $(PYTHON) $(GUI_BUILDER)
endif

clean :
ifeq ($(target),win64)
	$(info tbd for win64)
else
	@echo 'cleaning DGA_fit_library'
	$(Q)$(MAKE) -C $(DGA_FIT_FOLDER) target=$(target) clean
endif
	@echo 'cleaning PLS2-Cython'
#	$(Q)rm -f $(PLS2_FOLDER)/pls2_wrapper*.pyd
#	$(Q)rm -f $(PLS2_FOLDER)/pls2_wrapper*.so
	$(Q)rm -f $(PLS2_FOLDER)/pls2_wrapper.cpp
	$(Q)rm -rf $(PLS2_FOLDER)/build
	@echo 'cleaning LC3-Cython'
#	$(Q)rm -f $(LC3_FOLDER)/LaserController3*.pyd
#	$(Q)rm -f $(LC3_FOLDER)/LaserController3*.so
	$(Q)rm -f $(LC3_FOLDER)/LaserController3.c
	$(Q)rm -rf $(LC3_FOLDER)/build

help :
	@echo 'V=1     : verbose'

print: 
	$(info $$DGA_FIT_FOLDER is [${DGA_FIT_FOLDER}])
