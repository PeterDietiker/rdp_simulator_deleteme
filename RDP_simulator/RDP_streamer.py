'''
This package provides a streamer class for RDP objects

It allows for loading of raw data from RDPLogs via the RDP class
Created on 13 Nov 2020

@author: P.Dietiker
'''

import os
import pickle
import logging
import numpy as np
import sys
sys.path.insert(1, os.path.join(os.path.dirname(__file__), os.path.pardir,"Grifols"))
from Grifols.RDP import RDP



class rdp_streamer(object):

    def __init__(self, file_path=None, logger=None):
        '''
        initialises the streamer
        :param file_path: file path
        :type file_path: string
        :param logger: Logger to use
        :type logger: logging.logger
        '''
        self.file_path = file_path
        self.rdp = None  # file handle

        # init handle if file path is given
        self.set_path(file_path)

        # init logger
        if logger is None:
            self.logger = logging.getLogger(__name__)
            self.logger.handlers = []  # clear all handlers
            self.logger.setLevel(logging.DEBUG)
            logging_handler = logging.StreamHandler()
            logging_handler.setLevel(logging.DEBUG)
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            logging_handler.setFormatter(formatter)
            self.logger.addHandler(logging_handler)
        else:
            self.logger = logger

    def set_path(self, file_path=None):
        '''
        Set the path to the file and initialises the RDP class
        :param file_path: path to file
        :type file_path: string
        '''

        if self.rdp is not None:
            del self.rdp
            self.rdp = None
        if file_path is not None:
            self.file_path = os.path.abspath(file_path)
            dirname = os.path.dirname(self.file_path)
            filename =os.path.basename(self.file_path)
            # check for existence of folder
            if not os.path.exists(dirname):
                self.logger.warning('Not able to open file')
                return
            # open the file
            try:
                self.rdp = RDP(dirname,filename)
                self.wells = iter(self.rdp.labels)
                
            except:
                self.rdp = None
                self.logger.warning('Not able to open file')
                return

        else:
            self.file_path = None
            self.rdp = None


    def load(self, well = None):
        '''
        returns raw data of the next well if no well is givven
        If a vial is given, it loads this well
        
        :param vial: vial to load
        :type vial: str
        '''
        if self.rdp is not None:
            try:
                if well is None:
                    self.actual_well = next(self.wells)
                else:
                    self.actual_well = well
                self.actual_data = self.rdp.data[self.actual_well][self.rdp.channel]
                data = np.vstack((self.actual_data.index,self.actual_data["Count_raw"].values))
                try:
                    self.actual_concentration = self.rdp.results.loc[self.actual_well,f"Conc_{self.rdp.channel}"]
                except KeyError:
                    self.actual_concentration = np.nan
            except StopIteration:
                self.actual_data = 0
                data = 0
                self.actual_concentration = 0
                self.logger.info('End of file reached')
                raise
        else:
            self.logger.warning('No file selected')
            self.actual_data = 0
            data = 0
            self.actual_concentration = 0
        return data, self.actual_concentration

    def reset(self):
        '''
        resets the well iterator
        '''
        if self.rdp is not None:
            self.wells = iter(self.rdp.labels)
            return
        else:
            self.logger.warning('No file selected')
            return

    def __del__(self):
        if self.rdp is not None:
            del self.rdp
            self.rdp = None
