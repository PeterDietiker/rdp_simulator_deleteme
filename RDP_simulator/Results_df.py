'''
Created on 14.03.2020

@author: peterdietiker
'''

import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import numpy as np
import statsmodels.formula.api as sm
import logging

import RDP_simulator.helper_functions as hf


class results_df():
    '''
    Class for storing the results of the simulations. The nr of events together with the parameters are stored in a pandas data frame.
    '''
        
    def __init__(self, sig_sim_instance):
        self.sig_sim = sig_sim_instance
        self.logger = self.sig_sim.logger
        self.vars_dict = vars(self.sig_sim)
        self.ols = None
        self.results_dict = dict()
        self.generate_df()
    
    def __str__(self):
        return self.results_df.to_string()
        
    def _get_result(self):
        '''
        his function gathers all paramters and the number of events and returns it as a pandas dataframe
        '''
        all_parameter_dicts = [d for d in self.vars_dict if '_dict' in d]
        all_parameters =  {}
        for d in all_parameter_dicts:
            all_parameters.update(self.vars_dict[d])
        for key, value in all_parameters.items():
            all_parameters[key] = [value]
        #remove all callables
        for key, value in all_parameters.items():
            if callable(value):
                del all_parameters[key]
        
        result = pd.DataFrame.from_dict(all_parameters)
        result['nr_events'] = self.sig_sim.nr_events_buffer.mean
        return result
    
    def generate_df(self):
        '''
        Initialises the data frame
        '''
        self.results_df = self._get_result()
        self.results_df['plot_color'] = "blue"
        self.results_df = self.results_df.drop(0)
    
    def reset_df(self):
        '''
        resets the data frame
        '''
        self.generate_df()
    
    def add_result(self, **args):
        '''
        Adds a new result to the data frame

        :param ***args: any additional variables to add to the result
        '''
        new_data = self._get_result()
        for key, value in args.items():
            new_data[key] = value
        self.results_df = self.results_df.append(new_data, ignore_index=True, sort=False)
        
    def save(self,path):
        '''
        Saves the data frame as csv
        :param path: path to file
        '''
        self.results_df.to_csv(path)
        
    def calculate_regression(self,x_axis = 'index'):
        '''
        calculates a linear regression of the events vs. the given parameter
        @param x_axis : independent paramter for the regression
        '''
        if x_axis != 'index':
            if x_axis not in self.results_df.columns:
                self.logger.warning("Paramter "+x_axis+" not in results dataframe")
                return
        if x_axis == 'index':
            self.ols = sm.ols(formula =  "nr_events ~ "+ x_axis,data = self.results_df.reset_index()).fit()
        else:
            self.ols = sm.ols(formula =  "nr_events ~ "+ x_axis,data = self.results_df).fit()
        self.results_dict["slope"] = self.ols.params[1]
        self.results_dict["intercept"] = self.ols.params[1]
        if x_axis == 'conc':
            try:
                self.results_dict["LOD"] = 3*self.results_df.loc[self.results_df['conc']==0,'nr_events'].std()/self.ols.params[1]
            except:
                self.results_dict["LOD"] = None
        else:
            self.results_dict["LOD"] = None
                   
        

    def plot_results(self, ax=None, x_axis = 'index', init=False,**kwargs):
        '''plots the nr of events as a function of the given x_axis.
        @param ax: axis to plot the curve. If 'None' a new fig is generated
        @param x_axis: Prameter to plots the nr of events against. If None, the results rae plottet in chronologically order
        @param init: True if this is the first plot
        

        '''
        '''
        #old stuff for matplotlib plot
        if x_axis == 'index':
            x_array = self.results_df.index
            
        else:
            x_array = self.results_df[x_axis]
            
        y_array = self.results_df['nr_events']
        
        color_array = self.results_df['plot_color']
        rgb_color_array = np.array([mpl.colors.to_rgb(color) for color in color_array])

        x_axis_factor = 1
        if x_axis == 'conc':
            x_axis_factor = 1E15
        elif x_axis == 'power':
            x_axis_factor = 1E3
            
        if ax is None:
            fig, ax = plt.subplots(1)
            init = True

        if init:
            
            ax.scatter(x_array*x_axis_factor, y_array, color = rgb_color_array, label='nr_events')
            ax.set_xlabel(x_axis)
            ax.set_ylabel('# events')
            # ax.legend(loc=0)
            ax.figure.tight_layout()
        else:
            ax.lines[0].set_xdata(x_array*x_axis_factor)
            ax.lines[0].set_ydata(y_array)
            ax.lines[0].set_color(rgb_color_array)
    
        '''
        if ax is None:
            fig, ax = plt.subplots(1)
        
        x_axis_factor = 1
        if x_axis == 'conc':
            x_axis_factor = 1E15
        elif x_axis == 'power':
            x_axis_factor = 1E3
            
        plot_df = self.results_df.copy().reset_index()
        plot_color = kwargs.pop('plot_color', '')
            
        plot_df[x_axis] = plot_df[x_axis]*x_axis_factor
        if plot_color != '':
            color_labels = plot_df[plot_color].unique()
            # List of RGB triplets
            n_items = color_labels.shape[0]
            color_list = [mpl.colors.to_rgb(c) for c in color_labels]
            # Map label to RGB
            color_map = dict(zip(color_labels, color_list))
            plot_df.plot(x = x_axis, y='nr_events',kind='scatter',c = plot_df[plot_color].map(color_map),**kwargs,ax=ax)
        else:
            plot_df.plot(x = x_axis, y='nr_events',kind='scatter',**kwargs,ax=ax)
            
    
            hf.set_plot_limits(ax, 'x')
            hf.set_plot_limits(ax, 'y')
            
            try: 
                self.calculate_regression(x_axis)
            except:
                pass
            else:
                if self.ols.rsquared >0.9:
                    x_range = np.linspace(plot_df[x_axis].min(),plot_df[x_axis].max(),20)
                    ax.plot(x_range*x_axis_factor,self.ols.predict(pd.DataFrame({x_axis:x_range})),color="red")
                    max_pos = ax.get_ylim()[0]+np.linspace(0.7,0.9,3)*np.array(ax.get_ylim()).ptp()
                    text_x_pos = ax.get_xlim()[0] + (ax.get_xlim()[1]-ax.get_xlim()[0])*0.1
                    ax.text(text_x_pos,max_pos[0],"R: "+str(round(self.ols.rsquared,3)))
                    ax.text(text_x_pos,max_pos[1],f"Slope: {self.results_dict['slope']/x_axis_factor:.3}")
                    if self.results_dict['LOD'] is not None:
                        ax.text(text_x_pos,max_pos[2],f"LOD: {self.results_dict['LOD']*x_axis_factor:.3}")
            finally:
                return ax.figure, ax
    
    