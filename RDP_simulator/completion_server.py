#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Server script for autocompletion

Main server script for a pyqode.python backend. You can directly use this
script in your application if it fits your needs or use it as a starting point
for writing your own server.

::

    usage: server.py [-h] [-s [SYSPATH [SYSPATH ...]]] port

    positional arguments:
      port                  the local tcp port to use to run the server

    optional arguments:
      -h, --help            show this help message and exit
      -s [SYSPATH [SYSPATH ...]], --syspath [SYSPATH [SYSPATH ...]]

"""
import argparse
import logging
import sys
import os

class ListWordsProvider(object):
    """
    Provides completions based on a given list of words
    """
    def __init__(self,word_list):
        
        self.word_set = set()
        for word in word_list:
            self.word_set.add(word)
        self.word_set = sorted(self.word_set)
        

    def complete(self, code, line, column, path, encoding, prefix):
        """
        Provides completions based on a given list of words

        :param code: code to complete
        :param args: additional (unused) arguments.
        """
        completions = []
        #hf.pydevBrk()
        for word in self.word_set:
            if word.startswith(prefix):
                completions.append({'name': word})
        return completions

if __name__ == '__main__':
    """
    Server process' entry point
    """
    logging.basicConfig()
    # setup argument parser and parse command line args
    parser = argparse.ArgumentParser()
    parser.add_argument("port", help="the local tcp port to use to run "
                        "the server")
    parser.add_argument("word_list_path", nargs="?", default=None,help="path to word list to add to the autocompleter "
                        "the server")
    parser.add_argument('-s', '--syspath', nargs='*')
    args = parser.parse_args()

    # add user paths to sys.path
    if args.syspath:
        for path in args.syspath:
            print('append path %s to sys.path' % path)
            sys.path.append(path)
            
    from pyqode.core import backend
    from pyqode.python.backend.workers import JediCompletionProvider
    
    print("completion_server pid: {:d}:".format(os.getpid()))
    
    try:
        import RDP_simulator.helper_functions  as hf
    except:
        pass
    else:
        #hf.pydevBrk()
        pass
    # setup completion providers
    #standard jedi provider
    backend.CodeCompletionWorker.providers.append(JediCompletionProvider())
    #provider based on the words in the document
    backend.CodeCompletionWorker.providers.append(
        backend.DocumentWordsProvider())
    #provider based in word list 
    if args.word_list_path:
        with open(args.word_list_path) as f:
            word_list = f.read().splitlines()
        
        backend.CodeCompletionWorker.providers.insert(0,
            ListWordsProvider(word_list))

    # starts the server
    backend.serve_forever(args)


