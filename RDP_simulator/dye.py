'''
Created on 21.01.2021

@author: peterdietiker
'''
from pathlib import Path
import pandas as pd
import logging
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate


class Dye(object):
    '''
    classdocs
    '''


    def __init__(self, name = None, data_path = None,logger = None):
        '''
        Constructor
        '''
        self.name = name
        self.file_path = Path(__file__).parent
        default_path = Path(r"V:\Projects\71045-01_Grifols_Feasbility build\200_Engineering\220_Optic_Engineering\Documents")
        self.data_path = default_path if data_path is None else Path(data_path)
        
        self.get_logger(logger)
        self.get_available_dyes()
        self.set_dye(name)
        
     
    def set_dye(self, name,epsilon = 1E5, lifetime = 1E-9, lifetime_isc = 1E-6, lifetime_triplet = 1E-6, quantum_efficiency = 1):   
        '''
        If name is a valid dye name, the corresponding paramters are loaded. If name is "manual" the given parameters are used. 
        :param name: Name of the Dye. either a valid name or "manual"
        :type name: str
        :param epsilon: absorbance in "$\mathrm{L}\,\mathrm{mol}^1\,\mathrm{cm}^1$"
        :type epsilon: float
        :param lifetime: lifetime of excited state in s
        :type lifetime: float
        :param lifetime_isc: inverse of intersystem crossing rate in s
        :type lifetime_isc: float
        :param lifetime_triplet: lifetime of triplet state in s
        :type lifetime_triplet: float
        :param quantum_efficiency: quantum efficiency
        :type quantum_efficiency: float
        '''
        if name is None:
            self.name = None
            self.data = {key:0 for key in self.all_dye_data.columns}
            self.absorbance = None
            self.emission = None
            def calc_emission_rate(*args):
                return 0
            self.calc_emission_rate = calc_emission_rate
        elif name == "manual":
            self.data = {"epsilon":epsilon, "lifetime" : lifetime, "lifetime_isc" : lifetime_isc, "lifetime_triplet" : lifetime_triplet, "quantum_efficiency":quantum_efficiency}
            self.absorbance = None
            self.emission = None
            self.calc_emission_rate = self._calc_emission_rate
        elif name not in self.all_dye_data.index:
            self.name = None
            self.logger.exception(f"Data for Dye {self.name} not available. Please add dye to 'Dye_data.csv'")
            self.data = {key:0 for key in self.all_dye_data.columns}
            self.absorbance = None
            self.emission = None
            def calc_emission_rate(*args):
                return 0 
            self.calc_emission_rate = calc_emission_rate
        else:
            self.name = name
            self.data = self.all_dye_data.loc[name]
            
            self.load_absorbance()
            self.load_emission() 
        
            #vectorized emission rate function
            #self.calc_emission_rate = np.vectorize(self._calc_emission_rate,excluded='self',signature= )
            self.calc_emission_rate = self._calc_emission_rate
        
        
    def get_logger(self,logger):
        # init logger
        if logger is None:
            self.logger = logging.getLogger(__name__)
            self.logger.handlers = []  # clear all handlers
            self.logger.setLevel(logging.DEBUG)
            logging_handler = logging.StreamHandler()
            logging_handler.setLevel(logging.DEBUG)
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            logging_handler.setFormatter(formatter)
            self.logger.addHandler(logging_handler)
        else:
            self.logger = logger
    
    
            
    def get_available_dyes(self):
        dye_dict_path = self.data_path / "Dye_data.csv"
        try:
            self.all_dye_data = pd.read_csv(dye_dict_path, sep=";", index_col = "dye", usecols = np.arange(6))
        except:
            self.logger.error(f"Cannot read dye data under path: {dye_dict_path}")
            columns = ["epsilon", "lifetime" ,"lifetime_isc" , "lifetime_triplet" , "quantum_efficiency"]
            self.all_dye_data = pd.DataFrame(columns = columns)
        
    def load_absorbance(self,path = None):
        '''
        This loads tha absorbance spectrum of the dye
        The spectrum shall be normalised to a maximum of 1. 
        '''
        if path is None:
            fn = self.data_path / (self.name+" - Abs.txt")
        else:
            fn = path
        try:
            self.absorbance = pd.read_csv(fn, sep="\t", header=None, names=["wl","abs"],comment='#', skiprows = 6).set_index("wl")
        except Exception:
            self.logger.error("Not able to load absorbance spectrum", exc_info=True)
        
        self.set_color()
        self.absorbance_interpolator = scipy.interpolate.interp1d(self.absorbance.index,self.absorbance['abs'],kind = "quadratic",fill_value=0,bounds_error=False)
    
    def load_emission(self,path = None):
        '''
        This loads tha emission spectrum of the dye
        The spectrum shall be normalised to a maximum of 1. 
        '''
        if path is None:
            fn = self.data_path / (self.name+" - Em.txt")
        else:
            fn = path
        try:
            self.emission = pd.read_csv(fn, sep="\t", header=None, names=["wl","em"],comment='#',skiprows = 6).set_index("wl")
        except Exception:
            self.logger.error("Not able to load emission spectrum", exc_info=True)
            
    
    def _calc_emission_rate(self,I, wavelength = None):
        '''
        This function calculates the theoretical emission rate of a fluorophore. 
        According to https://doi.org/10.3929/ethz-a-004321653 (page 9)
        Parameter:
        ----------
        :param I: Laser Leistung in W/m^2
        :param wavelength: Excitation wavelength in 
        

        Return:
        --------
        R: Photon emission rate 1/s
        '''
        if not self.absorbance is None:
            #interpolate epsilon if absorbance spectrum is available
            wavelength = self.absorbance['abs'].idxmax() if wavelength is None else wavelength
            epsilon = self.data["epsilon"]*self.absorbance_interpolator(wavelength)
        else:
            epsilon = self.data["epsilon"]
        if epsilon <= 0:
            return 0
        tau = self.data["lifetime"]
        tau_isc = self.data["lifetime_isc"]
        tau_t = self.data["lifetime_triplet"]
        quantum_efficiency = self.data["quantum_efficiency"]
        h = 6.6E-34
        c = 299792458 
        Na = 6.022E23
        E_photon = h*c/(wavelength*1E-9)
        n_photon = I/E_photon #Umrechnung der Leistung nach n/m^2
        sigma = epsilon*3.825E-21 #umrechung nach Tkachenk02006a,Seite 5 ist um Faktor 100 falsch!!!
        sigma = sigma*1E-4 #umrechnung vom cm^2 nach m^2
        #sigma = 1E-9*(1E-6)**2
        #print(sigma)
        k_r = 1/tau #Übergangsrate für fluoreszenzübergang
        k_ISC =1/tau_isc #Übergangsrate in triplet state
        k_T =  1/tau_t #Übergangsrate von Tripletstate in Grundzustand
        R_inf = k_r/(1+(k_ISC/k_T))
        n_s = R_inf/sigma*(1+k_ISC/k_r)
        R = quantum_efficiency*R_inf/(1+n_s/n_photon)
        return R
    
          
    
    def plot(self,ax = None, y = 'both', units_abs = None, scale_em = False):
        if ax is None:
            fig, ax = plt.subplots(1)
            new_plot = True
        else:
            fig = ax.figure
            new_plot = False
        
        ax_absorbance = ax
        ax_emission = ax   
        
        scale_factor_abs = 1
        unit_abs = "a.u."
        if units_abs == 'absorbance':
            scale_factor_abs = self.data.epsilon
            unit_abs = "$\mathrm{L}\,\mathrm{mol}^1\,\mathrm{cm}^1$"
            if new_plot:
                ax_absorbance = ax.twinx()
        
        scale_factor_em = 1
        unit_em = "a.u."
        if scale_em == True:
            scale_factor_em = self.data.quantum_efficiency
            unit_em = "a.u."
            
        
                
            
        if y in ["absobance", "both"]:
            ax_absorbance.plot(self.absorbance.index, self.absorbance['abs']*scale_factor_abs, color = self.color,label = "emission")
            ax_absorbance.set_ylabel(unit_abs)
        if y in ["emission", "both"]:
            ax_emission.fill(self.emission.index, self.emission['em']*scale_factor_em, facecolor = self.color,alpha=0.5,label = "emission")
            ax_emission.set_ylabel(unit_em)
        
        ax.set_xlabel("Wavelength / nm")
        plt.tight_layout()
        return fig
    
    def set_color(self, color = None):
        '''
        Set the color used to plot spectra. 
        Of color is None, the color is calculated based on the absorbance spectrum
        :param color: None or any format recognized by matplotlib. See https://matplotlib.org/tutorials/colors/colors.html
        '''
        if color is None:
            try:
                self.color = self.calculate_color(self.absorbance['abs'].idxmax())
            except: #no absorbance spectrum loaded
                self.logger.info
                self.color = "blue"
        else:
            self.color = color
    
    def calculate_color(self,wavelength):
        '''
        Calculates a ploting color based on absorbance maximum of the Dye. 
        Based on this; https://academo.org/demos/wavelength-to-colour-relationship/ 
        :param wavelength: Wavelength in nm
        :type wavelength: double
        '''
        Gamma = 0.80
        IntensityMax = 1
        if (wavelength >= 380) and (wavelength<440):
            red = -(wavelength - 440) / (440 - 380)
            green = 0.0
            blue = 1.0
        elif (wavelength >= 440) and (wavelength<490):
            red = 0.0
            green = (wavelength - 440) / (490 - 440)
            blue = 1.0
        elif (wavelength >= 490) and (wavelength<510):
            red = 0.0
            green = 1.0
            blue = -(wavelength - 510) / (510 - 490)
        elif (wavelength >= 510) and (wavelength<580):
            red = (wavelength - 510) / (580 - 510)
            green = 1.0
            blue = 0.0
        elif (wavelength >= 580) and (wavelength<645):
            red = 1.0
            green = -(wavelength - 645) / (645 - 580)
            blue = 0.0
        elif (wavelength >= 645) and (wavelength<781):
            red = 1.0
            green = 0.0
            blue = 0.0
        else:
            red = 0.0
            green = 0.0
            blue = 0.0
        # Let the intensity fall off near the vision limits
        if (wavelength >= 380) and (wavelength<420):
            factor = 0.3 + 0.7*(wavelength - 380) / (420 - 380)
        elif (wavelength >= 420) and (wavelength<701):
            factor = 1.0
        elif (wavelength >= 701) and (wavelength<781):
            factor = 0.3 + 0.7*(780 - wavelength) / (780 - 700)
        else:
            factor = 0.0
            
        if (red != 0):
            red = IntensityMax * (red * factor)**Gamma

        if (green != 0):
            green = IntensityMax * (green * factor)**Gamma
 
        if (blue != 0):
            blue = IntensityMax * (blue * factor)**Gamma

        return (red,green,blue)
    
if __name__ == '__main__':
    import matplotlib as mpl
    mpl.interactive(False)
    #dye = Dye("Alexa Fluor 647",data_path = r"V:\Projects\71045-01_Grifols_Feasbility build\200_Engineering\220_Optic_Engineering\Documents")    
    #print(dye.absorbance)
    #fig = dye.plot(units_abs = "absorbance",scale_em = True)
    #plt.show()
    dye =  Dye(name = "Alexa Fluor 647",data_path = r"V:\Projects\71045-01_Grifols_Feasbility build\200_Engineering\220_Optic_Engineering\Documents")   
    print(dye.calc_emission_rate(1))
    #print(dye.absorbance_interpolator(1))
    