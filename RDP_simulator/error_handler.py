'''
Created on 19.05.2017

@author: pdietiker
'''

import sys
from pyqode.qt import QtCore, QtWidgets, QtGui
import logging

class error_handler(object):
    '''
    This class provides an error handler based on logging. It redirects logs to a dialog or a text widget dependent on the instance
    of the parent_widget argument. 
    '''
    def __init__(self, parent_widget=None, name = None, level = 'DEBUG'):  
        '''
        :param parent_widget: text widget where the log is rederec to. If not a text widget, a dialog is used to show log
        :type parent_widget: (QtWidgets.QTextEdit,QtWidgets.QPlainTextEdit,QtWidgets.QTextBrowser) or QtWidgets.QMainWindow 
        :param name: name of the logger to use
        :type name: string
        :param level: logging level
        :type level: integer
        '''
        self.level = level
        #Use a dialog if parent_widget is not a text editing widget. 
        self.use_dialog = not isinstance(parent_widget,(QtWidgets.QTextEdit,QtWidgets.QPlainTextEdit,QtWidgets.QTextBrowser))
        if self.use_dialog:
            self.error_widget = error_dialog(parent_widget)
        else:
            self.error_widget = error_qtextedit(parent_widget)
        if name is None:
            name = __name__
        self.logger = logging.getLogger(name)
        self.handler = QtHandler(self.error_widget)
        self.handler.setFormatter(logging.Formatter("%(levelname)s: %(message)s"))
        self.logger.addHandler(self.handler)
        self.logger.setLevel(self.level)
    
    def write(self,buf):  
        '''
        function to redirect sys.stdout and sys.stderr
        I dont know why, but if I uncomment the followig lines, the program crashes from time to time due to stack overflow
        :param buf: 
        :type buf:
        '''
        #for line in buf.rstrip().splitlines():
           #self.logger.log(self.level, line.rstrip())
            
        if buf and not buf.isspace():
            self.logger.log(self.level, buf.rstrip())
            
            
    
    def flush(self):
        '''
        for compatibiity with sys.stdout
        '''
        pass

class error_qtextedit(object):
    '''
    Class to redirect log to qtextedit
    '''
    def __init__(self,qtextedit_widget):
        '''
        :param qtextedit_widget: text widget
        :type qtextedit_widget: QtWidgets.QTextEdit,QtWidgets.QPlainTextEdit or QtWidgets.QTextBrowser
        '''
        self.widget = qtextedit_widget
    
    def set_message(self,message,color):
        tc = self.widget.textColor()
        self.widget.moveCursor(QtGui.QTextCursor.End)
        self.widget.setTextColor(color)
        self.widget.insertPlainText(message)
        self.widget.setTextColor(tc)
    
    def exec_(self):
        pass

class error_dialog(QtWidgets.QDialog):
    '''
    Class to redirect log to dialog
    '''
    #Dialog, der aufgeht bei error
    def __init__(self,parent=None):
        super(error_dialog, self).__init__(parent)

        self.buttonBox = QtWidgets.QDialogButtonBox(self)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Ok)

        self.textBrowser = QtWidgets.QTextBrowser(self)

        self.verticalLayout = QtWidgets.QVBoxLayout(self)
        self.verticalLayout.addWidget(self.textBrowser)
        self.verticalLayout.addWidget(self.buttonBox)
        self.buttonBox.accepted.connect(self.close)
        
    def set_message(self,message,color):
        self.textBrowser.clear()
        self.textBrowser.append(message)
        




class QtHandler(logging.Handler):
    '''
    Handler for log
    '''
    #logging handler. oeffnet den error dialog
    def __init__(self, new_error_dialog):
        logging.Handler.__init__(self)
        self.error_dialog=new_error_dialog
        self.color = QtCore.Qt.black
        self.error_color= QtCore.Qt.red
        
    def emit(self, record):
        record = self.format(record)
        if record.startswith("ERROR"):
            color=self.error_color
        else:
            color=self.color
        #if record: XStream.stdout().write('%s\n'%record)
        self.error_dialog.set_message('%s\n'%record,color)
        self.error_dialog.exec_()
        # originally: XStream.stdout().write("{}\n".format(record))