'''
Created on 19.05.2017

@author: pdietiker
'''


if __name__ == '__main__':
    # matplotlib and pyqtpgraph integration example by Vasilije Mehandzic
# based on embedding_in_qt4.py --- Simple Qt4 application embedding matplotlib canvases
# http://matplotlib.org/examples/user_interfaces/embedding_in_qt4.html
    import sys
    import os
    import pyqtgraph
    import matplotlib
    import numpy

    
    from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
    
    class MyMplCanvas(FigureCanvas):
        def __init__(self, parent=None, width=5, height=4, dpi=100):
            fig = matplotlib.figure.Figure(figsize=(width, height), dpi=dpi)
            self.axes = fig.add_subplot(111)
    
            self.compute_initial_figure()  
    
            FigureCanvas.__init__(self, fig)
            self.setParent(parent)
    
            FigureCanvas.setSizePolicy(self, pyqtgraph.Qt.QtWidgets.QSizePolicy.Expanding, pyqtgraph.Qt.QtWidgets.QSizePolicy.Expanding)
            FigureCanvas.updateGeometry(self)
    
        def compute_initial_figure(self):
            pass
    
    
    class MyStaticMplCanvas(MyMplCanvas):
        def compute_initial_figure(self):
            t = numpy.arange(0.0, 3.0, 0.01)
            s = numpy.sin(2*numpy.pi*t)
            self.axes.plot(t, s)
    
    
    class MyDynamicMplCanvas(MyMplCanvas):
        def __init__(self, *args, **kwargs):
            MyMplCanvas.__init__(self, *args, **kwargs)
            self.b1_clicked = False
            self.b2_clicked = False
            self.maxn = 10
            timer = pyqtgraph.Qt.QtCore.QTimer(self)
            timer.timeout.connect(self.update_figure)
            timer.start(100)
    
        def compute_initial_figure(self):
            self.axes.plot([0, 1, 2, 3], [1, 2, 0, 4], 'r')
    
        def update_figure(self):        
            l = [numpy.random.randint(0, self.maxn) for i in range(self.maxn)]
            self.axes.cla()
            self.axes.plot(list(range(self.maxn)), l, 'r')
            self.draw()
            if self.b1_clicked:
                self.b1_clicked = False
                if self.maxn>2: self.maxn -= 1
            if self.b2_clicked:
                self.b2_clicked = False
                self.maxn += 1    
        
    
    class ApplicationWindow(pyqtgraph.Qt.QtGui.QMainWindow):
        def __init__(self):
            pyqtgraph.Qt.QtGui.QMainWindow.__init__(self)
            self.setWindowTitle('matplotlib and pyqtpgraph integration example')
            
            self.main_widget = pyqtgraph.Qt.QtGui.QWidget(self)
            self.layout = pyqtgraph.Qt.QtGui.QVBoxLayout(self.main_widget)
            self.static_matplotlib_canvas = MyStaticMplCanvas(self.main_widget, width=5, height=4, dpi=100)
            self.dynamic_matplotlib_canvas = MyDynamicMplCanvas(self.main_widget, width=5, height=4, dpi=100)
                 
            self.glw = pyqtgraph.LayoutWidget()
            self.b1_b = pyqtgraph.Qt.QtWidgets.QPushButton('-')
            self.b1_b.clicked.connect(self.b1_clicked)     
            self.b2_b = pyqtgraph.Qt.QtWidgets.QPushButton('+')
            self.b2_b.clicked.connect(self.b2_clicked)
            self.glw1 = self.glw.addWidget(self.b1_b, row=0, col=0)        
            self.glw1 = self.glw.addWidget(self.b2_b, row=0, col=1)        
    
            self.plw = pyqtgraph.PlotWidget()     
            self.p1 = self.plw.plot()        
            self.data1 = numpy.random.random(shape = 300)        
            self.p1.setData(self.data1) 
    
            self.layout.addWidget(self.static_matplotlib_canvas)
            self.layout.addWidget(self.dynamic_matplotlib_canvas)        
            self.layout.addWidget(self.glw)
            self.layout.addWidget(self.plw)
    
            self.main_widget.setFocus()
            self.setCentralWidget(self.main_widget)
            
            self.timer = pyqtgraph.Qt.QtCore.QTimer()
            self.timer.timeout.connect(self.update)
            self.timer.start(0.1)   
    
        def update(self):
            self.data1[:-1] = self.data1[1:]        
            self.data1[-1] = numpy.random.normal()
            self.p1.setData(self.data1)
    
        def b1_clicked(self):
            self.dynamic_matplotlib_canvas.b1_clicked = True        
            
        def b2_clicked(self):
            self.dynamic_matplotlib_canvas.b2_clicked = True
            
    qApp = pyqtgraph.Qt.QtGui.QApplication(sys.argv)
    aw = ApplicationWindow()
    aw.show()
    sys.exit(qApp.exec_())