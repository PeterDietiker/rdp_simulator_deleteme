from RDP_simulator.scripting_methods import ScriptingMethods as sm #Do not run this line!!!
import numpy as np
print(sm.RDP.sig_sim.calc_dict)
print(sm.RDP.sig_sim.result_dict)
import pandas

sm.set_laser_parameters(power=5, wavelength = 645)
sm.set_calc_parameters(method = 'smc', stats = 'poisson', threshold_quantile = 5.5,\
                             normalisation = 60, single_count = False)
sm.take_measurements(10)
sm.set_dye_parameters(dye = "alexa647", conc = 60)
sm.set_optics_parameters(r_pinhole=150)
sm.set_scan_parameters(bin_length = 100)
sm.set_statistics_size(10)
sm.set_optics_parameters(f_objective=3,na_objective=0.75)
sm.reset_statistics()
sm.reset_results_df()
sm.set_dye_parameters(dye = "alexa647", conc = 0)


for n in np.linspace(10000,250000,20):
    sm.reset_statistics()
    sm.set_noise_parameters(mean = n)
    sm.take_measurements(10)
    sm.RDP.sig_sim.results_df.add_result(baseline = n/10000, color = "red")
    sm.plot_results(x_axis = 'baseline', plot_color = "color")

sm.set_statistics_size(10)
sm.reset_statistics()
sm.reset_results_df()
for conc in np.linspace(0,110,10):
    sm.reset_statistics()
    sm.set_dye_parameters(conc = conc)
    sm.take_measurements(5)
    sm.RDP.sig_sim.results_df.add_result(conc = conc*1E-15)
    sm.plot_results(x_axis = 'conc')


for fl in np.linspace(1,20,10):
    sm.set_optics_parameters(f_objective = fl)
    sm.take_measurements(10)
    sm.RDP.sig_sim.results_df.add_result(f_objective = fl)
    sm.plot_results(x_axis = 'f_objective')

for na in np.linspace(0.1,0.8,10):
    sm.set_optics_parameters(na_objective = na)
    sm.take_measurements(10)
    sm.RDP.sig_sim.results_df.add_result(NA = na)
    sm.plot_results(x_axis = 'NA')

sm.set_statistics_size(10)
sm.reset_statistics()
sm.reset_results_df()
for method, stats, color in zip(['local_baseline','local_baseline','smc'],["poisson", "poisson robust",'poisson'],['blue','green','red']):
    sm.set_calc_parameters(method = method, stats = stats)
    for conc in np.linspace(0,1000,10):
        sm.set_dye_parameters(conc = conc)
        sm.take_measurements(10)
        sm.RDP.sig_sim.results_df.add_result(plot_color = color)
        sm.plot_results(x_axis = 'conc',plot_color = "plot_color")



for method, stats, color in zip(['local_baseline','local_baseline','smc'],["poisson", "poisson robust",'poisson'],['blue','green','red']):
    sm.set_calc_parameters(method = method, stats = stats)
    for conc in np.hstack((np.repeat([0],2),np.linspace(0,50,10))):
        sm.set_dye_parameters(conc = conc)
        sm.take_measurements(1)
        sm.RDP.sig_sim.results_df.add_result(plot_color = color)
        sm.plot_results(x_axis = 'conc',plot_color = "plot_color")

#sm.logger.warning(sm.RDP.sig_sim.results_df.results_df)
#sm.RDP.sig_sim.results_df.save("collimation_focal_length_scan_all.csv")

sm.logger.warning(sm.RDP.sig_sim.rdp_streamer.actual_concentration)
print(pandas.__version__)
