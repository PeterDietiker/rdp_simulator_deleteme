'''
Created on 17 Nov 2017

@author: CTCHguest
'''
from past.utils import old_div
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt


class gaussian_kde_2D(object):
    '''
    Class for two dimensional conditional kernel density estimation for two correlated channels
    It models the density as a linear combination of gaussian kde in the third dimension.
    This approach has the following adavantages:
    1. THe underlying density must be approximatable by  gaussian kernel in two dimension only, the shape in the third dimension is arbitrarly
    2, The conditional pdf in two dimension can easily be calculated.

    To be tested: Normalisation of the pdf. Should be ok but was never tested.
    '''

    def __init__(self, noise_array_1, parameter_array=np.array([0]), noise_array_2=np.array([0]), method='scipy', bandwidth='scott'):
        '''
        Initialises the gaussian_kde_2D with the noise ant the parameter array.
        :param noise_array_1: Measured noise of channel 1. For each value in parameter_array,
        :type noise_array_1: nd.array
        :param parameter_array: array of the values of first dimension at which the noise was measrued. If noise_array is of rank 1, then this is optional and has no influence
        :type parameter_array: nd.array
        :param noise_array_2: Measured noise of channel 2. For each value in parameter_array,
        :type noise_array_2: nd.array
        :param method: package used for the kernel density estimation. 'scipy' and 'scikit' is implemented. Scikit is faster.
        :type method: string
        :param bandwidth: bandwidth used for the kernel density estimation. scotts rule ("scott"), silvermans rule ("silverman") or a numerical gan be given
        :type bandwidth: int,float or string
        '''
        if method == 'scikit':
            # make it this way to be able to use the class without installing scikit
            global sklearn
            sklearn = __import__('sklearn.neighbors', globals(), locals())

        self.noise_array_1 = np.atleast_2d(noise_array_1)  # in case there is only one measurement
        if not np.array_equal(noise_array_2, np.array([0])):
            self.noise_array_2 = np.atleast_2d(noise_array_2)  # in case there is only one measurement
            self.dimensions = 3
        else:
            self.dimensions = 2
            self.noise_array_2 = None
        self.parameter_array = np.atleast_1d(parameter_array)  # in case there is only one parameter
        assert self.noise_array_1.shape[0] == self.parameter_array.shape[0], 'noise_array and piezo array must be of same first dimension'
        if self.dimensions == 3:
            assert self.noise_array_2.shape[0] == self.parameter_array.shape[0], 'noise_array and piezo array must be of same first dimension'
        self.method = method
        self.bandwidth = bandwidth
        self.get_kde_list()  # get list of gaussian_kde in 2. dimension

    def get_kde_list(self):
        '''
        This generates a list of gaussian_kde in 2. dimension (list is along 1. dimension, 1. dimension is deimension of parameter_array)
        '''
        self.kde_list = []
        if self.method == 'scipy':
            bandwidth = self.bandwidth
            for idx, value in enumerate(self.parameter_array):
                if self.dimensions == 3:
                    array = np.vstack((np.atleast_2d(self.noise_array_1[idx]), np.atleast_2d(self.noise_array_2[idx])))
                    self.kde_list.append(scipy.stats.gaussian_kde(array, bw_method=bandwidth))
                else:
                    self.kde_list.append(scipy.stats.gaussian_kde(self.noise_array_1[idx], bw_method=bandwidth))
        elif self.method == 'scikit':
            if self.bandwidth == 'scott':
                # bandwidth=0.04
                # scotts rule from scipy.stats.kde:
                n = self.noise_array_1[0].shape[0]
                d = self.dimensions - 1
                bandwidth = np.power(n, (old_div(-1., (d + 4))))
                print(bandwidth)
            elif self.bandwidth == 'silverman':
                # silverman rule from scipy.stats.kde
                n = self.noise_array_1[0].shape[0]
                d = self.dimensions - 1
                bandwidth = np.power(n * (d + 2.0) / 4.0, old_div(-1., (d + 4)))
                print(bandwidth)
            else:
                bandwidth = self.bandwidth
            for idx, value in enumerate(self.parameter_array):
                if self.dimensions == 3:
                    array = np.vstack((np.atleast_2d(self.noise_array_1[idx]), np.atleast_2d(self.noise_array_2[idx]))).T
                    self.kde_list.append(sklearn.neighbors.KernelDensity(bandwidth=bandwidth).fit(array))
                else:
                    self.kde_list.append(sklearn.neighbors.KernelDensity(bandwidth=bandwidth).fit(self.noise_array_1[idx][:, np.newaxis]))

    def find_nearest(self, array, value):
        '''
        This finds the nearest value in an array
        :param array: array to look up
        :type array: 1D nd.array
        :param value: value to look up
        :type value: int, float
        '''
        # from: https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
        idx, val = min(enumerate(array), key=lambda x: abs(x[1] - value))
        return idx, val

    def get_components(self, value):
        '''
        gives the components used for the linear combination of the kde
        :param value: value of the parameter in first dimension
        :type value: int, float
        '''
        if len(self.kde_list) > 0:
            # gives postion and value of the nearest element in the parameter array
            nearest = self.find_nearest(self.parameter_array, value)

            dist_vector = np.empty(2)  # vector of the distance of the value to its neighbors in the parameter_array
            index_vector = np.empty(2)  # vector of the index of the neighbors in the parameter_array

            if nearest[1] < value:
                left_dist = value - nearest[1]
                try:
                    right_dist = self.parameter_array[nearest[0] + 1] - value
                except IndexError:
                    # value is larger than the maximum value in parameter_array
                    right_dist = np.finfo(np.float).max
                left_index = nearest[0]
                right_index = left_index + 1
            elif nearest[1] > value:
                if nearest[0] == 0:
                    left_dist = np.finfo(np.float).max
                else:
                    left_dist = value - self.parameter_array[nearest[0] - 1]
                right_dist = self.parameter_array[nearest[0]] - value
                left_index = nearest[0] - 1
                right_index = left_index + 1
            else:
                # exact match
                left_dist = 0
                right_dist = 1
                left_index = nearest[0]
                right_index = left_index + 1
            index_vector[0] = left_index
            index_vector[1] = right_index
            dist_vector[0] = left_dist
            dist_vector[1] = right_dist
            # normalise dist vector to get coefficients of the linear combination
            norm = np.linalg.norm(dist_vector, ord=1)
            dist_vector /= norm
            dist_vector = 1 - dist_vector
            # print dist_vector
            result = np.empty((0, 2))
            # generate array of the non-zero components
            for idx, value in enumerate(dist_vector):
                if value > 0:
                    result = np.vstack((result, np.array((index_vector[idx], dist_vector[idx]))))
        else:
            # only one component
            result = np.array([0, 1])
        result = np.atleast_2d(result)
        # result=np.array([[left_index,dist_vector[0]],[right_index,dist_vector[1]]])

        return result

    def group_values(self, values, group_span=5):
        '''
        This function groups the values in groups of span group_span
        Nan values on values are handled in a way that Nan at the beginning of the array are assigned to the lowest value in array
        and Nan values at the end of the array
        :param values: values to be grouped
        :type values: np.array
        :param group_span: span of the groups. If group_span==0, values are not grouped except the NaN's
        :type group_span: int,float
        '''
        values_clean = values[~np.isnan(values)]
        # indexes of all nan
        nan_idx = np.nonzero(np.isnan(values))[0]
        # split the indexes to a group at the beginning of the array and one at the end of the array
        nan_split = np.split(nan_idx, np.where(np.diff(nan_idx) != 1)[0] + 1)
        if group_span > 0:
            nr_groups = int(values_clean.ptp() / float(group_span)) + 1

            groups = np.empty((nr_groups, 2))
            actual_index = 0
            a_min = values_clean.min()
            for idx, group in enumerate(groups):
                group[0] = np.searchsorted(values_clean, a_min + (idx + 1) * group_span, 'left') - actual_index  # number of values in range
                group[1] = values_clean[actual_index:actual_index + int(group[0])].mean()  # mean value
                actual_index += int(group[0])
        else:
            # just copy values_clean
            nr_groups = values_clean.shape[0]
            groups = np.column_stack((np.ones_like(values_clean), values_clean))
        # add the groups of the nans
        # print nan_split
        #import ipdb; ipdb.set_trace()
        if nan_split[0].shape[0] > 0:
            if nan_split[0][0] == 0:
                groups = np.vstack((np.array([nan_split[0].shape[0], values_clean.min()]), groups))
                if len(nan_split) > 1:
                    groups = np.vstack((groups, np.array([nan_split[1].shape[0], values_clean.max()])))
            else:
                groups = np.vstack((groups, np.array([nan_split[0].shape[0], values_clean.max()])))
        return groups[groups[:, 0] > 0]

    def resample(self, values, nr_samples, group_span=5):
        '''
        resamples the kde
        :param value: value of parameter in first dimension
        :type value: int , float
        :param nr_samples: number of samples
        :type nr_samples: int
        :param group_span: samples are grouped according to the value in 1st dimension (parameter dimension) for performance reasons
                            This is the span of the group
        :type group_span: int,float
        '''
        values = np.atleast_1d(values)
        groups = self.group_values(values, group_span)
        samples = np.empty((values.shape[0] * nr_samples, self.dimensions - 1))

        actual_index = 0
        for idx, group in enumerate(groups):
            components = self.get_components(group[1])
            # print components
            nr_group_samples = nr_samples * int(group[0])
            if components.shape[0] > 1:
                nr_samples_1 = np.random.choice([0, 1], nr_group_samples, p=components[:, 1]).sum()
                nr_samples_0 = nr_group_samples - nr_samples_1
                if self.method == 'scipy':
                    samples[actual_index:actual_index + nr_group_samples, :] = np.hstack((self.kde_list[components[0, 0].astype(int)].resample(nr_samples_0),
                                                                                          self.kde_list[components[1, 0].astype(int)].resample(nr_samples_1))).T
                elif self.method == 'scikit':
                    samples[actual_index:actual_index + nr_group_samples, :] = np.hstack((self.kde_list[components[0, 0].astype(int)].sample(nr_samples_0).ravel(),
                                                                                          self.kde_list[components[1, 0].astype(int)].sample(nr_samples_1).ravel())).reshape(nr_group_samples, self.dimensions - 1)
            else:
                if self.method == 'scipy':
                    samples[actual_index:actual_index + nr_group_samples, :] = self.kde_list[components[0, 0].astype(int)].resample(nr_group_samples).T
                elif self.method == 'scikit':
                    samples[actual_index:actual_index + nr_group_samples, :] = self.kde_list[components[0, 0].astype(int)].sample(nr_group_samples).ravel().reshape(nr_group_samples, self.dimensions - 1)
            actual_index += nr_group_samples
        if self.dimensions == 3:
            return samples
        else:
            return samples

    def pdf(self, values):
        '''
        calculates the conditional pdf in the second diemnsion.
        :param values: array of the points where to calculate the pdf
        :type values: nd.array: [[1. dimension,2.dimension,(3. dimension)]]
        '''
        values = np.atleast_2d(values)
        assert values.shape[1] >= 2, 'values must have two components'
        result = np.zeros(values.shape[0])
        for idx, value in enumerate(values):
            components = self.get_components(value[0])
            if self.method == 'scipy':
                for comp in components:
                    result[idx] += (self.kde_list[comp[0].astype(int)].pdf(value[1:]) * comp[1])[0]
            elif self.method == 'scikit':
                for comp in components:
                    result[idx] += np.exp((self.kde_list[comp[0].astype(int)].score_samples(np.atleast_2d(value[1:])).ravel())[0]) * comp[1]

        return result

    def pdf_plot2D(self, ax=None, save_path=None, ylim=None, xlim=None, axes=("1", "2"), z_value=None):
        '''
        plots the probability density function together with the underlying data in 1D
        :param ax: axis to plot, if none a new figure is used
        :type ax: matplotlib.axes.Axes
        :param save_path: path to save the image, if None image is not saved
        :type save_path: string
        :param ylim: ylimits of the plot
        :type ylim: list: [ylim_min,ylim_max]
        :param axes: defines the x and y axes of the plot. "1" means Channel 1, "2" means channel 2 and "p" means parameter
        :type axes: list of 2 strings
        :param z_value: value of third dimension, if None the pdf is integrated over third dimension
        :type z_value: float or None
        '''
        axes = list(axes)
        axes_list = ["1", "2", "p"]
        # determine which axis is z-axis (the one which is no in the axes list)

        data_dict = {"1": self.noise_array_1, "2": self.noise_array_2, "p": self.parameter_array}

        if self.dimensions == 2:
            axes = ("p", "1")
        if axes[0] == "p":
            xmin = data_dict[axes[0]].min()
            xmin -= 0.2 * abs(xmin)
            xmax = data_dict[axes[0]].max()
            xmax += 0.2 * abs(xmax)
        else:
            xmin = np.percentile(data_dict[axes[0]], 0.1)
            xmax = np.percentile(data_dict[axes[0]], 99.9)
        if axes[1] == "p":
            ymin = data_dict[axes[1]].min()
            ymin -= 0.2 * abs(ymin)
            ymax = data_dict[axes[1]].max()
            ymax += 0.2 * abs(ymax)
        else:
            ymin = np.percentile(data_dict[axes[1]], 0.1)
            ymax = np.percentile(data_dict[axes[1]], 99.9)

        if ylim is not None:
            ymin = ylim[0]
            ymax = ylim[1]
        if xlim is not None:
            xmin = xlim[0]
            xmax = xlim[1]

        #import ipdb; ipdb.set_trace()
        if self.dimensions == 3:
            #which is z_axis
            z_axis = axes_list[np.nonzero([y not in axes for y in axes_list])[0][0]]
            if z_value is None:
                zmin = np.percentile(data_dict[z_axis], 0.1)
                zmax = np.percentile(data_dict[z_axis], 99.9)
            else:
                zmin = z_value
                zmax = z_value
            Z, X, Y = np.mgrid[zmin:zmax:50j, xmin:xmax:50j, ymin:ymax:50j]
            if axes == ["p", "1"]:
                points = np.vstack((X.ravel(), Y.ravel(), Z.ravel())).T
            elif axes == ["1", "p"]:
                points = np.vstack((Y.ravel(), X.ravel(), Z.ravel())).T
            elif axes == ["1", "2"]:
                points = np.vstack((Z.ravel(), X.ravel(), Y.ravel())).T
            elif axes == ["2", "1"]:
                points = np.vstack((Z.ravel(), Y.ravel(), X.ravel())).T
            elif axes == ["p", "2"]:
                points = np.vstack((X.ravel(), Z.ravel(), Y.ravel())).T
            elif axes == ["2", "p"]:
                points = np.vstack((Y.ravel(), Z.ravel(), X.ravel())).T
        else:
            Z, X = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
            points = np.vstack((Z.ravel(), X.ravel())).T
        # return points,X.shape
        pdf = np.reshape(self.pdf(points).T, X.shape)
        if self.dimensions == 3:
            pdf = np.sum(pdf, axis=0)

        # selects data to plot
        # the data is filtered according to the set z_value
        # if z dimension is the parameter dimension, the value closest to the set z_value are used
        # if z dimension is "1" or "2", the values closer than 1/10 peak-to-peak to the set z_value are used
        if axes[0] == "p":
            x_data = np.repeat(self.parameter_array, self.noise_array_1.shape[1])
        else:
            if z_value is None:
                x_data = data_dict[axes[0]].ravel()
            else:
                if z_axis == "p":
                    x_data = data_dict[axes[0]]
                    x_data = x_data[self.find_nearest(data_dict["p"], z_value)[0], :].ravel()
                else:
                    x_data = data_dict[axes[0]]
                    y_data = data_dict[axes[1]]
                    filter_array = np.abs(y_data - z_value) < old_div(np.ptp(y_data), 10)
                    data = x_data[filter_array].ravel()

        if axes[1] == "p":
            y_data = np.repeat(self.parameter_array, self.noise_array_1.shape[1])
        else:
            if z_value is None:
                y_data = data_dict[axes[1]].ravel()
            else:
                if z_axis == "p":
                    y_data = data_dict[axes[1]]
                    y_data = y_data[self.find_nearest(data_dict["p"], z_value)[0], :].ravel()
                else:
                    y_data = data_dict[axes[1]]
                    filter_array = np.abs(y_data - z_value) < old_div(np.ptp(y_data), 10)
                    y_data = y_data[filter_array].ravel()

        if ax is None:
            fig, ax = plt.subplots()
            newfig = True
        else:
            newfig = False
        # ax.imshow(np.rot90(Z), cmap=plt.cm.gist_earth_r,extent=[xmin, xmax, ymin,noise_array_flattend ymax])
        # only plot 5000 points
        leave_out = int(np.ceil(old_div(x_data.shape[0] * 1.0, 5000)))
        # leave_out=1
        ax.plot(x_data[::leave_out], y_data[::leave_out], 'k.', markersize=1)
        ax.set_xlim([xmin, xmax])
        if ylim is None:
            ax.set_ylim([ymin, ymax])
        else:
            ax.set_ylim(ylim)
        if axes[0] == '1':
            x_label = "Channel 1"
        elif axes[0] == '2':
            x_label = "Channel 2"
        elif axes[0] == 'p':
            x_label = "Piezo voltage / V"
        ax.set_xlabel(x_label)
        if axes[1] == '1':
            y_label = "Channel 1"
        elif axes[1] == '2':
            y_label = "Channel 2"
        elif axes[1] == 'p':
            y_label = "Piezo voltage / V"
        ax.set_ylabel(y_label)
        if 'p' in axes:
            ax.set_title("2D conditional kernel estimation")
        else:
            ax.set_title("2D conditional kernel estimation at piezo voltage of {}".format(z_value))
        ax.imshow(np.rot90(pdf), cmap=plt.cm.gist_earth_r, extent=[xmin, xmax, ymin, ymax], interpolation="hamming")
        if axes[1] == 'p':
            ax.set_aspect(old_div(np.asarray(ax.get_xlim()).ptp(), np.asarray(ax.get_ylim()).ptp()))
        else:
            ax.set_aspect('auto')
        if save_path is not None:
            fig.savefig(save_path, dpi=300)
        if newfig:
            plt.show()
        # return fig

    def pdf_plot1D(self, value, ax=None, axis='1'):
        '''
        plot the conditional pdf at a certain value in first dimension
        :param value: vlaue of parameter in first dimension
        :type value: int, float
        :param ax: axis to plot, if none a new figure is used
        :type ax. matplotlib.axes.Axes
        '''
        parameter_nearest = self.find_nearest(self.parameter_array, value)
        xmin = self.noise_array_1[parameter_nearest[0]].min()
        xmin -= 0.2 * abs(xmin)
        xmax = self.noise_array_1[parameter_nearest[0]].max()
        xmax -= 0.2 * abs(xmax)

        if self.dimensions == 3:
            ymin = self.noise_array_2[parameter_nearest[0]].min()
            ymin -= 0.2 * abs(ymin)
            ymax = self.noise_array_2[parameter_nearest[0]].max()
            ymax -= 0.2 * abs(ymax)
            Z, X, Y = np.mgrid[value:value:1j, xmin:xmax:20j, ymin:ymax:20j]
            points = np.vstack((Z.ravel(), X.ravel(), Y.ravel())).T
        else:
            Z, X = np.mgrid[value:value:1j, xmin:xmax:100j]
            points = np.vstack((Z.ravel(), X.ravel())).T
        pdf = np.reshape(self.pdf(points).T, X.shape)
        if self.dimensions == 3:
            if axis == '1':
                pdf = np.mean(pdf, axis=2)[0]
            elif axis == '2':
                pdf = np.mean(pdf, axis=1)[0]
            if axis == '1':
                noise = self.noise_array_1[parameter_nearest[0]]
                points = X[0, :, 0]
                binwidth = old_div(noise.ptp(), 50)
                bins = np.arange(xmin, xmax + binwidth, binwidth)
            elif axis == '2':
                noise = self.noise_array_2[parameter_nearest[0]]
                points = Y[0, 0, :]
                binwidth = old_div(noise.ptp(), 50)
                bins = np.arange(ymin, ymax + binwidth, binwidth)
        else:
            pdf = pdf[0]
            noise = self.noise_array_1[parameter_nearest[0]]
            points = X[0, :]
            binwidth = old_div(noise.ptp(), 50)
            bins = np.arange(xmin, xmax + binwidth, binwidth)

        if ax is None:
            fig, ax = plt.subplots()
            newfig = True
        else:
            newfig = False
        n, bins, patches = ax.hist(noise, bins, color='g', density=True)
        pdf = old_div(pdf * n.max(), pdf.max())
        ax.plot(points, pdf)
        if axis == '1':
            x_label = "Channel 1"
        elif axis == '2':
            x_label = "Channel 2"
        elif axis == 'p':
            x_label = "Piezo voltage / V"
        ax.set_xlabel(x_label)
        y_label = "Probability density"
        ax.set_ylabel(y_label)
        ax.set_title('pdf at value of {}'.format(value))
        ax.set_aspect('auto')
        ax.set_ylim([0, n.max() * 1.1])
        # make x-axis symmetric
        xlim = ax.get_xlim()
        ax.set_xlim([-np.abs(xlim).max(), np.abs(xlim).max()])
        if newfig:
            plt.show()
        # return fig


def measure(n, std=[1, 0.5]):
    "Measurement model, return two coupled measurements."
    m1 = np.random.normal(size=n, scale=std[0])
    m2 = np.random.normal(scale=std[1], size=n)
    return m1 + m2, m1 - m2

if __name__ == '__main__':

    data1 = measure(1000, std=[1, 0.5])
    data2 = measure(1000, std=[2, 1])
    ch1 = np.vstack((data1[0], data2[0]))
    ch2 = np.vstack((data1[1], data2[1]))
    n_estimator = gaussian_kde_2D(ch1, [10, 20], ch2, method='scipy', bandwidth="scott")
    n_estimator.pdf_plot1D(10, axis='1')
    n_estimator.pdf_plot2D(axes=("1", "2"), z_value=10)
    n_estimator.pdf_plot2D(axes=("p", "2"), ylim=[-6, 6])
