'''
Created on 19.05.2017

@author: pdietiker
'''
import matplotlib
import os
import sys
import pickle
import numpy as np
import types
from decorator import decorator

from pyqode.qt import QtCore, QtGui, QtWidgets
import pyqtgraph as pg


def set_plot_limits(ax, direction):
    '''
    setzt die plot limits. Version 2. hier wird direkt die achse uebergeben 
    Falls die y achse gesetzt wird, so werden nur die daten innerhalb der plotlimits der x-achse fuer die skalierung verwendet.
    :param ax:         Achse fuer die die limits gesetzt werden sollen
    :param direction:  Richtung fuer die die limits gesetz werden sollen: 'x' oder 'y'
    '''

    if direction in ['x', 'both']:
        lims_new = np.asarray(ax.get_xlim())
        # bestimmung ob es sich bei der x-achse um ein datetime objekt handelt
        for line in ax.lines:
            xdata = np.atleast_1d(line.get_xdata())
            if len(xdata) == 0:
                continue

                # np.isfinite funktioniert nicht mit dem datentyp datetime. daher wird erst hier geprueft.
            xdata = xdata[np.isfinite(xdata)]
            if len(xdata) > 0:
                lims_new = np.array([(xdata).min(), (xdata).max()])
            else:
                continue
            minimum = lims_new[0]
            maximum = lims_new[1]
            break

        for line in ax.lines:
            xdata = np.atleast_1d(line.get_xdata())
            if len(xdata) == 0:
                continue
            xdata = xdata[np.isfinite(xdata)]
            if len(xdata[np.isfinite(xdata)]) > 0:
                minimum = (xdata).min()
                maximum = (xdata).max()
            else:
                continue
            if minimum < lims_new[0]:
                lims_new[0] = minimum
            if maximum > lims_new[1]:
                lims_new[1] = maximum

        if ax.get_xscale() == 'log':
            margin_factor = 4
            lims_new[0] = lims_new[0] - lims_new[0] / margin_factor
            lims_new[1] = lims_new[1] + lims_new[1] / margin_factor
        else:
            margin_factor = 20
            lims_new[0] = lims_new[0] - np.ptp(lims_new) / margin_factor
            lims_new[1] = lims_new[1] + np.ptp(lims_new) / margin_factor

        ax.set_xlim(lims_new)

    if direction in ['y','both']:
        x_lims = np.asarray(ax.get_xlim(), dtype=np.float32)
        y_lims_new = np.asarray(ax.get_ylim(), dtype=np.float32)
        for line in ax.lines:
            xdata = np.atleast_1d(line.get_xdata())
            if len(xdata) == 0:
                continue
            ydata = np.atleast_1d(line.get_ydata())
            ydata = ydata[np.isfinite(xdata)]
            xdata = xdata[np.isfinite(xdata)]
            xdata = xdata[np.isfinite(ydata)]
            ydata = ydata[np.isfinite(ydata)]
            ydata = ydata[np.logical_and(xdata >= x_lims[0], xdata <= x_lims[1])]

            if len(ydata) >= 1:
                y_lims_new = np.array([ydata.min(), ydata.max()], dtype=np.float32)
                break
        for line in ax.lines:
            xdata = np.atleast_1d(line.get_xdata())
            if len(xdata) == 0:
                continue

            ydata = np.atleast_1d(line.get_ydata())
            ydata = ydata[np.isfinite(xdata)]
            xdata = xdata[np.isfinite(xdata)]
            xdata = xdata[np.isfinite(ydata)]
            ydata = ydata[np.isfinite(ydata)]
            ydata = ydata[np.logical_and(xdata >= x_lims[0], xdata <= x_lims[1])]

            if len(ydata) >= 1:
                #             if (line.get_ydata()).min()<lims[0]+np.ptp(lims)/20:
                if ydata.min() < y_lims_new[0]:
                    y_lims_new[0] = ydata.min()
    #             if (line.get_ydata()).max()>lims[1]-np.ptp(lims)/20:
                if ydata.max() > y_lims_new[1]:
                    y_lims_new[1] = ydata.max()

        if ax.get_yscale() == 'log':
            margin_factor = 4
            y_lims_new[0] = y_lims_new[0] - y_lims_new[0] / margin_factor
            y_lims_new[1] = y_lims_new[1] + y_lims_new[1] / margin_factor
        else:
            margin_factor = 20
            y_lims_new[0] = y_lims_new[0] - np.ptp(y_lims_new) / margin_factor
            y_lims_new[1] = y_lims_new[1] + np.ptp(y_lims_new) / margin_factor

        ax.set_ylim(y_lims_new)


def updateView(source, targed):
    '''
    updates the view of targed
    :param source: source ViewBox
    :type source: pyqtgraph.ViewBox()
    :param targed: target Viewbox
    :type targed: pyqtgraph.ViewBox()
    '''
    # view has resized; update auxiliary views to match
    targed.setGeometry(source.sceneBoundingRect())

    # need to re-update linked axes since this was called
    # incorrectly while views had different shapes.
    # (probably this should be handled in ViewBox.resizeEvent)
    targed.linkedViewChanged(source, targed.XAxis)

class InfoText(pg.TextItem):
    '''
    This Class generates a TextItem which with additional functionality which allows for the
    automatic positioning in the upper left corner. It shows some informations on the plotted curves
    '''
    def __init__(self,plot):
        '''
        Initialises a new InfoText instance
        :param plot: parent PlotWidget
        :type plot: PlotWidget
        '''
        super(InfoText, self).__init__()
        self.plot=plot

        #connect the update functions
        #self.position_proxy = pg.SignalProxy(plot.sigRangeChanged, rateLimit=50, slot=self.update_position)
        plot.sigRangeChanged.connect(self.update_position)
        plot.scene().sigMouseClicked.connect(self.update_text)
    def update_position(self):
        '''
        updates the position of the label in such a way that it is bound to the upper left corner of the plt
        '''
        x_pos=self.plot.getAxis('bottom').range[0]
        y_pos=self.plot.getAxis('left').range[1]
        self.setPos(x_pos,y_pos)
    def update_text(self,evt):
        '''
        Updates the shown information
        :param evt: sigMouseClicked event
        :type evt: sigMouseClicked event
        '''
        pos = evt._scenePos
        mousePoint = self.plot.getViewBox().mapSceneToView(pos)
        xData_ref, yData_ref = self.plot.listDataItems()[0].getData()
        xData_sig, yData_sig = self.plot.listDataItems()[1].getData()
        idx = np.searchsorted(xData_sig,  mousePoint.x(), side="left")-1
        if (idx>=0) and (idx < len(xData_sig)):
            self.setText("threshold: -")
        else:
            self.setText("")

def pydevBrk():
    '''
    enables the pydev debugger
    '''
    import os
    import sys
    if os.name == 'nt':
        #pydevdPath = r'C:\Users\CTCHguest\.p2\pool\plugins\org.python.pydev_4.5.1.201601132212\pysrc'
        pydevdPath = r'C:\Users\peterdietiker\.p2\pool\plugins\org.python.pydev.core_7.5.0.202001101138\pysrc'
    else:
        pydevdPath = r'/home/pdietiker/.p2/pool/plugins/org.python.pydev.core_7.2.0.201903251948/pysrc'
    if pydevdPath not in sys.path:
        sys.path.append(pydevdPath)
    try:
        import pydevd
        pydevd.settrace(suspend=True)
    except ImportError:
        pass  # Most probably, pydev is not installed on the system


@decorator
def wait(function, *args, **kwargs):
    '''
    decorator which changes the cursor to a Busy cursor during evaluation of functions
    :param function: function to decorate
    :type function: function
    '''
    QtWidgets.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
    try:
        function(*args, **kwargs)
    except IOError as e:
        print("Unexpected error:", sys.exc_info()[0])
    finally:
        QtWidgets.QApplication.restoreOverrideCursor()

