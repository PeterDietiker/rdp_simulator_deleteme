'''
Created on 19.05.2017

@author: pdietiker
'''

import sys
import os
import pyqtgraph
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pyqode.qt

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5 import NavigationToolbar2QT as NavigationToolbar
from pyqode.qt import QtCore, QtGui, QtWidgets 

class MyMplCanvas(FigureCanvas):

    def __init__(self, parent=None, width=5, height=3, dpi=100, title='', message_label=None):
        # self.fig = matplotlib.figure.Figure(figsize=(width, height), dpi=dpi,facecolor='white')
        self.fig = plt.figure(figsize=(width, height), dpi=dpi, facecolor='white')
        self.axes = self.fig.add_subplot(111)

        self.compute_initial_figure()
        super(MyMplCanvas, self).__init__(self.fig)
        # FigureCanvas.simulator(self, self.fig)
        self.setParent(parent)

        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.updateGeometry()

        # add toolbar but do not show it
        self.navi_toolbar = NavigationToolbar(self, None)
        # activate zoom mode
        self.toolbar_mode = 'zoom'
        self.navi_toolbar.zoom()

        # text to show mouse event
        self.event_text = self.axes.text(0.01, 0.98, "", transform=self.axes.transAxes, verticalalignment='top')
        # last mouse event
        self.last_event = None

        self.message_label = message_label
        self.mpl_connect('button_release_event', self.onclick)

    def compute_initial_figure(self):
        pass

    def redraw(self):
        self.draw()
        self.flush_events()
        self.updateGeometry()

    def onclick(self, event):
        if event.button == 1:
            if (event.xdata is not None) and (event.ydata is not None):
                #event data gives coordinates of the last added axis.
                #but we want the coordinates of the first axes. 
                #Those have to be calculated  
                # see https://stackoverflow.com/questions/16672530/cursor-tracking-using-matplotlib-and-twinx/16672970#16672970
                if self.axes != event.inaxes: 
                    inv = self.axes.transData.inverted()
                    x, y = inv.transform(np.array((event.x, event.y)).reshape(1, 2)).ravel()
                else:
                    x, y = event.x, event.y
                self.event_text.set_text("x: {:6.2f}\ny: {:4.3f}".format(x, y))
                if self.message_label is not None:
                    self.message_label.setText(str(x)+' '+str(y))
                self.redraw()
        elif event.button == 2:
            if self.toolbar_mode == 'zoom':
                self.navi_toolbar.pan()
                self.toolbar_mode = 'pan'
            else:
                self.navi_toolbar.zoom()
                self.toolbar_mode = 'zoom'

        self.last_event = event


class MyStaticMplCanvas(MyMplCanvas):

    def compute_initial_figure(self):
        t = np.arange(0.0, 3.0, 0.01)
        s = np.sin(2 * np.pi * t)
        self.axes.plot(t, s)

if __name__ == '__main__':
    from pyqode.qt import QtCore, QtGui, QtWidgets 
    #from PyQt5 import QtWidgets, QtGui, QtCore
    app = QtWidgets.QApplication(sys.argv)
    main = QtWidgets.QDialog()
    layout = QtWidgets.QVBoxLayout()
    lbl = QtWidgets.QLabel("0")
    plot_canvas = MyMplCanvas(message_label=lbl)
    layout.addWidget(plot_canvas)
    layout.addWidget(lbl)
    main.setLayout(layout)
    plot_canvas.axes.plot(np.arange(100))
    main.show()
    sys.exit(app.exec_())
