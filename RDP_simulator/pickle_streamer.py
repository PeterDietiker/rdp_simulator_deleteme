'''
This package provides a streamer class for pickle objects

It allows for dumping and loading of pickle objects
Created on 12 Oct 2017

@author: P.Dietiker
'''

import os
import pickle
import logging


class pickle_streamer(object):

    def __init__(self, file_path=None, logger=None):
        '''
        initialieses the streamer
        :param file_path: file path
        :type file_path: string
        '''
        self.file_path = file_path
        self.access_mode = 'r'  # stores the actual acess mode
        self.read_position = 0  # stores the actual read position
        self.handle = None  # file handle

        # init handle if file path is given
        self.set_path(file_path)

        # init logger
        if logger is None:
            self.logger = logging.getLogger(__name__)
            self.logger.handlers = []  # clear all handlers
            self.logger.setLevel(logging.DEBUG)
            logging_handler = logging.StreamHandler()
            logging_handler.setLevel(logging.DEBUG)
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            logging_handler.setFormatter(formatter)
            self.logger.addHandler(logging_handler)
        else:
            self.logger = logger

    def set_path(self, file_path=None):
        '''
        Set the path to the file
        :param file_path: path to file
        :type file_path: string
        '''
        if self.handle is not None:
            self.handle.close()
        if file_path is not None:
            self.file_path = os.path.abspath(file_path)
            dirname = os.path.dirname(self.file_path)
            # check for existence of folder
            if not os.path.exists(dirname):
                try:
                    os.mkdir(dirname)
                except:
                    self.handle = None
                    self.file_path = None
                    self.logger.warning('Not able to create path')
                    return
            # open the file
            try:
                self.handle = open(self.file_path, 'a+b')
                self.access_mode == 'r'
                self.handle.seek(0, 0)
                self.read_position = 0
                
            except:
                self.handle = None
                self.logger.warning('Not able to open file')
                return

        else:
            self.file_path = None
            self.handle = None

    def dump(self, data):
        '''
        dumps the given data to the file
        :param data: data to dump
        :type data: any
        '''

        if self.handle is not None:
            if self.access_mode == 'r':
                # if actual access mode was 'r', go to end of file and set actual access mode to 'w'
                self.handle.seek(0, 2)
                self.access_mode = 'w'
            pickle.dump(data, self.handle, protocol=pickle.HIGHEST_PROTOCOL)
        else:
            self.logger.warning('No file selected')

    def load(self):
        '''
        returns the next pickle object in the file
        If EOF is reached 0 is returned
        '''
        if self.handle is not None:
            if self.access_mode == 'w':
                # if actual access mode was 'w', go to actual read position and set actual access mode to 'r'
                self.handle.seek(self.read_position, 0)
                self.access_mode = 'r'
            try:
                data = pickle.load(self.handle)
            except EOFError:
                data = 0
                self.logger.info('End of file reached')
                raise
            self.read_position = self.handle.tell()
        else:
            self.logger.warning('No file selected')
            data = 0
        return data

    def reset(self):
        '''
        resets the file
        '''
        if self.handle is not None:
            self.handle.close()
            open(self.file_path, 'w').close()
            self.handle = open(self.file_path, 'a+b')
            return
        else:
            self.logger.warning('No file selected')
            return

    def __del__(self):
        if self.handle is not None:
            self.handle.close()
