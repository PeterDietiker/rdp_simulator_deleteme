# -*- coding: utf-8 -*-
"""
This file contains the pyQode QtDesigner plugin and the handler which controls the editor and the script execution

Created on 6 Apr 2018

@author: pdietiker
"""

# This only works with PyQt, PySide does not support the QtDesigner module
import os
import platform
import sys
import json
import textwrap
import re
import subprocess
import logging
os.environ['QT_API'] = 'pyqt5'

from pyqode.qt import QtCore, QtGui, QtWidgets
from pyqode.core import modes as coremodes
from pyqode.core.widgets import SplittableCodeEditTabWidget
from pyqode.core.api import TextHelper
from pyqode.core.api.decoration import TextDecoration
#from pyqtgraph.Qt import QSettings
from pyqode.python.backend import server
from pyqode.python.widgets.code_edit import PyCodeEdit
from pyqode.python import modes


from RDP_simulator import completion_server
from RDP_simulator.scripting_methods import ScriptingMethods
import RDP_simulator.helper_functions  as hf


execution_wait = QtCore.QWaitCondition()
execution_mutex =QtCore.QMutex()

class PyCodeEditPlugin(SplittableCodeEditTabWidget):
    
    def setCurrentIndex(self,index):
        pass
    
    def klass(self):
        return PyCodeEdit

    def objectName(self):
        return 'pyCodeEdit'



class pyqode_handler(QtCore.QObject):
    '''
    The is the class which takes care of all the routines related to the script editor and script execution
    '''
    #signal to stop script
    stop_signal = QtCore.pyqtSignal()
    #variable which indicates if script thread should be paused
    pause_execution = False

    def __init__(self,tabWidget,parent_instance):
        super(pyqode_handler,self).__init__()
        self.parent = parent_instance #parent instance (RDP_simulator)
        #self.word_list_path = self.create_word_list()
        self.logger = parent_instance.out_handler.logger #the logger
        self.tabWidget = tabWidget #PyCodeEditPlugin widget
        self.ui = tabWidget.window().ui #the user interface
        self.tabWidget.show()
        self.tabWidget.register_code_edit(PyCodeEdit)
        self.settings=Settings()
        self.settings.interpreter = sys.executable
        #thread in which the scripts are executed
        self.script_thread=ScriptThread(self,self.logger)
        #line highlighter
        self.exec_highlighter=ExecutionHighlighter()
        #add statusbar widget
        self.script_status_widget = QtWidgets.QLabel("Script not running");
        self.script_status_widget.setAutoFillBackground(True)
        self.script_status_widget.setStyleSheet("""QLabel { background-color : white;}""")
        self.script_status_widget.setStyle((self.script_status_widget.style()))
        self.ui.statusbar.addPermanentWidget(self.script_status_widget);
        #connect all actions related to the editor and the scritp exectuion
        self.conncet_actions()

    def __del__(self):

        self.tabWidget.close_all()
        #remove cyclic reference of self scrpt_thread in its globals dict
        self.script_thread.globals.pop("st")
        #print("script_thread refcount: {}".format(sys.getrefcount(self.script_thread)))
        del self.script_thread
        print("pyqode_handler deleted")

    def conncet_actions(self):
        '''
        connects the actions
        '''
        self.ui.actionNew.triggered.connect(self.on_new)
        self.ui.actionOpen.triggered.connect(self.on_open)
        self.ui.actionSave.triggered.connect(self.on_save)
        self.ui.actionSave_as.triggered.connect(self.on_save_as)
        self.tabWidget.addAction(self.ui.actionRun_selection)
        self.ui.actionRun_selection.triggered.connect(self.on_run_selection)
        self.tabWidget.addAction(self.ui.actionRun_all)
        self.ui.actionRun_all.triggered.connect(self.on_run_all)
        self.tabWidget.addAction(self.ui.actionStop)
        self.ui.actionStop.triggered.connect(self.on_stop_execution)
        self.tabWidget.addAction(self.ui.actionPause_script)
        self.ui.actionPause_script.triggered.connect(self.on_pause_execution)
        self.ui.actionStop.setDisabled(True)
        self.ui.actionPause_script.setDisabled(True)
        #execution actions
        #execution line highlighter
        self.script_thread.hl_signal.connect(self.update_execution_highlighter)
        #signal for script finished
        self.script_thread.end_signal.connect(self.on_exec_thread_finished)
        #signal for new command to execute
        self.script_thread.command_signal.connect(self.execute_command)
        
        
        
    def setup_editor(self, editor):
        """
        Setup the python editor, run the server and connect a few signals.

        :param editor: editor to setup.
        """
        editor.cursorPositionChanged.connect(self.on_cursor_pos_changed)
        try:
            m = editor.modes.get(modes.GoToAssignmentsMode)
        except KeyError:
            pass
        else:
            assert isinstance(m, modes.GoToAssignmentsMode)
            m.out_of_doc.connect(self.on_goto_out_of_doc)
        #=======================================================================
        # try:
        #     autocomp=editor.modes.get(coremodes.CodeCompletionMode)
        # except KeyError:
        #     pass
        # else:
        #     assert isinstance(autocomp, coremodes.CodeCompletionMode)
        #     autocomp.filter_mode = 2
        #=======================================================================
        
        #start the server for the autocompletion 
        #this should be started on on_new() but somehow it does not work...
        #editor.backend.start(completion_server.__file__, args = [self.word_list_path])
        #hf.pydevBrk()
        syspath = os.path.dirname(os.getcwd())
        args = ["--syspath",syspath]
        editor.backend.start(completion_server.__file__,args =args)
        
        #editor.backend.start(server.__file__)#, args = [self.word_list_path])
    
        #editor.setPlainText(default_text)
        #editor.moveCursor(QtGui.QTextCursor.End)
    
    
    def on_new(self):
        """
        Add a new empty code editor to the tab widget
        """
        interpreter, pyserver, args = self._get_backend_parameters()
        new_editor = self.tabWidget.create_new_document(
                    extension='.py', interpreter=interpreter,
                    server_script=pyserver,args=args)
        self.setup_editor(new_editor)
            
            #self.actionRun.setDisabled(True)
            #self.actionConfigure_run.setDisabled(True)
        #add default text
        default_text="from RDP_simulator.scripting_methods import ScriptingMethods as sm\nimport RDP_simulator.lasercontroller3 as lc3\nsm.take_measurements(10)"
        TextHelper(new_editor).insert_text(default_text,keep_position=False)
     
     
    def on_save(self):
        '''
        Save the current editor document
        '''
        self.tabWidget.save_current()
        #self._enable_run()
        #self._update_status_bar(self.tabWidget.current_widget())
    
    def on_save_as(self):
        """
        Save the current editor document as.
        """
        path = self.tabWidget.current_widget().file.path
        path = os.path.dirname(path) if path else ''
        filename, _ = QtWidgets.QFileDialog.getSaveFileName(self.tabWidget, 'Save', path)
        if filename:
            if os.path.splitext(filename)[1] != ".py":
                filename = os.path.splitext(filename)[0]+".py"
            #self.tabWidget.current_widget().file.path = filename
            self.tabWidget.save_current_as()
            #self.recent_files_manager.open_file(filename)
            #self.menu_recents.update_actions()
            #self.actionRun.setEnabled(True)
            #self.actionConfigure_run.setEnabled(True)
            #self._update_status_bar(self.tabWidget.current_widget())    
    
    def on_open(self):
        """
        Shows an open file dialog and open the file if the dialog was
        accepted.
        """
        path_to_open = os.path.dirname(os.path.abspath(sys.argv[0]))
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(self.tabWidget, 'Select file', path_to_open)
        if filename:
            self.open_file(filename)
        #self.actionRun.setEnabled(True)
        #self.actionConfigure_run.setEnabled(True)
    
    def on_cursor_pos_changed(self):
        editor = self.tabWidget.current_widget()
        if editor:
            l, c = TextHelper(editor).cursor_position()
            #self.lbl_cursor_pos.setText(
            #    '%d:%d' % (l + 1, c + 1))
        
    
    def on_run_selection(self):
        '''
        runs the selected code
        if no code is selected, it runs the current line
        '''
        if not self.script_thread.isRunning():
            self.ui.actionStop.setDisabled(False)
            self.ui.actionPause_script.setDisabled(False)
            editor = self.tabWidget.current_widget()
            self.script_thread.set_editor(editor)
            self.script_thread.set_mode("select")
            self.exec_highlighter.set_editor(editor)
            #we cannot delete highlighting because the script selection relies on highlighting
            #self.exec_highlighter.delete_all_highlights()
            # update status bar info
            self.script_status_widget.setText("Script running")
            self.script_status_widget.setStyleSheet("""QLabel { background-color : lime;}""")
            self.script_status_widget.setStyle((self.script_status_widget.style()))
            QtWidgets.QApplication.processEvents()
            
            self.script_thread.start()
        else:
            self.logger.warning("script already running")
    
    def on_run_all(self):
        '''
        runs the whole script
        if no code is selected, it runs the current line
        '''
        if not self.script_thread.isRunning():
            self.ui.actionStop.setDisabled(False)
            self.ui.actionPause_script.setDisabled(False)
            editor = self.tabWidget.current_widget()
            self.script_thread.set_editor(editor)
            self.script_thread.set_mode("all")
            self.exec_highlighter.set_editor(editor)
            self.exec_highlighter.delete_all_highlights()
            # update status bar info
            self.script_status_widget.setText("Script running")
            self.script_status_widget.setStyleSheet("""QLabel { background-color : lime;}""")
            self.script_status_widget.setStyle((self.script_status_widget.style()))
            QtWidgets.QApplication.processEvents()
            self.script_thread.start()
            
        else:
            self.logger.warning("script already running")
     
    def on_pause_execution(self):
        '''
        Pauses the execution by setting the variable pause_execution to True
        In the function execute_command() the execution_wait will not wake up the script thread in this case
        
        If pause_execute is allready True, it will resume the execution by calling execution_wait.wakeAll()
        '''
        if self.script_thread.isRunning():
            if self.pause_execution:
                self.pause_execution = False
                # update status bar info
                self.script_status_widget.setText("Script running")
                self.script_status_widget.setStyleSheet("""QLabel { background-color : lime;}""")
                self.script_status_widget.setStyle((self.script_status_widget.style()))
                QtWidgets.QApplication.processEvents()
                self.logger.info("script resumed")
                execution_wait.wakeAll()
            else:
                self.pause_execution = True
                # update status bar info
                self.script_status_widget.setText("Script paused")
                self.script_status_widget.setStyleSheet("""QLabel { background-color : yellow;}""")
                self.script_status_widget.setStyle((self.script_status_widget.style()))
                QtWidgets.QApplication.processEvents()
                self.logger.info("script paused")
                
    def on_stop_execution(self):
        '''
        emits self.stop_signal to stop script execution
        It also calls execution_wait.wakeAll() for the case that the script was paused
        '''
        if self.script_thread.isRunning():
            self.stop_signal.emit() 
            execution_wait.wakeAll()
            self.ui.actionStop.setDisabled(True)
            self.ui.actionPause_script.setDisabled(True)
            self.pause_execution = False
            # update status bar info
            self.script_status_widget.setText("Script stopped")
            self.script_status_widget.setStyleSheet("""QLabel { background-color : white}""")
            self.script_status_widget.setStyle((self.script_status_widget.style()))
            QtWidgets.QApplication.processEvents()
           
    @QtCore.pyqtSlot(int)
    def update_execution_highlighter(self,line_nr):
        '''
        updates the execution line highlighter. It does this under control of the execution mutex
        to make sure that the script waits until gighlighter is updated
        
        :param line_nr: line number
        :type line_nr: int
        '''
        execution_mutex.lock()
        self.exec_highlighter.refresh(line_nr) 
        execution_wait.wakeAll()
        execution_mutex.unlock()
    
    @QtCore.pyqtSlot(dict)
    def execute_command(self,_dict):
        '''
        This executes a command from the execution thread.        
        
        Executing the command from the main thread is mandatory if gui elements are going to be altered.
        The script if paused after execution if self.pause_execution==True by not calling execution_wait.wakeAll()
        
        :param _dict: dictionary containing the method and the arguments
        :type _dict: dict
        '''
        execution_mutex.lock()
        command=_dict["command"]
        args = _dict["args"]
        kwargs =  _dict["kwargs"]
        try:
            command(*args,**kwargs)
        except:
            self.logger.exception("execution error")
        if not self.pause_execution:
            execution_wait.wakeAll()
        execution_mutex.unlock()

    @QtCore.pyqtSlot()    
    def on_exec_thread_finished(self):
        '''
        Clears the execution line highlighter, refreshes the caret highlighter
        and disables the actionStop and actionPause_script actions after the script has finished. 
        '''
        self.ui.actionStop.setChecked(False)
        self.ui.actionStop.setDisabled(True)
        self.ui.actionRun_all.setChecked(False)
        self.ui.actionPause_script.setChecked(False)
        self.ui.actionPause_script.setDisabled(True)
        self.exec_highlighter.clear_deco()
        self.exec_highlighter.refresh_carethighlight()
        # update status bar info
        self.script_status_widget.setText("Script finished")
        self.script_status_widget.setStyleSheet("""QLabel { background-color : white}""")
        self.script_status_widget.setStyle((self.script_status_widget.style()))
        QtWidgets.QApplication.processEvents()
        
    def open_file(self, path, line=None):
        """
        Creates a new GenericCodeEdit, opens the requested file and adds it
        to the tab widget.

        :param path: Path of the file to open

        :return The opened editor if open succeeded.
        """
        editor = None
        if path:
            interpreter, pyserver, args = self._get_backend_parameters()
            editor = self.tabWidget.open_document(
                path, None, interpreter=interpreter, server_script=pyserver,
                args=args)
            if editor:
                self.setup_editor(editor)
            #self.recent_files_manager.open_file(path)
            #self.menu_recents.update_actions()
        if line is not None:
            TextHelper(self.tabWidget.current_widget()).goto_line(line)
        return editor    
    
    def _get_backend_parameters(self):
        """
        Gets the pyqode backend parameters (interpreter and script).
        """
        #hf.pydevBrk()
        frozen = hasattr(sys, 'frozen')
        interpreter = Settings().interpreter
        if frozen:
            interpreter = None
        pyserver = completion_server.__file__ if interpreter is not None else 'server.exe'
        syspath = os.path.dirname(os.getcwd())
        args = ["--syspath",syspath]
        return interpreter, pyserver, args
    



    def get_interpreters(self):
        if platform.system().lower() == 'linux':
            executables = [os.path.join('/usr/bin/', exe)
                           for exe in ['python2', 'python3']
                           if os.path.exists(os.path.join('/usr/bin/', exe))]
        else:
            executables = set()
            paths = os.environ['PATH'].split(';')
            for path in paths:
                if 'python' in path.lower():
                    if 'scripts' in path.lower():
                        path = os.path.abspath(os.path.join(path, os.pardir))
                executable = os.path.join(path, 'python.exe')
                if os.path.exists(executable):
                    executables.add(executable)
        return executables
    
    def on_goto_out_of_doc(self, assignment):
        """
        Open the a new tab when goto goes out of the current document.

        :param assignment: Destination
        """
        editor = self.open_file(assignment.module_path)
        if editor:
            TextHelper(editor).goto_line(assignment.line, assignment.column)

    def create_word_list(self):
        '''
        creates a list of all methods in in the RDPsimulator and
        signal_simulator classes and saves them in a file
        This file can be used for autocompletion.
        '''
        path=os.path.join(os.path.dirname(os.path.dirname(__file__)),"autocompleter_list.txt")
        word_list = set()
        for method in dir(self.parent):
            if method not in dir(QtWidgets.QMainWindow):
                word_list.add(method)       
        module_list = [self.parent.sig_sim]
        for module in module_list:
                for item in dir(module):
                    if not item.startswith("__"):
                        word_list.add(item)
        word_list = sorted(word_list)
        with open(path,"w") as f:
            for item in word_list:
                f.write("%s\n" % item)
        return path
            
        
        
        
class Settings():
    def __init__(self):
        self.settings = QtCore.QSettings('Volpi', 'RDP_simulator')

    @property
    def interpreter(self):
        interpreter = self.settings.value('interpreter', '')
        if not os.path.exists(interpreter):
            interpreter = ''
        return interpreter

    @interpreter.setter
    def interpreter(self, value):
        assert os.path.exists(value)
        self.settings.setValue('interpreter', value)

    @property
    def run_configs(self):
        """
        Returns the dictionary of run configurations. A run configuration is
        just a list of arguments to append to the run command.

        This is internally stored as a json object

        """
        string = self.settings.value('run_configs', '{}')
        return json.loads(string)

    @run_configs.setter
    def run_configs(self, value):
        self.settings.setValue('run_configs', json.dumps(value))

    def get_run_config_for_file(self, filename):
        try:
            dic = self.run_configs
            config = dic[filename]
        except KeyError:
            config = []
            self.set_run_config_for_file(filename, config)
        return config

    def set_run_config_for_file(self, filename, config):
        dic = self.run_configs
        dic[filename] = config
        self.run_configs = dic

class ScriptThread(QtCore.QThread):
    '''
    This thread executes the script from the script editor. It is crucial to run the script from its own thread. otherwise it wouldnt be possible to
    interact in any way with the gui during execution of the script
    '''
    #signal for execution line highlighting
    hl_signal = QtCore.pyqtSignal(int)
    #signal is emited if script is finished
    end_signal = QtCore.pyqtSignal()
    #signal is emited for each command in the script.
    #the command is send to the main thread where it will be executed.
    command_signal = QtCore.pyqtSignal(dict)
    
    def __init__(self,parent,logger):
        super(ScriptThread,self).__init__()
        #ScriptingMethods is a class with all the methods accessible from the scribt. 
        self.logger = logging.getLogger('ScriptThread')
        self.handler = ThreadHandler(logger,execution_mutex, execution_wait, self.command_signal)
        self.logger.setLevel('DEBUG')
        self.handler.setFormatter(logging.Formatter("%(levelname)s: %(message)s"))
        self.logger.addHandler(self.handler)
        
        self.sm = ScriptingMethods(parent,self.logger, execution_mutex, execution_wait,self.command_signal)
        self.script = ""
        #Editor which contains the script
        self.editor = None
        #mode is either "select" or "all" and decides wether the whole script or only the selection will be executed
        self.mode = "select"
        #mutex and axecution_wait to synchronise the threads
        self.execution_mutex = execution_mutex
        self.execution_wait= execution_wait
        #offset for line highlighter if only selected lines are executed
        self.line_offset = 0
        #if self.stop==True, the script stops
        self.stop=False
        self.set_globals()
        #connect the stop signal from the parent thread
        parent.stop_signal.connect(self.on_stop)
    
    def __del__(self):
        print("ScriptThread deleted")
    
    def set_editor(self,editor):
        '''
        Sets the editor from which the script should be executed
        :param editor: editor to use
        :type editor: PyCodeEdit
        '''
        self.editor = editor
    def set_globals(self):
        '''
        sets the ScriptingMethods instance to use
        :param sm: instance of ScriptingMethods
        :type sm: ScriptingMethods
        '''
        self.globals ={}
        self.globals["sm"]=self.sm
        #add own methods
        self.globals["st"]=self
        
    def set_mode(self,mode):
        '''
        sets the mode of the execution:
        mode = "all": whole script is executed
        mode = "select": only selected script is executed
        :param mode: "all" or "select"
        :type mode: str
        '''
        if mode in ["select","all"]:
            self.mode=mode
            
    def prepare_script(self):
        '''
        Prepares the script:
        -replaces u"\u2029" with newlines
        -removes the ScriptingMethods import statement
        -set self.offset to the line number of the first line of the selection range
        '''
        if self.mode == "select":
            script = TextHelper(self.editor).selected_text()
            self.offset, _ = TextHelper(self.editor).selection_range()
            #get whole line if nothing is selected
            if script == "":
                script = TextHelper(self.editor).current_line_text()
                self.offset = TextHelper(self.editor).current_line_nbr()
                
        else:
            script = self.editor.toPlainText()
            self.offset = 0
        self.script=script.replace(u"\u2029","\n")
        #remove whitespace
        self.script = textwrap.dedent(self.script)
        #=======================================================================
        # #replace methods from sm
        # regex = r"^(?P<indent>[ {4}]*)(?P<command>sm.[A-Za-z_]+\(.+\))"
        # replace = r"\g<indent>st.send_command('\g<command>')"
        # self.script=re.sub(regex, replace, self.script, 0, re.MULTILINE)
        #=======================================================================
        #remove import of sm
        import_string = "from RDP_simulator.scripting_methods import ScriptingMethods as sm #Do not run this line!!!\n"
        if import_string in self.script:
            self.offset+=1
            self.script=self.script.replace(import_string,"")

        
    def inject_code(self,stop_checker=True):
        '''
        This injects the code for highlighting the executed line and the stop_checker
        '''
        #hf.pydevBrk()
        #init code imports inspect module to get the line number
        init_code='''
        import inspect
        highlighter_index = 0
        '''
        #main code queries the line number and updates the highlighter accordingly
        code='''
        line_nr = inspect.currentframe().f_lineno
        st.update_hl(line_nr-highlighter_index*3-3+self.offset)
        '''

        #remove leading whitespaces
        init_code = textwrap.dedent(init_code)
        code = textwrap.dedent(code)
        #remove first newline
        init_code=init_code.split("\n",1)[1]
        code=code.split("\n",1)[1]
        #add stop_checker which stops the script if self.stop is true
        if stop_checker:
            code = code+'if st.stop: raise SystemExit(0)'

        #replace self.offset
        code = code.replace("self.offset","{:d}".format(self.offset))
        #hf.pydevBrk()
        #take care of the indentation level when injecting the highlighter code
        #=======================================================================
        # zum das problem mit den white lines zuu loesen
        # folgendes versuchen:
        # regex= (^(?: {4})*)(\w+)(\R)
        # sust= \1highlighter_code()\3\1\2\n
        #=======================================================================
        regex=r"(^(?: {4})*)(.*)"
        class substituter():
            '''
            substitutes the the regex matches with incrementing highlighter index 
            '''
            def __init__(self,code):
                self.code = code
                self.highlighter_index = 0
                self.last_indent=0
                self.nr_round_parenthesis=0
                self.nr_square_parenthesis=0
                self.nr_curly_parenthesis=0
            def substitute(self,matchobj):
                #indent of this line
                actual_indent = matchobj.groups()[0]
                #code in this line without indent
                code =  matchobj.groups()[1]
                #if code in this line is a control flow statement, use indent of last line
                #otherwise the injected code breaks to control flow
                if any(control in matchobj.groups()[1] for control in ["else:","elif","pass","except","finally:"]):
                #if matchobj.groups()[1] in ["else:","elif","pass","except","finally:"]:
                    indent=self.last_indent
                else: 
                    indent = actual_indent
                self.last_indent=indent
                if (self.nr_round_parenthesis==0) and (self.nr_square_parenthesis==0) and (self.nr_curly_parenthesis==0):
                    c = self.code.replace("highlighter_index","{:d}".format(self.highlighter_index))
                    c = indent+c.replace("\n","\n"+indent)+"\n"+actual_indent+code
                    self.highlighter_index+=1
                else:
                    c = indent+code
                    #self.highlighter_index+=1
                
                self.nr_round_parenthesis+=code.count('(')-code.count(')')
                self.nr_square_parenthesis+=code.count('[')-code.count(']')
                self.nr_curly_parenthesis+=code.count('{')-code.count('}')
                return c
        #hf.pydevBrk()
        subst=substituter(code)
        self.script=re.sub(regex, subst.substitute, self.script, 0, re.MULTILINE)
        #concatenate code
        #hf.pydevBrk()
        self.script=init_code+self.script
        #print self.script
        #self.logger.info(self.script)
        
    def update_hl(self,line_nr):
        '''
        This updates the execution line highlighter by emiting a signal to the main thread
        under control of the mutex
        :param line_nr: line number to highlight
        :type line_nr: int
        '''
        
        self.execution_mutex.lock()
        self.hl_signal.emit(line_nr)
        self.execution_wait.wait(self.execution_mutex)
        self.execution_mutex.unlock()
          
    def execute_script(self):
        '''
        executes the script in the scope of self.globals
        '''
        #remove empty lines
        #02.07.2018
        #I dont know why i decided to remove empty lines. 
        #This breaks hilighting. The highlighted line will no longer be the one which is executed.
        #Therefore I comment it out now
        #self.script = re.sub(r'^\s*\n', '', self.script, flags=re.MULTILINE)
        main_folder=os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        file_path=os.path.join(main_folder,"running_script.py")
        with open(file_path,"w") as file_out:
            file_out.write(self.script)
        try:
            code=compile(self.script,"<string>","exec")
        except:
            self.logger.exception("compile error")
        else:
            try:
                exec(code, self.globals)
            except SystemExit:
                pass
            except Exception as e:
                self.logger.exception("execution error")
                
    def on_stop(self):
        '''
        slot to set self.stop=True, which stops the script
        '''
        self.stop=True
        
    def run(self):
        '''
        working method of the thread.
        '''
        self.stop=False
        self. prepare_script()
        #self.add_stop_checker()
        self.inject_code(True)
        #print self.script
        self.execute_script()
        self.end_signal.emit()
        self.logger.info("script finished")



class ExecutionHighlighter():
    """ Highlights the currently executed line """
    def __init__(self):
        self._decoration = None
        self._pos = -1
        self.color = QtGui.QColor(0, 255, 0, 127)
        self.editor=None
    
    def set_editor(self,editor):
        self.editor=editor

    def clear_deco(self):
        """ Clear line decoration """
        if self._decoration:
            self.editor.decorations.remove(self._decoration)
            self._decoration = None
            
    def delete_all_highlights(self):
        '''
        deletes highlights in the editor due to selection and caret highlighting
        '''
        try:
            chl=self.editor.modes.get(coremodes.CaretLineHighlighterMode)
        except KeyError:
            pass
        else:
            chl._clear_deco()
            
        TextHelper(self.editor).clear_selection()
    
    def refresh_carethighlight(self):
        '''
        refreshes caret highlighting
        '''
        try:
            chl=self.editor.modes.get(coremodes.CaretLineHighlighterMode)
        except KeyError:
            pass
        else:
            chl.refresh()

    def refresh(self,line_nr):
        """
        Updates the current line decoration
        """
        self.clear_deco()
        
        brush = QtGui.QBrush(self.color)
        block = self.editor.document().findBlockByNumber(line_nr)
        #block is -1 if not valid (wrong line_nr)
        if block !=-1:
            self._decoration = TextDecoration(block)
            self._decoration.set_background(brush)
            self._decoration.set_full_width()
            self.editor.decorations.append(self._decoration)

    def clone_settings(self, original):
        self.background = original.background
        

        
class ThreadHandler(logging.Handler):
    '''
    This is a special logger handler which can used in threads like ScriptThread.
    It calls the logger function from the main thread via the command_signal signal. 
    It works the same way as the functions in scripting_methods.py.
    This is needed becaus we are not allow the alter gui elements from the ScriptThread thread. 
    '''
    #logging handler. oeffnet den error dialog
        
    
    def __init__(self, logger,execution_mutex, execution_wait,command_signal):
        logging.Handler.__init__(self)
        self.logger = logger
        self.execution_mutex = execution_mutex
        self.execution_wait = execution_wait
        self.command_signal = command_signal
        self.logging_dict = { "DEBUG" : self.logger.debug,
                        "CRITICAL": self.logger.fatal, 
                        "ERROR":self.logger.error,
                        "WARNING": self.logger.warning,
                        "WARN": self.logger.warn,
                        "INFO":self.logger.info}
        
    def emit(self, record):
        record_str = self.format(record)
        
        command = self.logging_dict[record.levelname]
        #if record: XStream.stdout().write('%s\n'%record)
        self._send_command(command,record_str)
        # originally: XStream.stdout().write("{}\n".format(record))
        
    def _send_command(self,command,*args,**kwargs):
        '''
        emits the signal command_signal with the command and the arguments under
        control of the mutex
        :param command: command to execute
        :type command: function
        '''
        self.execution_mutex.lock()
        self.command_signal.emit({"command":command,"args":args,"kwargs":kwargs})
        self.execution_wait.wait(self.execution_mutex)
        self.execution_mutex.unlock()
    