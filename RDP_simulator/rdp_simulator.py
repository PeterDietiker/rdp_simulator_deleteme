# coding: utf-8
'''
This module provides the class RDPsimulator which is a QT application and serves as a GUI.
'''

# import PySide  ## this will force pyqtgraph to use PySide instead of PyQt4
import sys
import sip
import os
import types
from io import IOBase
import io

#replacing the excepthook helps to debug exceptions in PyQT
#from https://fman.io/blog/pyqt-excepthook/
import traceback
from collections import namedtuple
from builtins import object
def excepthook(exc_type, exc_value, exc_tb):
    enriched_tb = _add_missing_frames(exc_tb) if exc_tb else exc_tb
    # Note: sys.__excepthook__(...) would not work here.
    # We need to use print_exception(...):
    traceback.print_exception(exc_type, exc_value, enriched_tb)

def _add_missing_frames(tb):
    result = fake_tb(tb.tb_frame, tb.tb_lasti, tb.tb_lineno, tb.tb_next)
    frame = tb.tb_frame.f_back
    while frame:
        result = fake_tb(frame, frame.f_lasti, frame.f_lineno, result)
        frame = frame.f_back
    return result

fake_tb = namedtuple(
    'fake_tb', ('tb_frame', 'tb_lasti', 'tb_lineno', 'tb_next')
)

sys.excepthook = excepthook

os.environ["QT_API"] = "pyqt5"
from pyqode.qt import QtCore, QtGui, QtWidgets 
import pyqtgraph as pg

#limit the number of threads used by numpy
os.environ["MKL_NUM_THREADS"] = "1" 
os.environ["NUMEXPR_NUM_THREADS"] = "1" 
os.environ["OMP_NUM_THREADS"] = "1" 
import numpy as np
import subprocess
import pickle
import logging
import time


# import modules
if __package__ is None:
    try:
        __file__
    except NameError:
        #loaded from interactive shell
        sys.path.insert(1, os.path.join(os.getcwd(), os.path.pardir))
    else:
        sys.path.insert(1, os.path.join(os.path.dirname(__file__), os.path.pardir))
    __package__ = 'RDP_simulator'  # @ReservedAssignment
    import RDP_simulator  # @UnusedImport
#     print sys.path
import RDP_simulator.helper_functions  as hf
from RDP_simulator.helper_functions import wait
from RDP_simulator.signal_simulator import signal_simulator
from RDP_simulator.error_handler import error_handler
from RDP_simulator.gui import gui
#from simulator.pls2 import pls2
from RDP_simulator.pyqode_plugin import pyqode_handler

# global option for pyqtgraph
pg.setConfigOption('background', 'w')

class pls2(object):
    def __init__(self):
        self.initialised = False

class RDPsimulator(QtWidgets.QMainWindow):  #pylint: disable=too-many-instance-attributes, too-many-public-methods
    '''
    This is the DGA simulator. It is a PyQT4 (PySide) application which allows for the simulation of Laser spectra including
    effects of drift and laser lineshape.
    A library for Partial least square fits is provided wich allows for the fit of the concentrations.
    '''

    def __init__(self):
        super(RDPsimulator, self).__init__()
        #hf.pydevBrk()
        # Create the main window
        self.ui = gui.Ui_MainWindow()
        self.ui.setupUi(self)
        
        # init error handler 
        self.error_handler = error_handler(self)
    
        
        self.center()
        self.setWindowTitle('RDP simulator')
        #Hide unused Widgets
        self.ui.PLS2_measurement_parameters.hide()
        self.ui.PLS2_calibration_parameters.hide()
        self.ui.PLS2_processing_parameters.hide()
        self.ui.PLS2_wl_calibration_parameters.hide()
        # self.ui.select_tuning_file_button.clicked.connect(self.plot)

        # self.ui.Plot.getPlotItem().showAxis('right')
        # self.ui.Plot.getPlotItem().showAxis('top')

        
        # init spectrum simulator
        self.sig_sim = signal_simulator(
            self.error_handler.logger)
        # init PLS2 class

        #self.pls2 = pls2.PLS2(self.error_handler.logger)
        self.pls2 = pls2()
        

        # show PID (for easy connection with debugger)
        pid = os.getpid()
        self.ui.menuPID.setTitle("PID: " + str(pid))
        
        # init controls
        self.init_controls()

        # init the timer which controls the rate at which measurements are
        # taken (simulated)
        self.init_measurement_timer()

        # init plots
        self.init_counts_plot()
        self.init_nr_events_plot()
        self.init_results_plot()
        #self.init_absorbance_plot()
        #self.init_drift_plot()
        #self.init_tuning_curve_plot()
        self.init_path_plot()
        self.init_histogram_plots()
        self.init_histogram_signal_plot()
        #self.init_power_spectrum_plot()
        #self.init_noise_kernel_plot()
        #self.init_pls_2_absorbance_fit_plot()

        # connect spectrum_simulator controls
        self.connect_controls()
        #self.connect_drift_parameters()
        #self.connect_drift_actual_time()

        # connect simulation controls
        self.connect_simulation_controls()

        # some variables
        # last simulation time, used to measure speed
        self.last_time = pg.ptime.time()
        self.fps = 1  # last frames per second

        self.nr_measurements_acquired = 0  # number of acquired measurements
        # number of measurements to acquire, 0=inf
        self.nr_measurements_to_acquire = 0

        # allan plot process
        # allan plot is handled in a new process as computations are quite
        # heavy
        self.allan_proc = None
        self.concentration_plot_items = None #items of the concentration plot graph
        self.components_to_plot = [] #index of components to plot in the  concentration plot graph
        
        #handler for infos from pyqode_plugin
        self.out_handler = error_handler(self.ui.Console_textBrowser, level = logging.INFO, name = "stdout")
        #init handler for pyqode widget 
        self.pyqode_handler = pyqode_handler(self.ui.Editor_tabWidget,self)
        # show window
        self.show()
        
        #open a new document in pyqode widget
        #I have to open a deocument after self.show() otherwise the completion server
        #will not shut down upon closing the dga simulator 
        self.pyqode_handler.on_new()
        main_folder = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        file_path = os.path.join(main_folder,"RDP_simulator","example_script.py")
        self.pyqode_handler.open_file(file_path, 1)
        
        
        #redirect sdtout
        #self.stdout_handler = error_handler(self.ui.Console_textBrowser, level = logging.INFO, name = "stdout")
        #sys.stdout = self.stdout_handler
        #redirect stderr
        #cannot redirect stderr because this elads to buffer overflow
        #self.stderr_handler = error_handler(self.ui.Console_textBrowser, level = logging.ERROR, name = "stderr")
        #sys.stderr = self.stderr_handler
        #sys.stderr = self.out_handler

    def __del__(self):
        #import objgraph
        #import gc
        #objgraph.show_refs([self.pyqode_handler], filename='pyqode_handler-graph.png')
        #del all the class which span their own processes
        #if self.allan_proc is not None:
            #self.allan_proc.terminate()
        
        #self.pyqode_handler.tabWidget.close_all()
        #print("pyqode_handler refcount: {}".format(sys.getrefcount(self.pyqode_handler)))
        #gc.collect()
        #print(gc.get_referrers(self.pyqode_handler))
        del self.pyqode_handler
        #print("sig_sim refcount: {}".format(sys.getrefcount(self.sig_sim)))
        del self.sig_sim
        #print("pls2 refcount: {}".format(sys.getrefcount(self.pls2)))
        del self.pls2
        
        

    def center(self):
        '''
        centers the window on the activ screen
        '''
        frame_geom = self.frameGeometry()
        screen = QtWidgets.QApplication.desktop().screenNumber(QtWidgets.QApplication.desktop().cursor().pos())
        center_point = QtWidgets.QApplication.desktop().screenGeometry(screen).center()
        frame_geom.moveCenter(center_point)
        self.move(frame_geom.topLeft())

    def init_counts_plot(self):
        '''
        Initialises the plot of the transmission signal
        '''
        for plot in [self.ui.counts_plot]:
            plot.getPlotItem().setTitle('Counts / bin', color='000')
            plot.getPlotItem().setLabel('left', 'counts / bin')
            plot.getPlotItem().setLabel('bottom', 'Bin / #')
            for axis in ['left', 'right', 'top', 'bottom']:
                plot.getPlotItem().getAxis(axis).setPen(pg.mkPen(0, 0, 0))
            plot.getPlotItem().showButtons()
            # AutoRange of y-scale is disabled for performance
            plot.enableAutoRange('y', False)
            plot.setYRange(-10, 1050)
            #self.channel2_item = pg.PlotCurveItem([], pen='b', name='channel_2')
            self.channel1_item = pg.PlotCurveItem([], pen='r', name='channel_1')
            self.threshold_item = pg.PlotCurveItem([], pen='r', name='channel_1')
            #plot.addItem(self.channel2_item)
            plot.addItem(self.channel1_item)
            plot.addItem(self.threshold_item)
        #stuff for showing actual values
        label = hf.InfoText(self.ui.counts_plot)
        self.ui.counts_plot.addItem(label,ignoreBounds=True)
        label.setText("")
        label.update_position()
                

    def init_results_plot(self):
        '''
        Initialises the absorbance plot
        '''
        self.ui.results_plt.fig.suptitle('Results', fontsize=12)
        # show coordinate does not work...
        '''
        def mouse_move(event):
            if not event.inaxes:
                return

            x, y = event.xdata, event.ydata

            self.ui.results_plt.fig.suptitle('Absorbance: x=%1.2f, y=%1.2f' % (x, y))
            self.ui.results_plt.redraw()
        self.ui.results_plt.fig.canvas.mpl_connect('motion_notify_event', mouse_move)
        '''
        #self.plot_results(init=True, x_axis = 'index')

    def init_tuning_curve_plot(self):
        '''
        Initialises the tuning curve plot
        '''
        self.ui.tuning_curve_plt.fig.suptitle('Tuning curve', fontsize=12)
        self.plot_tuning_curve(init=True)

    def init_path_plot(self):
        '''
        Initialises the lineshape plot
        '''
        self.ui.path_plt.fig.suptitle('Path', fontsize=12)
        self.plot_path(init=True)
    
    def init_histogram_plots(self):
        '''
        Initialises the histogram plot
        '''
        import matplotlib.gridspec as gridspec
        self.ui.bin_histogram_plt.axes.remove()
        gs = gridspec.GridSpec(2, 2)
        self.ui.bin_histogram_plt.axes = []
        
        self.ui.bin_histogram_plt.axes.append(self.ui.bin_histogram_plt.fig.add_subplot(gs[0, 0]))
        self.ui.bin_histogram_plt.axes.append(self.ui.bin_histogram_plt.fig.add_subplot(gs[1, 0]))
        self.ui.bin_histogram_plt.axes.append(self.ui.bin_histogram_plt.fig.add_subplot(gs[1, 1]))

        self.ui.bin_histogram_plt.fig.suptitle('Bin histograms', fontsize=12)
        self.plot_bin_histogram(init=True)
    
    def init_histogram_signal_plot(self):
        self.ui.histogram_signal_plot.fig.suptitle("Histogram",fontsize=12)
        self.ui.histogram_signal_plot.axes.autoscale(enable=False, axis='y')
        self.ui.histogram_signal_plot.axes.autoscale(enable=False, axis='x')
        self.ui.histogram_signal_plot.axes.set_xlim(0,120)
        self.ui.histogram_signal_plot.axes.set_ylim(0,100)

    def init_power_spectrum_plot(self):
        '''
        initialises the power spectrum plot
        '''
        self.ui.power_spectrum_plt.fig.suptitle('Power spectrum', fontsize=12)
        self.plot_power_spectrum(init=True)

    def init_noise_kernel_plot(self):
        '''
        initialieses the noise kernel plot
        '''
        self.ui.noise_kernel_plt.fig.suptitle('Noise kernel', fontsize=12)
        self.plot_noise_kernel(init=True)

    def init_pls_2_absorbance_fit_plot(self):
        '''
        initalises the plot of the PLS2 absorbance, fitted absorbance and residuals.
        '''
        plot = self.ui.PLS2_absorbance_fit_plot
        plot.addLegend()
        plot.getPlotItem().setTitle('Absorbance', color='000')
        plot.getPlotItem().setLabel('left', 'Absorbance')
        plot.getPlotItem().setLabel('bottom', 'Piezo / V')
        for axis in ['left', 'right', 'top', 'bottom']:
            plot.getPlotItem().getAxis(axis).setPen(pg.mkPen(0, 0, 0))
        plot.getPlotItem().showButtons()
        # add item for measurement and fitted absorbance
        self.absorbance_item = pg.PlotCurveItem([0], pen='b', name='measurement')
        self.fitted_absorbance_item = pg.PlotCurveItem([], pen='r', name='fitted absorbance')
        plot.addItem(self.absorbance_item)
        plot.addItem(self.fitted_absorbance_item)
        # residuals plots
        self.residuals_vb = pg.ViewBox()
        plot.getPlotItem().scene().addItem(self.residuals_vb)
        plot.getPlotItem().showAxis('right')
        plot.getPlotItem().getAxis('right').linkToView(self.residuals_vb)
        self.residuals_vb.setXLink(plot.getPlotItem())
        plot.getPlotItem().getAxis('right').setLabel('residuals')
        self.residuals_item = pg.PlotCurveItem([], pen='g', name='residuals')
        self.residuals_vb.addItem(self.residuals_item)
        plot.getPlotItem().vb.sigResized.connect(lambda: hf.updateView(plot.getPlotItem().vb, self.residuals_vb))
        # add legend item
        plot.getPlotItem().legend.addItem(self.residuals_item, 'residuals')
        #add info text
        '''we have more than one instance of hf.InfoText and each one needs its own update_text method. 
        thats why have to monkey patch this method in every instance. I am not sure if this is  the most elegant design 
        but it works'''
        #new update_text class method
        def update_text(self,evt):
            '''
            Updates the shown information
            :param evt: sigMouseClicked event
            :type evt: sigMouseClicked event
            '''
            pos = evt._scenePos
            mousePoint = self.plot.getViewBox().mapSceneToView(pos)
            xData_abs, yData_abs = self.plot.listDataItems()[0].getData()
            idx = np.searchsorted(xData_abs,  mousePoint.x(), side="left")-1
            if (idx>=0) and (idx < len(xData_abs)):
                self.setText("Index: {:d}\nWavelength {:1.4f} um\nAbsorbance: {:02.2f}".format(idx,xData_abs[idx],yData_abs[idx]))
            else:
                self.setText("")
        label = hf.InfoText(plot)
        #disconnect signal to be able to monkeypatch the method
        plot.scene().sigMouseClicked.disconnect(label.update_text)
        #Monkey patch update_text Method
        label.update_text=types.MethodType(update_text,label)
        #reconnect signal
        plot.scene().sigMouseClicked.connect(label.update_text)
        plot.addItem(label,ignoreBounds=True)
        label.setText("")
        label.update_position()
        

    def init_nr_events_plot(self):
        '''
        Initialises the plot of the concentrations
        '''
        plot = self.ui.nr_events_plot
        # try to remove the legend (in order to set new component names)
        try:
            plot.plotItem.clear()
            plot.plotItem.legend.scene().removeItem(plot.plotItem.legend)
        except AttributeError:
            pass  # no legend

        #plot.addLegend()
        plot.getPlotItem().setTitle('# events', color='000')
        plot.getPlotItem().setLabel('left', '# events')
        plot.getPlotItem().setLabel('bottom', 'measurements')
        for axis in ['left', 'right', 'top', 'bottom']:
            plot.getPlotItem().getAxis(axis).setPen(pg.mkPen(0, 0, 0))
        plot.getPlotItem().showButtons()
        nr_of_components = 1
        colors = ['b', 'r', 'g', 'y']
        self.nr_events_item = pg.PlotCurveItem([0], pen=colors[0 % len(colors)], name="nr_events")
        plot.addItem(self.nr_events_item)
        plot.setXRange( 0, self.ui.nr_events_statistics_size.value())

    def init_pls_2_wavelength_calibration_plot(self):
        '''
        Init the wavelength calibration plot
        '''
        self.ui.PLS2_wl_calibration_plot.fig.suptitle(
            'wavelength calibration', fontsize=12)
        self.plot_pls_2_wavelength_calibration(init=True)

    def plot_absorbances(self, init=False):
        '''
        update the absorbance plot with new data
        :param init: If True, the plot is reinitialised
        :type init: bool
        '''
        self.sig_sim.plot_absorbance_spectrum(ax=self.ui.results_plt.axes, init=init)
        self.ui.results_plt.redraw()

    def plot_tuning_curve(self, init=False):
        '''
        update the tuning curve plot with new data
        :param init: If True, the plot is reinitialised
        :type init: bool
        '''
        self.sig_sim.plot_piezo_tuning_curve(ax=self.ui.tuning_curve_plt.axes, init=init, drifted=True)
        self.ui.tuning_curve_plt.redraw()

    def init_drift_plot(self):
        '''
        Initialises the drift plot
        '''
        self.plot_drift(init=True)

    def plot_drift(self, init=False, recalc=False):
        '''
        update the drift plot with new data
        :param init: If True, the plot is reinitialised
        :type init: bool
        :param recalc:recalculates the drift if true
        :type recalc:bool
        '''
        self.sig_sim.plot_drift_curve(ax=self.ui.drift_plt, init=init, recalc=recalc, pyqtgraph=True)

    def plot_path(self, init=False):
        '''
        update the path plot with new data
        :param init: If True, the plot is reinitialised
        :type init: bool
        '''
        self.sig_sim.plot_path(ax=self.ui.path_plt.axes, init=init)
        self.ui.path_plt.redraw()


    def plot_bin_histogram(self, init=False):
        '''
        update the histogram plot with new data
        :param init: If True, the plot is reinitialised
        :type init: bool
        '''
        self.sig_sim.plot_bin_histograms(axs=self.ui.bin_histogram_plt.axes, init=init)
        self.ui.bin_histogram_plt.redraw()
        
        
    def plot_results(self,x_axis,init=False,clear = False,**kwargs):
        '''
        Plots the results
        :param x_axis: paramter to use as x axis
        :param init: If True, the plot is reinitialised
        '''
        if clear:
            self.ui.results_plt.axes.clear()
            
        self.sig_sim.results_df.plot_results(ax=self.ui.results_plt.axes, x_axis = x_axis, init = init,**kwargs)
        self.ui.results_plt.redraw()
        
        
    def plot_power_spectrum(self, init=False):
        '''
        update the  power spectrum plot with new data
        :param init: If True, the plot is reinitialised
        :type init: bool
        '''
        self.sig_sim.plot_power_spectrum(ax=self.ui.power_spectrum_plt.axes, init=init)
        self.ui.power_spectrum_plt.redraw()
        
    def plot_histogram_signal(self,init=False):
        self.sig_sim.plot_bin_histogram_signal(ax=self.ui.histogram_signal_plot.axes,init=init)
        self.ui.histogram_signal_plot.redraw()

    def plot_noise_kernel(self, init=False, axes=('1')):
        '''
        update the noise kernel plot with new data
        :param init: If True, the plot is reinitialised
        :type init: bool
        '''
        self.sig_sim.plot_noise_pdf(ax=self.ui.noise_kernel_plt.axes, init=init, axes=axes)
        self.ui.noise_kernel_plt.redraw()

    def plot_channels(self):
        '''
        update the transmission spectrum plot with new data
        '''
        # calculation of frame per seconds (from
        # pyqtgraph/examples/PlotSpeedTest.py)
        now = pg.ptime.time()
        dt = now - self.last_time
        self.last_time = pg.ptime.time()
        s = np.clip(dt * 3., 0, 1)
        self.fps = self.fps * (1 - s) + (1.0 / dt) * s
        self.ui.counts_plot.setTitle(
            'Counts / bin' + '  fps: ' + '{:2.1f}'.format(self.fps), color='000')
        x_axis, counts= self.sig_sim.return_counts()
        #self.channel2_item.setData(x_axis, channel_2)
        self.channel1_item.setData(x_axis, counts)
        threshold = self.sig_sim.result_dict['threshold_counts']
        self.threshold_item.setData(x_axis,np.repeat([threshold],len(x_axis)))
        # self.ui.channel_1_plt.plot(x_axis,channel_1, clear=True, pen=pg.mkPen('b'))
        # self.ui.channel_1_plt.plot(x_axis,channel_2, clear=False, pen=pg.mkPen('r'))

    def plot_pls_2_absorbance_fit(self):
        '''
        update absorbance, fitted absorbance and residuals plot
        '''
        plot = self.ui.PLS2_absorbance_fit_plot
        # get new data
        y_array = self.pls2.forward_absorbance
        # if wavelength is calibrateed, set x label
        if self.pls2.wavelength_calibration_status:
            x_array = self.pls2.wl_wavelengths
            plot.getPlotItem().setLabel('bottom', 'Wavelength / um')
        else:
            x_array, _, _, _= self.sig_sim.return_spectrum()
            plot.getPlotItem().setLabel('bottom', 'Piezo / V')
            self.fitted_absorbance_item.setData([], [])
            self.residuals_item.setData([], [])
#         print 'shape x: {:d}'.format(x_array.shape[0])
#         print 'shape y: {:d}'.format(y_array.shape[0])
        # update data
        self.absorbance_item.setData(x_array, y_array)
        if self.pls2.calc_concentrations:
            if self.pls2.new_concentrations_available:
                x_array = self.pls2.model_wavenumbers
                y_array = self.pls2.fitted_absorbance
                # plot.plot(x_array,y_array, clear=False, pen=pg.mkPen('r'),name='interpolated absorption')
                self.fitted_absorbance_item.setData(x_array, y_array)
                # set new _data to plot item
                self.residuals_item.setData(x_array, self.pls2.residuals)
        else:
            self.fitted_absorbance_item.setData([], [])
            self.residuals_item.setData([], [])

    def plot_nr_events(self):
        '''
        update concentrations plot
        '''
        data = self.sig_sim.nr_events_buffer.get_all_valid().flatten()
        plot = self.ui.nr_events_plot
        # set the x range automatically
        if (data.shape[0] > 1):
            if plot.getPlotItem().getViewBox().getState()['autoRange'][0]:
                if plot.getAxis('bottom').range[1] < data.shape[0]:
                    plot.setXRange(0, data.shape[0] * 2)
        self.nr_events_item.setData(np.arange(data.shape[0]),data)


    def plot_pls_2_wavelength_calibration(self, init=False):
        '''
        update wavelength calibration plot
        '''
        self.pls2.plot_wavelength_calibration(ax=self.ui.PLS2_wl_calibration_plot.axes, init=init)
        self.ui.PLS2_wl_calibration_plot.redraw()

    def connect_controls(self):
        '''
        connects the slots to the signals of the controls of sig_sim
        '''
        
        # #Global parameters
        self.ui.time_resolution.editingFinished.connect(self.connect_additional_parameters)
        '''
        # Gas concentrations
        self.ui.component_1_conc.valueChanged.connect(lambda: self.connect_concentrations(
            str(self.ui.component_1.currentText()), self.ui.component_1_conc.value(), 1))
        self.ui.component_2_conc.valueChanged.connect(lambda: self.connect_concentrations(
            str(self.ui.component_2.currentText()), self.ui.component_2_conc.value(), 1))
        self.ui.component_3_conc.valueChanged.connect(lambda: self.connect_concentrations(
            str(self.ui.component_3.currentText()), self.ui.component_3_conc.value(), 1))
        self.ui.component_4_conc.valueChanged.connect(lambda: self.connect_concentrations(
            str(self.ui.component_4.currentText()), self.ui.component_4_conc.value(), 1))
        self.ui.component_5_conc.valueChanged.connect(lambda: self.connect_concentrations(
            str(self.ui.component_5.currentText()), self.ui.component_5_conc.value(), 1))

        self.ui.component_1_conc_2.valueChanged.connect(lambda: self.connect_concentrations(
            str(self.ui.component_1.currentText()), self.ui.component_1_conc_2.value(), 2))
        self.ui.component_2_conc_2.valueChanged.connect(lambda: self.connect_concentrations(
            str(self.ui.component_2.currentText()), self.ui.component_2_conc_2.value(), 2))
        self.ui.component_3_conc_2.valueChanged.connect(lambda: self.connect_concentrations(
            str(self.ui.component_3.currentText()), self.ui.component_3_conc_2.value(), 2))
        self.ui.component_4_conc_2.valueChanged.connect(lambda: self.connect_concentrations(
            str(self.ui.component_4.currentText()), self.ui.component_4_conc_2.value(), 2))
        self.ui.component_5_conc_2.valueChanged.connect(lambda: self.connect_concentrations(
            str(self.ui.component_5.currentText()), self.ui.component_5_conc_2.value(), 2))
            '''
        
        # Noise parameters
        self.ui.noise_method.currentIndexChanged.connect(self.connect_noise_parameters)
        self.ui.noise_std.editingFinished.connect(self.connect_noise_parameters)
        self.ui.noise_mean.editingFinished.connect(self.connect_noise_parameters)
        self.ui.detector_darknoise.editingFinished.connect(self.connect_noise_parameters)
        self.ui.calc_single_count.clicked.connect(self.connect_calc_parameters)
        self.ui.scale_noise.clicked.connect(self.connect_noise_parameters)
        self.ui.noise_file_path.textChanged.connect(self.connect_noise_parameters)
        
        # scan parameters
        self.ui.xy_scan_distance.editingFinished.connect(self.connect_scan_parameters)
        self.ui.xy_scan_speed.editingFinished.connect(self.connect_scan_parameters)
        self.ui.objective_spin_radius.editingFinished.connect(self.connect_scan_parameters)
        self.ui.objective_spin_speed.editingFinished.connect(self.connect_scan_parameters)
        self.ui.scan_time.editingFinished.connect(self.connect_scan_parameters)
        self.ui.bin_time.editingFinished.connect(self.connect_scan_parameters)

        # optics parameters
        self.ui.f_objective.editingFinished.connect(self.connect_optics_parameters)
        self.ui.na_objective.editingFinished.connect(self.connect_optics_parameters)
        self.ui.f_laser_collimation.editingFinished.connect(self.connect_optics_parameters)
        self.ui.f_relay.editingFinished.connect(self.connect_optics_parameters)
        self.ui.r_pinhole.editingFinished.connect(self.connect_optics_parameters)
        self.ui.detector_efficiency.editingFinished.connect(self.connect_optics_parameters)
        self.ui.optics_efficiency.editingFinished.connect(self.connect_optics_parameters)
        self.ui.laser_collimation_dist.editingFinished.connect(self.connect_optics_parameters)
        self.ui.laser_to_objective_dist.editingFinished.connect(self.connect_optics_parameters)
        self.ui.detector_dead_time.editingFinished.connect(self.connect_optics_parameters)
        self.ui.select_eef_LineEdit.textChanged.connect(self.connect_optics_parameters)
        self.ui.select_eef_pushButton.clicked.connect(
                lambda: self.select_file(self.ui.select_eef_LineEdit, mode='open'))

        # dye parameters
        self.ui.dye_selector.currentIndexChanged.connect(self.connect_dye_parameters)
        self.ui.dye_concentration.editingFinished.connect(self.connect_dye_parameters)
        self.ui.dye_epsilon.editingFinished.connect(self.connect_dye_parameters)
        self.ui.dye_lifetime.editingFinished.connect(self.connect_dye_parameters)
        self.ui.dye_lifetime_isc.editingFinished.connect(self.connect_dye_parameters)
        self.ui.dye_lifetime_triplet.editingFinished.connect(self.connect_dye_parameters)
        self.ui.dye_quantum_efficiency.editingFinished.connect(self.connect_dye_parameters)
        
        # lineshape parameters
        self.ui.laser_power.editingFinished.connect(self.connect_laser_parameters)
        self.ui.laser_wavelength.editingFinished.connect(self.connect_laser_parameters)
        self.ui.laser_waist.editingFinished.connect(self.connect_laser_parameters)
        self.ui.laser_m_squared.editingFinished.connect(self.connect_laser_parameters)
        
        
        #statistics parameters
        self.ui.nr_events_statistics_size.editingFinished.connect(self.update_statistics_size)
        #self.ui.nr_events_statistics_size.valueChanged.connect(self.update_statistics_size)
        self.ui.nr_events_reset_statistics_button.clicked.connect(self.reset_statistics)
        
        # calc parameters
        self.ui.calc_method.currentIndexChanged.connect(self.connect_calc_parameters)
        self.ui.calc_stats.currentIndexChanged.connect(self.connect_calc_parameters)
        self.ui.calc_threshold.editingFinished.connect(self.connect_calc_parameters)
        self.ui.calc_normalisation.editingFinished.connect(self.connect_calc_parameters)
        self.ui.calc_single_count.clicked.connect(self.connect_calc_parameters)
        '''
        # PLS2
        # PLS2 control
        self.ui.PLS2_init_Library.stateChanged.connect(lambda: self.init_pls2(self.ui.PLS2_init_Library.isChecked()))
        # stream control
        '''

        def select_and_update():
            '''
            Opens the select file dialog and updates the streaming parameters afterwards.
            '''
            if str(self.ui.simulation_stream_mode.currentText()) == 'load':
                mode = 'open'
            else:
                mode = 'save'
            self.select_file(self.ui.simulation_stream_path, mode=mode)
            self.connect_stream_parameters()
        self.ui.select_dump_file.clicked.connect(select_and_update)
        self.ui.simulation_stream_checkbox.clicked.connect(self.connect_stream_parameters)
        self.ui.simulation_stream_mode.currentIndexChanged.connect(self.connect_stream_parameters)
        self.ui.simulation_stream_path.editingFinished.connect(self.connect_stream_parameters)
        
        #zmq stream paramters
        self.ui.simulation_enable_zmq_stream_checkbox.stateChanged.connect(self.connect_zmq_stream_parameters)
        self.ui.simulation_zmq_server_address.editingFinished.connect(self.connect_zmq_stream_parameters)
        self.ui.simulation_zmq_flush_btn.clicked.connect(self.sig_sim.zmq_streamer.flush)
        

        # nr_measurements_to_acquire
        self.ui.nr_meas_to_acquire.valueChanged.connect(self.connect_nr_meas_to_acquire)
        
        #add event handler for a change in dye concentration
        self.sig_sim.update_conc_from_stream+=self.conc_handler
    
    def conc_handler(self,_,conc):
        '''
        event handler for dye concentration. 
        This is needed to update concentration if data is loaded from rdp result stream
        signal_simulator.update_conc_from_stream is an obsub event.
        '''
        self.ui.dye_concentration.blockSignals(True)
        self.ui.dye_concentration.setValue(conc)
        self.ui.dye_concentration.blockSignals(False)

    @QtCore.pyqtSlot()
    @wait
    def connect_additional_parameters(self):
        update = self.sig_sim.set_additional_parameters(time_resolution = self.ui.time_resolution.value())
        self.init_controls(which='additional')
        if update:
            self.plot_bin_histogram()
    
    @QtCore.pyqtSlot()
    @wait
    def connect_optics_parameters(self):
        '''
        event handler for the optics parameters
        '''
        update = self.sig_sim.set_optics_parameters(f_objective=self.ui.f_objective.value(),
                                           na_objective=self.ui.na_objective.value(),
                                           f_laser_collimation=self.ui.f_laser_collimation.value(),
                                           f_laser_collimation_dist=self.ui.laser_collimation_dist.value(),
                                           laser_to_objective_dist = self.ui.laser_to_objective_dist.value(),
                                           f_relay=self.ui.f_relay.value(),
                                           r_pinhole=self.ui.r_pinhole.value(),
                                           detector_efficiency = self.ui.detector_efficiency.value(),
                                           optics_efficiency = self.ui.optics_efficiency.value(),
                                           detector_dead_time = self.ui.detector_dead_time.value(),
                                           eef_function_path = self.ui.select_eef_LineEdit.text())


        self.init_controls(which='optics')
        if update:
            self.plot_bin_histogram()
    
    @QtCore.pyqtSlot()
    @wait
    def connect_noise_parameters(self):
        '''
        self.ui.noise_method.currentIndexChanged.connect(self.connect_noise_parameters)
        self.ui.laser_noise_rsd.valueChanged.connect(self.connect_noise_parameters)
        self.ui.laser_noise_rsqrtcov.valueChanged.connect(self.connect_noise_parameters)
        self.ui.detector_noise_sd.valueChanged.connect(self.connect_noise_parameters)
        self.ui.detector_noiese_sqrtcov.valueChanged.connect(self.connect_noise_parameters)

        self.ui.select_noise_file_button.clicked.connect(lambda: self.select_file(self.ui.noise_file_path))
        self.ui.noise_file_path.textChanged.connect(self.connect_noise_parameters)
        event handler for the optics parameters
        ''' 
        update = self.sig_sim.set_noise_parameters(method=str(self.ui.noise_method.currentText()),
                                           std=self.ui.noise_std.value(),
                                           mean=self.ui.noise_mean.value(),
                                           detector_darknoise=self.ui.detector_darknoise.value(),
                                           scale_noise = self.ui.scale_noise.isChecked(),
                                           file_path=str(self.ui.noise_file_path.text()))


        self.init_controls(which='noise') #um objectivwe efficiency anzupassen
        
    @wait
    @QtCore.pyqtSlot()
    def connect_2d_pdf_plot(self):
        '''
        event handler for the 2D pdf plots
        '''
        if str(self.ui.laser_noise_pdf_axes.currentText()) == 'Ch1 vs Ch2':
            axes = ('1', '2')
        elif str(self.ui.laser_noise_pdf_axes.currentText()) == 'Ch1 vs Piezo':
            axes = ('p', '1')
        elif str(self.ui.laser_noise_pdf_axes.currentText()) == 'Ch2 vs Piezo':
            axes = ('p', '2')
        elif str(self.ui.laser_noise_pdf_axes.currentText()) == 'Ch1':
            axes = ('1')
        elif str(self.ui.laser_noise_pdf_axes.currentText()) == 'Ch2':
            axes = ('2')
        self.plot_noise_kernel(True, axes)

    @wait
    @QtCore.pyqtSlot()
    def connect_scan_parameters(self):
        '''
        event handler for the scan parameters
        '''
        update = self.sig_sim.set_scan_parameters(xy_speed=self.ui.xy_scan_speed.value(),
                                        xy_scan_dist=self.ui.xy_scan_distance.value(),
                                        objective_spin_radius= self.ui.objective_spin_radius.value(),
                                        objective_rps = self.ui.objective_spin_speed.value(), 
                                        scan_time=self.ui.scan_time.value(),
                                        bin_length = self.ui.bin_time.value())
        self.plot_path(init=True)
        if update:
            self.plot_bin_histogram()

    @wait
    @QtCore.pyqtSlot()
    def connect_cell_length(self):
        '''
        event handler for cell length
        '''
        self.sig_sim.set_cell_length(self.ui.absorption_cell_length.value())
        self.plot_absorbances()

    @wait
    @QtCore.pyqtSlot()
    def connect_dye_parameters(self):
        '''
        event handler for the tuning parameters
        '''
        update = self.sig_sim.set_dye_parameters(dye_name=str(self.ui.dye_selector.currentText()),
                                              conc=self.ui.dye_concentration.value(),
                                              epsilon=self.ui.dye_epsilon.value(),
                                              lifetime = self.ui.dye_lifetime.value(),
                                              lifetime_isc=self.ui.dye_lifetime_isc.value(),
                                              lifetime_triplet = self.ui.dye_lifetime_triplet.value(),
                                              quantum_efficiency=self.ui.dye_quantum_efficiency.value())

        if str(self.ui.dye_selector.currentText()) in ['manual']:
            self.ui.dye_concentration.setEnabled(True)
            self.ui.dye_quantum_efficiency.setEnabled(True)
            self.ui.dye_lifetime_isc.setEnabled(True)
            self.ui.dye_epsilon.setEnabled(True)
            self.ui.dye_lifetime_triplet.setEnabled(True)
            self.ui.dye_lifetime.setEnabled(True)
            
        else:
            self.ui.dye_concentration.setEnabled(True)
            self.ui.dye_quantum_efficiency.setDisabled(True)
            self.ui.dye_lifetime_isc.setDisabled(True)
            self.ui.dye_epsilon.setDisabled(True)
            self.ui.dye_lifetime_triplet.setDisabled(True)
            self.ui.dye_lifetime.setDisabled(True)
        
        self.init_controls(which='dye')
        if update:
            self.plot_bin_histogram()
        
        

        #self.plot_tuning_curve()
        #self.plot_absorbances()

    @wait
    @QtCore.pyqtSlot()
    def connect_laser_parameters(self):
        '''
        event handler for lineshape parameters
        '''
        update = self.sig_sim.set_laser_parameters(power = self.ui.laser_power.value(), 
                                                       wavelength = self.ui.laser_wavelength.value(),
                                                       waist=self.ui.laser_waist.value(),
                                                       m_squared = self.ui.laser_m_squared.value())
        self.init_controls(which='laser')
        if update:
            self.plot_bin_histogram()
        
    @wait
    @QtCore.pyqtSlot()
    def connect_calc_parameters(self):
        '''
        event handler for lineshape parameters
        '''
        update = self.sig_sim.set_calc_parameters(method = self.ui.calc_method.currentText(), 
                                           stats = self.ui.calc_stats.currentText(),
                                           threshold=self.ui.calc_threshold.value(),
                                           normalisation = self.ui.calc_normalisation.value(),
                                           single_count = self.ui.calc_single_count.isChecked())
        
        if str(self.ui.calc_method.currentText()) in ['smc']:
            self.ui.calc_stats.setDisabled(True)
            #self.ui.calc_threshold.setDisabled(True)
            self.ui.calc_single_count.setDisabled(True)
            
        else:
            self.ui.calc_stats.setEnabled(True)
            #self.ui.calc_threshold.setEnabled(True)
            self.ui.calc_normalisation.setEnabled(True)
            self.ui.calc_single_count.setEnabled(True)
                        
        self.init_controls(which='calc')
        if update:
            self.plot_bin_histogram()

    @QtCore.pyqtSlot()
    def connect_concentrations(self, component, value, channel):
        '''
        event handler for the concentrations
        :param component: name of component to set
        :type component:str
        :param value:value to set
        :type value: float
        :param channel: channel to set (1 or 2)
        :type channel: int
        '''
        self.sig_sim.set_concentrations(component, value, channel)
        self.plot_absorbances()

    @wait
    @QtCore.pyqtSlot()
    def connect_drift_parameters(self):
        '''
        event handler for the drift parameters
        '''
        # hf.pydevBrk()
        drift_array = np.zeros(6)
        drift_array[0] = self.ui.drift_static_offset.value()
        drift_array[1] = self.ui.drift_linear_rate.value()
        drift_array[2] = self.ui.drift_fast_decay_rate.value()
        drift_array[3] = self.ui.drift_fast_amplitude.value()
        drift_array[4] = self.ui.drift_slow_decay_rate.value()
        drift_array[5] = self.ui.drift_slow_amplitude.value()
        self.sig_sim.set_drift_parameters(drift_params=drift_array)
        self.plot_drift(recalc=True)
        self.plot_tuning_curve()
        self.plot_absorbances()

    @wait
    @QtCore.pyqtSlot()
    def connect_stream_parameters(self):
        '''
        event handler for stream parameters
        '''
        enable = self.ui.simulation_stream_checkbox.isChecked()
        mode = str(self.ui.simulation_stream_mode.currentText())
        file_path = str(self.ui.simulation_stream_path.text())

        self.sig_sim.set_stream_parameter(enable, mode, file_path)
        if enable and mode == 'load':
            # init tuning controls
            self.init_controls(('tuning', 'power'))
            #update pls2 measurement parameter
            if self.pls2.initialised:
                set_length = self.ui.dye_quantum_efficiency.value()
                tvss = abs((self.ui.dye_epsilon.value() -
                            self.ui.dye_lifetime.value()) / set_length)

                self.pls2.update_measurement_parameters(set_length, tvss)
            
    @wait
    @QtCore.pyqtSlot()
    def connect_zmq_stream_parameters(self):
        '''
        event handler for zmq stream parameter
        '''
        enable = self.ui.simulation_enable_zmq_stream_checkbox.isChecked()
        address = str(self.ui.simulation_zmq_server_address.text())
        self.sig_sim.set_zmq_parameter(enable, address)


    #===========================================================================
    # def drift_actual_time_handler(self, _, arg):
    #     '''
    #     event handler for the drift time (on spectrum_simulator side)
    #     Drift time is an obsub event.
    #     '''
    #     self.ui.drift_actual_time.blockSignals(True)
    #     self.ui.drift_actual_time.setValue(arg)
    #     self.ui.drift_actual_time.blockSignals(False)
    #     self.plot_drift()
    #===========================================================================
        
    def piezo_nr_steps_handler(self,_,**args):
        '''
        event handler for nr_steps (on spectrum_simulator side)
        Drift time is an obsub event.
        '''
        new_nr_steps = args["nr_steps"]
        self.ui.dye_quantum_efficiency.blockSignals(True)
        self.ui.dye_quantum_efficiency.setValue(new_nr_steps)
        if self.ui.PLS2_init_Library.isChecked():
            set_length = self.ui.dye_quantum_efficiency.value()
            tuning_step_size = abs(self.ui.dye_epsilon.value() - self.ui.dye_lifetime.value()) / set_length
            self.pls2.update_measurement_parameters(set_length, tuning_step_size)
        self.plot_tuning_curve()
        self.plot_absorbances()
        self.ui.dye_quantum_efficiency.blockSignals(False)
#===============================================================================
#     @QtCore.pyqtSlot()
#     def connect_drift_actual_time(self):
#         '''
#         event handler for the drift time
#         Connects an event handler for changes in the GUI and for changes in spectrum_simulator
#         '''
#         # actual drift time
# #         self.ui.drift_actual_time.valueChanged.disconnect()
# #             lambda: self.sig_sim.set_drift_parameters(drift_time=self.ui.drift_actual_time.value()))
#         self.ui.drift_actual_time.valueChanged.connect(
#             lambda: self.sig_sim.set_drift_time(self.ui.drift_actual_time.value()))
#         # self.sig_sim.set_drift_time.add_observer(self.drift_actual_time_handler)
#         # add self.drift_actual_time_handler as an event handler for the obsub
#         # event
#         self.sig_sim.set_drift_time += self.drift_actual_time_handler
#===============================================================================

    
    
    #===========================================================================
    # QtCore.pyqtSlot()
    # def connect_measurement_rate(self):
    #     '''
    #     event handler for the measurement rate
    #     Connects an event handler for changes in the GUI
    #     '''
    #     self.sig_sim.set_measurement_rate(self.ui.measurement_rate.value())
    #     if self.pls2.initialised:
    #         self.pls2.update_tuning_frequency(self.ui.measurement_rate.value())  #pylint: disable=no-member
    #===========================================================================

    QtCore.pyqtSlot()
    def connect_nr_meas_to_acquire(self):
        '''
        event handler for the number of measurements to acquire
        Connects an event handler for changes in the GUI
        '''
        self.nr_measurements_to_acquire = self.ui.nr_meas_to_acquire.value()

    def init_measurement_timer(self):
        '''
        inits the timer which controls the rate of measurements.
        The timer is everything else than accurate, because it takes not
        into account the amount of time the calculateions for one measurement take
        '''
        self.measurement_timer = QtCore.QTimer()
        # if self.ui.simulation_rate.value()==0, set to minimum timeout:
        # new: with QtWidgets.qApp.processEvents() in take_measurement(), timeout
        # can be set to zero
        min_timeout = 0
        self.measurement_timer.setInterval(min_timeout if self.ui.simulation_rate.value() == 0
                                           else int(1000.0 / self.ui.simulation_rate.value()))
        self.measurement_timer.timeout.connect(self.take_measurement)
        self.ui.simulation_rate.valueChanged.connect(
            lambda: self.measurement_timer.setInterval(min_timeout if self.ui.simulation_rate.value() == 0
                                                        else 1000.0 / self.ui.simulation_rate.value()))

    def init_controls(self, which=('all',)):
        '''
        Initialises all the controls for the signal_simulator class  with values from the signal_simulator
        instance.
        :param which: which parameters to initialise
        :type which: string or list of strings
        '''
        # make sure that which is a list
        if isinstance(which, str):
            which = (which,)
        self.block_signals(True, 'Simulation')
        if len(set(which) & set(['all'])):
            self.populate_nr_events_table_widget()
            self.ui.nr_events_statistics_size.setValue(self.sig_sim.nr_events_buffer.length)
        if len(set(which) & set(['all', 'additional'])):
            self.ui.time_resolution.setValue(self.sig_sim.additional_dict['time_resolution']*1E6)
            
            '''
            # fill comboboxes with available gases
            component_boxes = [self.ui.component_1, self.ui.component_2,
                               self.ui.component_3, self.ui.component_4, self.ui.component_5]
            for box in component_boxes:
                for component in self.sig_sim.dye_data.index:
                    box.addItem(component)

            # select the first 5 gases
            self.ui.component_1.setCurrentIndex(0)
            self.ui.component_2.setCurrentIndex(1)
            self.ui.component_3.setCurrentIndex(2)
            self.ui.component_4.setCurrentIndex(3)
            self.ui.component_5.setCurrentIndex(4)

            # set gas concentrations
            concentrations_1 = self.sig_sim.dye_data[
                'concentrations_1']
            self.ui.component_1_conc.setValue(concentrations_1[0])
            self.ui.component_2_conc.setValue(concentrations_1[1])
            self.ui.component_3_conc.setValue(concentrations_1[2])
            self.ui.component_4_conc.setValue(concentrations_1[3])
            self.ui.component_5_conc.setValue(concentrations_1[4])

            concentrations_2 = self.sig_sim.dye_data[
                'concentrations_2']
            self.ui.component_1_conc_2.setValue(concentrations_2[0])
            self.ui.component_2_conc_2.setValue(concentrations_2[1])
            self.ui.component_3_conc_2.setValue(concentrations_2[2])
            self.ui.component_4_conc_2.setValue(concentrations_2[3])
            self.ui.component_5_conc_2.setValue(concentrations_2[4])
            '''

        #scan parameters
        if len(set(which) & set(['all', 'scan'])):
            scan_dict = self.sig_sim.scan_dict
            self.ui.xy_scan_distance.setValue(scan_dict['xy_scan_dist']*1E3)
            self.ui.xy_scan_speed.setValue(scan_dict['xy_speed']*1E3)
            self.ui.objective_spin_radius.setValue(scan_dict['objective_spin_radius']*1E6)
            self.ui.objective_spin_speed.setValue(scan_dict['objective_rps'])
            self.ui.scan_time.setValue(scan_dict['scan_time'])
            self.ui.bin_time.setValue(scan_dict['bin_length']*1E6)
        # Noise parameters
        if len(set(which) & set(['all', 'noise'])):
            noise_dict = self.sig_sim.noise_dict
            self.ui.noise_file_path.setText('' if noise_dict['file'] is None else noise_dict['file'])
            if noise_dict['method'] == 'normal':
                current_index = 0
            else:
                current_index = 1
            self.ui.noise_method.setCurrentIndex(current_index)
            self.ui.noise_std.setValue(noise_dict['std'])
            self.ui.noise_mean.setValue(noise_dict['mean'])
            self.ui.detector_darknoise.setValue(noise_dict['detector_darknoise'])
            self.ui.scale_noise.setChecked(noise_dict['scale_noise'])

        #optics parameters
        if len(set(which) & set(['all', 'optics'])):
            optics_dict = self.sig_sim.optics_dict
            self.ui.f_objective.setValue(optics_dict['f_objective']*1E3)
            self.ui.na_objective.setValue(optics_dict['na_objective'])
            self.ui.f_laser_collimation.setValue(optics_dict['f_laser_collimation']*1E3)
            self.ui.laser_collimation_dist.setValue(optics_dict['f_laser_collimation_dist']*1E3)
            self.ui.laser_to_objective_dist.setValue(optics_dict['laser_to_objective_dist']*1E3)
            self.ui.f_relay.setValue(optics_dict['f_relay']*1E3)
            self.ui.r_pinhole.setValue(optics_dict['r_pinhole']*1E6)
            self.ui.detector_efficiency.setValue(optics_dict['detector_efficiency'])
            self.ui.optics_efficiency.setValue(optics_dict['optics_efficiency'])
            self.ui.detector_dead_time.setValue(optics_dict['detector_dead_time']*1E9)
            self.ui.select_eef_LineEdit.setText('' if optics_dict['encircled_energy_function_path'] is None else optics_dict['encircled_energy_function_path'])
            
            
        # dye parameters
        if len(set(which) & set(['all', 'dye'])):
            dye_dict = self.sig_sim.dye_dict
            
            items = ['manual']+list(dye_dict["dye_instance"].all_dye_data.index)
            self.ui.dye_selector.clear()
            for dye in items:
                    self.ui.dye_selector.addItem(dye)
                    
                    
            current_index = items.index(dye_dict["dye"])
            self.ui.dye_selector.setCurrentIndex(current_index)
            self.ui.dye_concentration.setValue(dye_dict['conc']*1E15)
            self.ui.dye_epsilon.setValue(dye_dict['epsilon'])
            self.ui.dye_lifetime.setValue(dye_dict['lifetime']*1E9)
            self.ui.dye_lifetime_isc.setValue(dye_dict['lifetime_isc']*1E6)
            self.ui.dye_lifetime_triplet.setValue(dye_dict['lifetime_triplet']*1E6)
            self.ui.dye_quantum_efficiency.setValue(dye_dict['quantum_efficiency'])
            
            if str(self.ui.dye_selector.currentText()) in ['manual']:
                self.ui.dye_concentration.setEnabled(True)
                self.ui.dye_quantum_efficiency.setEnabled(True)
                self.ui.dye_lifetime_isc.setEnabled(True)
                self.ui.dye_epsilon.setEnabled(True)
                self.ui.dye_lifetime_triplet.setEnabled(True)
                self.ui.dye_lifetime.setEnabled(True)
            
            else:
                self.ui.dye_concentration.setEnabled(True)
                self.ui.dye_quantum_efficiency.setDisabled(True)
                self.ui.dye_lifetime_isc.setDisabled(True)
                self.ui.dye_epsilon.setDisabled(True)
                self.ui.dye_lifetime_triplet.setDisabled(True)
                self.ui.dye_lifetime.setDisabled(True)

        # power spectrum parameters
        '''
        if len(set(which) & set(['all', 'power'])):
            power_dict = self.sig_sim.power_spectrum_dict
            self.ui.power_file_path.setText('' if power_dict['file'] is None else power_dict['file'])
            if power_dict['method'] == 'Constant':
                current_index = 0
            elif power_dict['method'] == 'Box':
                current_index = 1
            else:
                current_index = 2
            self.ui.xy_scan_distance.setCurrentIndex(current_index)
            self.ui.xy_scan_speed.setValue(power_dict['max_power'])
            self.ui.objective_spin_radius.setValue(power_dict['objective_spin_radius'])
            self.ui.objective_spin_speed.setValue(power_dict['wl_range'][0])
            self.ui.scan_time.setValue(power_dict['wl_range'][1])
        '''


        # Laser parameters
        if len(set(which) & set(['all', 'laser'])):
            laser_dict = self.sig_sim.laser_dict
            self.ui.laser_power.setValue(laser_dict['power']*1E3)
            self.ui.laser_wavelength.setValue(laser_dict['wavelength']*1E9)
            self.ui.laser_waist.setValue(laser_dict['laser_beam_waist']*1E6)
            self.ui.laser_m_squared.setValue(laser_dict['m_squared'])
            
        # calc parameters
        if len(set(which) & set(['all', 'calc'])):
            calc_dict = self.sig_sim.calc_dict
            method_list = ['local_baseline','smc']#,'0ppm_baseline']
            current_index = method_list.index(calc_dict["method"])
            self.ui.calc_method.setCurrentIndex(current_index)
            
            stats_list = ['poisson','poisson robust','normal','normal robust']
            current_index = stats_list.index(calc_dict["stats"])
            self.ui.calc_stats.setCurrentIndex(current_index)
            
            self.ui.calc_threshold.setValue(calc_dict['threshold'])
            self.ui.calc_normalisation.setValue(calc_dict['normalisation'])
            self.ui.calc_single_count.setChecked(calc_dict['single_count'])
        '''
        # stream_parameters
        if len(set(which) & set(['all', 'stream'])):
            stream_dict = self.sig_sim.stream_dict
            self.ui.simulation_stream_path.setText('' if stream_dict['file'] is None else stream_dict['file'])
            if stream_dict['mode'] == 'dump':
                current_index = 0
            elif stream_dict['mode'] == 'load':
                current_index = 1
            self.ui.simulation_stream_mode.setCurrentIndex(current_index)
            self.ui.simulation_stream_checkbox.setChecked(stream_dict['enable'])
        '''  
        # zmq stream paramters
        if len(set(which) & set(['all', 'zmq_stream'])):
            zmq_dict = self.sig_sim.zmq_dict
            self.ui.simulation_zmq_server_address.setText('' if zmq_dict['address'] is None else zmq_dict['address'])
            self.ui.simulation_enable_zmq_stream_checkbox.setChecked(zmq_dict["enable"])
        
        #update info text
        self.ui.info_browser.setText(self.sig_sim.get_info_string())
        self.block_signals(False, 'Simulation')

    def take_measurement(self, mode=None):
        '''
         Takes a measurement and updates all the plots
        :param mode: 'single' or None. If single number of acquired measurements is not checked
        :type mode: str od None
        '''
        #hf.pydevBrk()
        if ((self.nr_measurements_to_acquire > self.nr_measurements_acquired)
            or (self.nr_measurements_to_acquire == 0 or (mode == 'single'))):
            try:
                self.sig_sim.take_measurement()
                self.plot_nr_events()
                self.update_nr_events_table_widget()
                if self.ui.show_histogram_CheckBox.isChecked():
                    self.plot_histogram_signal()
            except (EOFError, StopIteration) as e:
                if not "zmq" in e.args:
                    #no further measurements available from stream
                    # stop simulation
                    if mode == None:
                        self.ui.start_simulation.setChecked(False)
                else:
                    pass #just wait for next message from zmq
                #wait a bit to not generate an empty loop
                time.sleep(0.01)
            else:
                self.plot_channels()
                if self.ui.PLS2_init_Library.isChecked():
                    x_axis, channel_1, channel_2, _ = self.sig_sim.return_spectrum()
                    environmental = np.zeros(6)
                    channel_2_type = self.ui.channel2_type.currentIndex()
                    self.pls2.add_new_data(channel_2, channel_1, x_axis, channel_2_type, environmental)
                    self.plot_pls_2_absorbance_fit()
                    self.pls2_update_shift_table_widget()
                    if self.pls2.new_wl_calibration_available:
                        self.plot_pls_2_wavelength_calibration()
                    if self.pls2.new_concentrations_available:
                        self.update_nr_events_table_widget()
                        self.plot_nr_events()
                    if self.ui.PLS2_save_absorbance_cont_CheckBox.isChecked():
                        self.save_absorbance()
                    if self.ui.PLS2_save_data_for_model_cont_CheckBox.isChecked():
                        self.write_Data_for_calibration()
    
                self.nr_measurements_acquired += 1
                self.ui.nr_meas_acquired.setValue(self.nr_measurements_acquired)
        else:
            # stop simulation 
            self.ui.start_simulation.setChecked(False)
        if mode == 'single':
            self.ui.single_step.setChecked(False)    
        QtWidgets.QApplication.processEvents()

    def connect_simulation_controls(self):
        '''
        connects the slots to the signals of the simulation controls
        '''
        self.ui.start_simulation.toggled.connect(self.start_simulation)
        self.ui.single_step.clicked.connect(lambda: self.take_measurement(mode='single'))
        #self.ui.measurement_rate.valueChanged.connect(self.connect_measurement_rate)
        self.ui.load_parameters_btn.clicked.connect(lambda: self.load_parameters(file_path=None))
        self.ui.save_parameters_btn.clicked.connect(lambda: self.save_parameters(path=None))

    @QtCore.pyqtSlot()
    def start_simulation(self):
        '''
        Event handler for the simulation controls
        '''
        if self.ui.start_simulation.isChecked():
            self.ui.start_simulation.setText(QtWidgets.QApplication.translate(
                "MainWindow", "stop simulation", None))
            self.nr_measurements_acquired = 0
            self.ui.nr_meas_acquired.setValue(0)
            self.measurement_timer.start()
        else:
            self.ui.start_simulation.setText(QtWidgets.QApplication.translate(
                "MainWindow", "start simulation", None))
            self.measurement_timer.stop()

    def select_file(self, qline=None, mode='open'):
        '''
        opens a window to select files
        The filename of the selected file is saved in the given qline widget
        :param qline: qline widget where the file name is saved
        :type qline: qline widget
        :param mode: mode of the QFileDialog
        :type mode: str:
                    'open' opens QFileDialog.getOpenFileName
                    'save' opens QFileDialog.getSaveFileName
        '''
        path_to_open = ""
        if qline is not None:
            path_to_open = os.path.dirname(str(qline.text()))
        
        if not os.path.exists(path_to_open):
            path_to_open = os.path.dirname(os.path.abspath(sys.argv[0]))
            
        if mode == 'open':
            fname, _  = QtWidgets.QFileDialog.getOpenFileName(
                self, 'Select file', path_to_open)
        elif mode == 'save':
            fname, _ = QtWidgets.QFileDialog.getSaveFileName(self, 'Select file', path_to_open, filter='*.pck')
        if qline is not None:
            qline.setText(fname)
        return fname

    def select_path(self, qline=None):
        '''
        opens a window to select path
        The selected path is saved in the given Qline widget
        :param qline: Qline widget where the file name is saved
        :type qline: Qline widget
        '''
        path_to_open = ""
        if qline is not None:
            path_to_open = os.path.dirname(str(qline.text()))
            
        if not os.path.exists(path_to_open):
            path_to_open = os.path.dirname(os.path.abspath(sys.argv[0]))
            
        fname = QtWidgets.QFileDialog.getExistingDirectory(self, 'Select path',path_to_open)
        if qline is not None:
            qline.setText(fname)
        return fname

    def connect_pls2(self,connect=True):
        '''
        connects the slots to the signals of the PLS2 controls. Before connecting it disconnects all slots. 
        
        :param connect: If True the slots will be connected, otherwise disconnected
        :type connect:
        '''
        if connect == False:
            #This is needed in the case that the pls2 library was reinitialised.
            #we have to disconnect all signals. Otherwise they end up do be connected multiple times. 
            list_of_disconnects = [
            self.ui.PLS2_nr_meas_to_int.valueChanged.disconnect,
            self.ui.PLS2_alignMethod.currentIndexChanged.disconnect,
            self.ui.PLS2_alignAlgorithm.currentIndexChanged.disconnect,
            self.ui.PLS2_aligner_num_sets.valueChanged.disconnect,
            self.ui.select_pls2_model_path_write_button.clicked.disconnect,
            self.ui.select_pls2_model_path_read_button.clicked.disconnect,
            self.ui.PLS2_model_path_read.textChanged.disconnect,
            self.ui.PLS2_model_number.currentItemChanged.disconnect,
            self.ui.PLS2_calculate_concentrations.stateChanged.disconnect,
            self.ui.PLS2_process_batchwise.stateChanged.disconnect,
            self.ui.PLS2_wl_calibration_order.valueChanged.disconnect,
            self.ui.PLS2_wl_peak_fitting_kernel.valueChanged.disconnect,
            self.ui.PLS2_wl_peak_search_minimum.valueChanged.disconnect,
            self.ui.PLS2_wl_peak_search_delta.valueChanged.disconnect,
            self.ui.PLS2_wl_peak_search_window.valueChanged.disconnect,
            self.ui.PLS2_wl_smoothing_kernel.valueChanged.disconnect,
            self.ui.PLS2_wl_peak_positions.cellChanged.disconnect,
            self.ui.PLS2_wl_calibrate_button.clicked.disconnect,
            self.ui.PLS2_wl_peak_search_start_index.valueChanged.disconnect,
            self.ui.PLS2_wavelength_calibraton_mode_comboBox.currentIndexChanged.disconnect,
            self.ui.nr_events_reset_statistics_button.clicked.disconnect,
            self.ui.PLS2_statistics_size.valueChanged.disconnect,
            self.ui.PLS2_save_data_for_model_Button.clicked.disconnect,
            self.ui.PLS2_build_model_Button.clicked.disconnect,
            self.ui.PLS2_calibration_wavelength_range_start.valueChanged.disconnect,
            self.ui.PLS2_calibration_wavelength_range_stop.valueChanged.disconnect,
            self.ui.PLS2_number_of_calibration_points.valueChanged.disconnect,
            self.ui.PLS2_show_allan.stateChanged.disconnect,
            self.ui.PLS2_aligner_threshold.valueChanged.disconnect,
            self.ui.PLS2_aligner_interp_factor.valueChanged.disconnect,
            self.ui.PLS2_shift_table.cellChanged.disconnect,
            self.ui.PLS2_update_absorbance_offset_button.clicked.disconnect,
            self.ui.PLS2_enable_absorbance_offset_button.clicked.disconnect,
            self.ui.save_concentrations_button.clicked.disconnect,
            self.ui.select_concentrations_data_path_button.clicked.disconnect,
            self.ui.PLS2_save_absorbance_button.clicked.disconnect,
            self.ui.PLS2_select_absorbance_data_path_button.clicked.disconnect,
            self.ui.PLS2_update_raw_offset_button.clicked.disconnect,
            self.ui.PLS2_raw_offset_end_index.valueChanged.disconnect,
            self.ui.PLS2_concentrations_plot_listWidget.itemSelectionChanged.disconnect]
            
            for disconnect in list_of_disconnects:
                try:
                    disconnect()
                except TypeError:
                    #no slot was connected
                    pass
        else:
            
            self.ui.PLS2_nr_meas_to_int.valueChanged.connect((lambda: self.pls2.update_num_sets(self.ui.PLS2_nr_meas_to_int.value())))
            self.ui.PLS2_alignMethod.currentIndexChanged.connect(
                lambda: self.pls2.update_enable_alignment(self.ui.PLS2_alignMethod.currentIndex()))
            self.ui.PLS2_alignAlgorithm.currentIndexChanged.connect(
                lambda: self.pls2.update_aligner_algorithm(self.ui.PLS2_alignAlgorithm.currentIndex()))
            self.ui.PLS2_aligner_num_sets.valueChanged.connect((lambda: self.pls2.update_aligner_num_sets(self.ui.PLS2_aligner_num_sets.value())))
            self.ui.select_pls2_model_path_write_button.clicked.connect((lambda: self.select_path(self.ui.PLS2_model_path_write)))
            self.ui.select_pls2_model_path_read_button.clicked.connect((lambda: self.select_path(self.ui.PLS2_model_path_read)))
            self.ui.PLS2_model_path_read.textChanged.connect(lambda: self.pls2_update_process_setting(True))
            self.ui.PLS2_model_number.currentItemChanged.connect(lambda: self.pls2_update_process_setting(False))
            self.ui.PLS2_calculate_concentrations.stateChanged.connect(lambda: self.pls2_update_process_setting(False))
            self.ui.PLS2_process_batchwise.stateChanged.connect(lambda: self.pls2_update_process_setting(False))
            self.ui.PLS2_wl_calibration_order.valueChanged.connect(self.pls2_update_wavelength_calibration_parameters)
            self.ui.PLS2_wl_peak_fitting_kernel.valueChanged.connect(self.pls2_update_wavelength_calibration_parameters)
            self.ui.PLS2_wl_peak_search_minimum.valueChanged.connect(self.pls2_update_wavelength_calibration_parameters)
            self.ui.PLS2_wl_peak_search_delta.valueChanged.connect(self.pls2_update_wavelength_calibration_parameters)
            self.ui.PLS2_wl_peak_search_window.valueChanged.connect(self.pls2_update_wavelength_calibration_parameters)
            self.ui.PLS2_wl_smoothing_kernel.valueChanged.connect(self.pls2_update_wavelength_calibration_parameters)
            self.ui.PLS2_wl_peak_positions.cellChanged.connect(self.pls2_update_wavelength_calibration_parameters)
            self.ui.PLS2_wl_calibrate_button.clicked.connect(self.pls2.calibrate_wavelength)
            self.ui.PLS2_wl_peak_search_start_index.valueChanged.connect(self.pls2_update_wavelength_calibration_parameters)
            self.ui.PLS2_wavelength_calibraton_mode_comboBox.currentIndexChanged.connect(self.pls2_update_wavelength_calibration_parameters)
            self.ui.nr_events_reset_statistics_button.clicked.connect(self.pls2_reset_statistics)
            self.ui.PLS2_statistics_size.valueChanged.connect(lambda: self.pls2_update_statistics_size(self.ui.PLS2_statistics_size.value()))
            self.ui.PLS2_save_data_for_model_Button.clicked.connect(self.write_Data_for_calibration)
            self.ui.PLS2_build_model_Button.clicked.connect(self.build_calibration_model)
            self.ui.PLS2_calibration_wavelength_range_start.valueChanged.connect(self.pls2_update_model_save_parameters)
            self.ui.PLS2_calibration_wavelength_range_stop.valueChanged.connect(self.pls2_update_model_save_parameters)
            self.ui.PLS2_number_of_calibration_points.valueChanged.connect(self.pls2_update_model_save_parameters)
            self.ui.PLS2_show_allan.stateChanged.connect(self.pls2_init_allan_plot)
            self.ui.PLS2_aligner_threshold.valueChanged.connect(
                lambda: self.pls2.update_aligner_threshold(self.ui.PLS2_aligner_threshold.value()))
            self.ui.PLS2_aligner_interp_factor.valueChanged.connect(
                lambda: self.pls2.update_aligner_interpolation_factor(self.ui.PLS2_aligner_interp_factor.value()))
            self.ui.PLS2_shift_table.cellChanged.connect(self.pls2_update_manual_shift_factors)
            self.ui.PLS2_update_absorbance_offset_button.clicked.connect(self.pls2.update_absorbance_offsets) #pylint: disable=no-member
            self.ui.PLS2_enable_absorbance_offset_button.clicked.connect(lambda :self.pls2.update_enable_absorbance_offsets(self.ui.PLS2_enable_absorbance_offset_button.isChecked())) #pylint: disable=no-member
            # concentrations_buttons
            self.ui.save_concentrations_button.clicked.connect(self.save_concentrations)
            self.ui.select_concentrations_data_path_button.clicked.connect(
                lambda: self.select_file(self.ui.save_concentrations_path, mode='save'))
            # save absorbance
            self.ui.PLS2_save_absorbance_button.clicked.connect(self.save_absorbance)
            self.ui.PLS2_select_absorbance_data_path_button.clicked.connect(
                lambda: self.select_file(self.ui.PLS2_save_absorbance_path, mode='save'))
            #raw offsets
            self.ui.PLS2_update_raw_offset_button.clicked.connect(self.pls2.update_zero_offset_values)
            self.ui.PLS2_raw_offset_end_index.valueChanged.connect(lambda: self.pls2.update_zero_offset_range(
                self.ui.PLS2_raw_offset_start_index.value(),self.ui.PLS2_raw_offset_end_index.value()))
            #concetration_to plot
            self.ui.PLS2_concentrations_plot_listWidget.itemSelectionChanged.connect(self.init_nr_events_plot)

    @QtCore.pyqtSlot()
    def build_calibration_model(self):
        '''
        Builds the PLS2 calibration model
        '''
        path = str(self.ui.PLS2_model_path_write.text())
        nr_components = self.ui.PLS2_number_of_components.value()
        self.pls2.build_calibration_model(path,nr_components)

    @QtCore.pyqtSlot()
    def write_Data_for_calibration(self):
        '''
        writes absorption data for the generation of a PLS2 model
        It checks wether a component should be included in the model based on the status of
        the LS2_use_component checkboxes
        If the checkbox is not ckecked, the corresponding concentration is set to zero.
        This allows for adding some gases to the sample without actually taking it into account in the PLS2 model.
        '''
        path = str(self.ui.PLS2_model_path_write.text())
        sim_data = self.sig_sim.dye_data.copy()
        button_list = [self.ui.PLS2_use_component_1_checkBox, self.ui.PLS2_use_component_2_checkBox,
                       self.ui.PLS2_use_component_3_checkBox, self.ui.PLS2_use_component_4_checkBox,
                       self.ui.PLS2_use_component_5_checkBox]
        
        name_field_list = [self.ui.component_1, self.ui.component_2, self.ui.component_3,
                           self.ui.component_4,self.ui.component_5]
        
        name_list = [str(field.currentText()) for field in name_field_list]
        
        delete_data_array =np.ones(len(button_list), dtype=np.bool)
        for idx, button in enumerate(button_list):
            delete_data_array[idx] = button.isChecked()

        for delete_data, name in zip(delete_data_array, name_list):
            if delete_data:
                sim_data.loc[name, 'concentrations_1'] = 0
        
        self.pls2.write_data_for_calibration(path, sim_data)

    @QtCore.pyqtSlot()
    def save_absorbance(self):
        '''
        Saves the absorbance in the PLS2 class to a binary file
        '''
        note = str(self.ui.PLS2_save_absorbance_note.toPlainText())
        file_path = str(self.ui.PLS2_save_absorbance_path.text())
        
        dirname = os.path.dirname(file_path)
        #creat directory
        if not os.path.isdir(dirname):
            try:
                os.makedirs(dirname)
            except (OSError, WindowsError):
                self.error_handler.logger.warning('wrong path:'+dirname)
                return
        
        self.pls2.save_absorbance(file_path, note)

    @QtCore.pyqtSlot()
    def save_concentrations(self):
        '''
        Saves all the conentrations to a file
        it also saves a csv parameter file with the same name and a loadable pcikle parameter file.
        '''
        if str(self.ui.save_concentrations_path.text()) == '':
            self.select_file(self.ui.save_concentrations_path)

        path = os.path.normpath(str(self.ui.save_concentrations_path.text()))
        
        basename = os.path.basename(path)
        dirname = os.path.dirname(path)
        #creat directory
        if not os.path.isdir(dirname):
            try:
                os.makedirs(dirname)
            except (OSError, WindowsError):
                self.error_handler.logger.warning('wrong path:'+dirname)
                return
        
        # remove extension
        basename = os.path.splitext(basename)[0]
        # add '.p' for pickle
        picklename = basename + '.pck'
        picklepath = os.path.join(dirname, picklename)
        self.pls2.calculator.save_data(picklepath)
        # write note
        notename = basename + '.txt'
        notepath = os.path.join(dirname, notename)
        with open(notepath, "w") as text_file:
            text_file.write(str(self.ui.save_concentrations_note.toPlainText()))
        simulator_csv_name = basename + '_simulator_parameters.csv'
        simulator_csv_path = os.path.join(dirname, simulator_csv_name)
        self.sig_sim.save_parameters(simulator_csv_path)
        # save pickle file of all parameters
        parameter_filename = basename + '_parameters.pck'
        parameter_path = os.path.join(dirname, parameter_filename)
        self.save_parameters(parameter_path)

    @QtCore.pyqtSlot()
    def pls2_init_allan_plot(self):
        '''
        Starts the statistics script in a new process.
        This script shows an allan plot of the concentrations
        '''
        os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
        if self.ui.PLS2_show_allan.isChecked() and (self.allan_proc is None):
            mmap_path = os.path.join(
                os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'result_map.dat')
            script_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'pls2', 'pls2_statistics.py')
            self.allan_proc = subprocess.Popen(
                ['python', script_path, mmap_path, 'Allan'])
        else:
            if self.allan_proc is not None:
                self.allan_proc.terminate()
                self.allan_proc = None

    @QtCore.pyqtSlot()
    def update_statistics_size(self):
        '''
        Event handler for the events_statistics_size control
        :param size: number of measurements in the statistics
        '''
        size = self.ui.nr_events_statistics_size.value()
        if size == -1:
            self.sig_sim.nr_events_buffer.set_inflatable(True)
        else:
            self.sig_sim.nr_events_buffer.set_inflatable(False)
            self.sig_sim.nr_events_buffer.change_length(size)
            self.ui.nr_events_plot.enableAutoRange('x', False)
            self.ui.nr_events_plot.setXRange(0, size)

            

    @QtCore.pyqtSlot()
    def reset_statistics(self):
        '''
        event handler for the reset statistics control.
        resetes the statistics
        '''
        self.sig_sim.nr_events_buffer.reset()  #pylint: disable=no-member
        if self.ui.nr_events_statistics_size.value() > 0:
            self.ui.nr_events_plot.setXRange(
                0, self.ui.nr_events_statistics_size.value())
        else:
            self.ui.nr_events_plot.setXRange(0, 10)
            self.ui.nr_events_plot.enableAutoRange('x', True)

    @QtCore.pyqtSlot()
    def init_pls2(self, connect):
        '''
        Event handler for the init_pls2 controll.
        It initialises and deinitialises the PLS2 library
        :param connect: If true, the librabry gets initialised, of False, the library get deinitialised
        :type connect:bool
        '''
        if connect:
            # connect PLS2 library
            num_sets = self.ui.PLS2_nr_meas_to_int.value()
            set_length = self.ui.dye_quantum_efficiency.value()
            # print set_length
            zero_offset_start = 0
            zero_offset_end = 10
            zero_offset_values = np.array([0, 0, 0], dtype=np.float64)
            tvss = abs((self.ui.dye_epsilon.value() -
                        self.ui.dye_lifetime.value()) / set_length)
            tuning_frequency = self.ui.measurement_rate.value()
            # print tvss
            path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
            # print path
            modelpath = os.path.join(
                path, 'calibration_models', 'default_CH4')
            # print modelpath
            orig_stdout = sys.stdout
            # set_length*2 because PLS2 library expects 2-sided triangle
            self.pls2.initialise(num_sets, set_length, zero_offset_start, zero_offset_end,
                                 tvss, path, zero_offset_values, modelpath, tuning_frequency)
            self.pls2_update_wavelength_calibration_parameters()
            self.pls2.update_zero_offset_range(
                self.ui.PLS2_raw_offset_start_index.value(),self.ui.PLS2_raw_offset_end_index.value())
            self.pls2.update_aligner_algorithm(self.ui.PLS2_alignAlgorithm.currentIndex())
            self.pls2.update_aligner_threshold(self.ui.PLS2_aligner_threshold.value())
            self.pls2.update_aligner_interpolation_factor(self.ui.PLS2_aligner_interp_factor.value())
            self.pls2.update_aligner_num_sets(self.ui.PLS2_aligner_num_sets.value())
            sys.stdout = orig_stdout
            self.pls2_populate_model_list_widget()
            self.populate_nr_events_table_widget()
            self.pls2_populate_concentrations_plot_listWidget()
            self.init_pls_2_wavelength_calibration_plot()
            self.init_nr_events_plot()
            self.connect_pls2()
            self.block_signals(False, 'PLS2')
        else:
            self.connect_pls2(False)
            #self.block_signals(True, 'PLS2')
            del self.pls2
            self.pls2 = pls2.PLS2(self.error_handler.logger)
            # self.pls2.deinit()

    def block_signals(self, block=True, controls='PLS2'):
        '''
        Blocks all signals of PLS2 controls
        Caveat!!! This does not really work. Because QObject.children() does not search iteratively!!!!!!
        :param block: Block signals if True
        :type block: bool
        :param controls: controls to block
        :type str:   'PLS2' for PLS2 conrols
                     'Simulation' for simulation controls
                     'ui' all controls
        '''
        if controls == 'PLS2':
            main_widget = self.ui.PLS2_Fit
        elif controls == 'Simulation':
            main_widget = self.ui.Simulation
        elif controls == 'ui':
            main_widget = self.ui
        else:
            return
        # tries to find all the input widgets
        for widget in main_widget.findChildren((QtWidgets.QDoubleSpinBox, QtWidgets.QListWidget, QtWidgets.QListWidgetItem, QtWidgets.QPushButton,
                                                QtWidgets.QRadioButton, QtWidgets.QSpinBox, QtWidgets.QTableWidget, QtWidgets.QTableWidgetItem,
                                                QtWidgets.QTextEdit, QtWidgets.QComboBox, QtWidgets.QLineEdit, QtWidgets.QCheckBox, QtWidgets.QPlainTextEdit)):
            widget.blockSignals(block)
        # unblock init widget
        if controls == 'PLS2':
            self.ui.PLS2_init_Library.blockSignals(False)

    
    @QtCore.pyqtSlot()
    def pls2_update_model_save_parameters(self):
        '''
        Event handler of the PLS2 model save parameters
        '''
        nr_steps = int(self.ui.PLS2_number_of_calibration_points.value())
        start_wl = self.ui.PLS2_calibration_wavelength_range_start.value()
        stop_wl = self.ui.PLS2_calibration_wavelength_range_stop.value()
        wl_range = np.array((start_wl, stop_wl))
        self.pls2.update_PLS2_model_save_interpolator_parameters(  #pylint: disable=no-member
            nr_steps, wl_range)

    @QtCore.pyqtSlot()
    def pls2_update_process_setting(self, update_list=False):
        '''
        Event handler fo the pls2 processing parameters
        :param update_list: If True, the list of the available models gets updated
        :type update_list:Bool
        '''
        calc_concentrations = int(self.ui.PLS2_calculate_concentrations.isChecked())
        calibration_model_path = os.path.normpath(str(self.ui.PLS2_model_path_read.text()))
        new_used_absorbance = self.ui.PLS2_used_triangle_side.currentIndex()
        new_calibration_model_nr = self.ui.PLS2_model_number.currentRow()
        # self.ui.PLS2_model_number was not yet initialised
        if new_calibration_model_nr < 0:
            new_calibration_model_nr = 0
        process_batchwise = int(self.ui.PLS2_process_batchwise.isChecked())
        self.pls2.update_parameters(calc_concentrations, calibration_model_path,
                                    new_used_absorbance, new_calibration_model_nr, process_batchwise)
        if update_list:
            # calibration model has changed
            self.pls2_populate_model_list_widget()
        self.populate_nr_events_table_widget()
        self.pls2_populate_concentrations_plot_listWidget()
        self.init_nr_events_plot()

    @QtCore.pyqtSlot()
    def pls2_update_wavelength_calibration_parameters(self):
        '''
        Event handler for the wavelength calibration parameters
        '''
        self.ui.PLS2_wl_peak_positions.resizeColumnsToContents()
        self.ui.PLS2_wl_peak_positions.resizeRowsToContents()

        smoothing_kernel_half_size = self.ui.PLS2_wl_smoothing_kernel.value()
        peak_search_minimum = self.ui.PLS2_wl_peak_search_minimum.value()
        peak_search_delta = self.ui.PLS2_wl_peak_search_delta.value()
        peak_fitting_kernel_half_size = self.ui.PLS2_wl_peak_fitting_kernel.value()
        peak_search_start_index = self.ui.PLS2_wl_peak_search_start_index.value()
        calibration_order = self.ui.PLS2_wl_calibration_order.value()
        peak_wavenumbers = np.empty(
            self.ui.PLS2_wl_peak_positions.columnCount())
        for idx in range(self.ui.PLS2_wl_peak_positions.columnCount()):
            peak_wavenumbers[idx] = float(
                str(self.ui.PLS2_wl_peak_positions.item(0, idx).text()))
        # delete zeros convert to wavenumbers
        peak_wavenumbers = 10000 * 1000.0 / peak_wavenumbers[peak_wavenumbers > 0]
        num_peaks_to_fit = peak_wavenumbers.shape[0]
        xaxis = 1  # 0 means wavenumber, 1 means wavelength
        calcoef = self.pls2.wl_calibration_coefficients
        peak_search_window = self.ui.PLS2_wl_peak_search_window.value()
        PLS2_wavelength_calibration_mode = self.ui.PLS2_wavelength_calibraton_mode_comboBox.currentIndex()
        self.pls2.update_wavelength_calibration_parameters(num_peaks_to_fit, smoothing_kernel_half_size, peak_search_minimum,
                                                           peak_search_delta,peak_fitting_kernel_half_size, calibration_order,
                                                           peak_wavenumbers,xaxis, calcoef, peak_search_window,
                                                           PLS2_wavelength_calibration_mode, peak_search_start_index)

    def pls2_populate_model_list_widget(self):
        '''
        Populates the list of the available PLS2 models
        '''
        self.ui.PLS2_model_number.clear()
        for model in range(self.pls2.num_models):
            model_str = ''
            for comp in range(self.pls2.num_calibration_components):
                comp_name = self.pls2.return_all_component_names(comp, model) #pylint: disable=no-member
                if comp_name != b'':
                    if comp == 0:
                        model_str = comp_name
                    else:
                        model_str += b'-' + comp_name
            self.ui.PLS2_model_number.addItem(model_str.decode('utf-8'))

    def populate_nr_events_table_widget(self):
        '''
        Populates the table with the concentrations
        '''
        table = self.ui.nr_events_table
        table.setColumnCount(5)
        nr_of_components = 1
        # one row for number of results
        table.setRowCount(nr_of_components + 1)
        headers = ['# events', 'mean', 'std. dev.','CV / %','threshold']
        for comp in range(nr_of_components):
            comp_name = self.sig_sim.noise_dict['method']
            table.setVerticalHeaderItem(
                comp, QtWidgets.QTableWidgetItem(comp_name))
            for dim in range(5):
                table.setItem(comp, dim, QtWidgets.QTableWidgetItem('0'))
        table.setVerticalHeaderItem(nr_of_components, QtWidgets.QTableWidgetItem('n'))
        table.setItem(nr_of_components, 0, QtWidgets.QTableWidgetItem('0'))
        table.setHorizontalHeaderLabels(headers)
        table.resizeColumnsToContents()
    
    def pls2_populate_concentrations_plot_listWidget(self):
        '''
        populates the list widget with the available concetrations
        '''
        widget = self.ui.PLS2_concentrations_plot_listWidget
        widget.clear()
        nr_of_components = 1#self.pls2.num_calibration_components
        for comp in range(nr_of_components):
            comp_name = self.pls2.return_all_component_names( #pylint: disable=no-member
                comp, self.pls2.current_model_used)
            widget.addItem(comp_name.decode('utf-8'))
        widget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        #select all items
        for i in range(widget.count()):
            widget.item(i).setSelected(True)

    def update_nr_events_table_widget(self):
        '''
        Updates the table with to concentrations with new data
        '''
        buffer = self.sig_sim.nr_events_buffer
        threshold_counts = self.sig_sim.result_dict["threshold_counts"]
        results = np.array([buffer.get_last_value(), buffer.mean,buffer.std,buffer.cv*100,threshold_counts], dtype = np.float)
        table = self.ui.nr_events_table
        if results.ndim == 1:
            #make sure to have a 2D array even if only 1 component is available
            results = results[:,np.newaxis]
        format_strings = ['{:01.0f}']+3*['{:01.2f}']+['{:01.0f}']
        for (dim, comp), value in np.ndenumerate(results):
            # table.setItem(comp,dim,QtWidgets.QTableWidgetItem('{:01.2f}'.format(value)))
            
            table.item(comp, dim).setData(0, format_strings[dim].format(value))
        table.item(table.rowCount() - 1, 0).setData(0, '{:d}'.format(buffer.nr_items))
        table.resizeColumnsToContents()
    
    def pls2_update_shift_table_widget(self):
        '''
        Updates the table with the shifts used by the aligner to align the spectra
        Itz sets the Background color of the items corresponding to thequality of the alignment
        red means quality bad, alignment not used
        green means quality ok, alignment used
        '''
        if (self.ui.PLS2_alignMethod.currentIndex()>0) and (self.ui.PLS2_alignMethod.currentIndex()!=5):
            shifts, alignments = self.pls2.get_shift_factors()
        else:
            #set shifts to zero if alignment is disabled
            shifts, alignments = [0,0,0,0,0,0,0,0], [2,2,2,2,2,2,2,2]
            
        table = self.ui.PLS2_shift_table
        for  idx, value in enumerate(shifts[::2]):
            table.item(0, idx).setData(0, '{:02.2f}'.format(value))
        for  idx, value in enumerate(alignments[::2]):
            if value==1:
                table.item(0, idx).setBackground(QtGui.QColor(204, 255, 204))
            elif value==0:
                table.item(0, idx).setBackground(QtGui.QColor(255, 204, 204))
            else:
                #set background color to white if alignment is disabled
                table.item(0, idx).setBackground(QtGui.QColor(255, 255, 255))

    @QtCore.pyqtSlot()
    def pls2_update_manual_shift_factors(self):
        manual_shift_factors = np.zeros(4,dtype=np.float)
        table = self.ui.PLS2_shift_table
        for  idx in range(4):
            manual_shift_factors[idx] = float(table.item(0, idx).text())
        self.pls2.update_aligner_manual_shift_factor(manual_shift_factors)
    
    @QtCore.pyqtSlot()
    def save_parameters(self, path=None):
        '''
        Saves all paramters of the spectrum_simulator and PLS2 instances
        :param path: Path to the save file. If None, a window is opened for selecting the file
        :type path: string
        '''
        if path is None:
            path = str(self.select_file(mode='save'))
        if path != '':  # window closed without selecting a path
            if os.path.splitext(path)[1]=='':
                #add pickle extension
                path = os.path.splitext(path)[0]+'.pck'
        dirname = os.path.dirname(path)
        if not os.path.isdir(dirname):
            try:
                os.makedirs(dirname)
            except (OSError, WindowsError):
                self.error_handler.logger.warning('wrong path:'+dirname)
                return
        # configuration of spectrum_simulator and pls2 are saved in the
        # same file
        with open(path, 'wb') as handle:
            self.sig_sim.save_parameters_pickle(handle)
            self.pls2.save_parameters_pickle(handle)

    @QtCore.pyqtSlot()
    @wait
    def load_parameters(self,file_path=None):
        '''
        Loads all the parameters of the spectrum_simulator and PLS2 instances.

        If path is not given, a dialog will be opened.
        This is not perfect because the setEnable attribute of the controls is not set according to the loaded parameters.
        
        :param file_path: Path to config file
        :type file_path: str
        '''
        if file_path == None:
            file_path = str(self.select_file())
        if file_path != '':  # window closed without selecting a path
            if os.path.isfile(file_path):
                with open(file_path, 'rb') as handle:
                    self.sig_sim.load_parameters(handle)
                    self.init_controls()
                    #hf.pydevBrk()
                    if self.pls2.initialised:
                        self.pls2.load_parameters(handle)
                        self.reinit_pls2_controls_from_instance()
                    else:
                        self.load_pls2_parameter_to_gui(handle)
    
                self.plot_lineshapes()
                self.plot_noise_kernel()
                self.plot_power_spectrum()
                self.plot_tuning_curve()
                self.plot_drift(False)
                self.plot_absorbances()
            else:
                self.out_handler.logger.warning("Parameter file not found: " + file_path)

    def load_pls2_parameter_to_gui(self,path_or_handler):
        '''
        This function loads all PLS2 parameters into the gui for the case that the pls2 instance is not initialised.
        :param path_or_handler: path or handler to a pickle file
        :type path_or_handler: str or handler
        '''
        if not isinstance(path_or_handler,IOBase):
            try:
                with open(path_or_handler, 'rb') as handle:
                    all_parameters=pickle.load(handle)
            except IOError:
                self.error_handler.logger.warning('not able to load save file')
                return
            except UnicodeDecodeError:
                #pickle file from python 2.7
                with io.open(path_or_handler, "rb") as handle:
                    u = pickle._Unpickler(handle)
                    u.encoding = 'latin-1'
                    all_parameters = u.load()
                    self.logger.warning('python 2.7 pickle file loaded')
                
        else:
            try:
                position = path_or_handler.tell()
                all_parameters=pickle.load(path_or_handler)
            except UnicodeDecodeError:
                #pickle file from python 2.7
                path_or_handler.seek(position)
                u = pickle._Unpickler(path_or_handler)
                u.encoding = 'latin-1'
                all_parameters = u.load()
                #self.logger.warning('python 2.7 pickle file loaded')

        #replace relative paths with absolute paths
        if ((not all_parameters['calibration_model_path'].startswith("C:"))
            and (not all_parameters['calibration_model_path'] == '')):
            main_folder = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
            abspath = os.path.join(main_folder,all_parameters['calibration_model_path']) 
            abspath = abspath.replace('/',os.sep)
            all_parameters['calibration_model_path'] = abspath
    
                
        self.ui.PLS2_statistics_size.setValue(all_parameters['statistics_num_sets'])
        self.ui.PLS2_model_path_read.setText(all_parameters['calibration_model_path'])
        self.ui.PLS2_used_triangle_side.setCurrentIndex(all_parameters['used_absorbance'])
        self.ui.PLS2_process_batchwise.setChecked(bool(all_parameters['process_batchwise']))

        self.ui.PLS2_wl_peak_positions.setColumnCount(40)
        self.ui.PLS2_wl_peak_positions.setRowCount(1)
        self.ui.PLS2_wl_peak_positions.setSortingEnabled(False)

        cal_wavelengths = 10**7 / all_parameters['wl_calibration_peak_wavenumbers']
        cal_wavelengths = np.append(cal_wavelengths, np.zeros(40 - len(all_parameters['wl_calibration_peak_wavenumbers'])))

        for idx, peak in enumerate(cal_wavelengths):
            # print idx, peak
            newitem = QtWidgets.QTableWidgetItem(str(peak))
            self.ui.PLS2_wl_peak_positions.setItem(0, idx, newitem)

        #for old files
        if 'wl_absorbance_reference' not in all_parameters:
            all_parameters['wl_absorbance_reference']=0
        
        if 'wl_calibration_mode' not in all_parameters:
            all_parameters['wl_calibration_mode']=0
        if 'peak_search_delta' not in all_parameters:
            all_parameters['peak_search_delta']=0.01
        
        if 'peak_search_start_index' not in all_parameters:
            all_parameters['peak_search_start_index']=0
                
        if 'aligner_interp_factor' not in all_parameters:
            all_parameters['aligner_interp_factor']=1
        
        if 'zero_offset_range' not in all_parameters:
            all_parameters['zero_offset_range']= np.zeros(2,dtype=np.int)
        if 'aligner_algorithm' not in all_parameters:
            all_parameters['aligner_algorithm']= 0

        self.ui.PLS2_wl_smoothing_kernel.setValue(all_parameters['smoothing_kernel_half_size'])
        self.ui.PLS2_wl_peak_search_minimum.setValue(all_parameters['peak_search_minimum'])
        self.ui.PLS2_wl_peak_search_delta.setValue(all_parameters['peak_search_delta'])
        self.ui.PLS2_wl_peak_fitting_kernel.setValue(all_parameters['peak_fitting_kernel_half_size'])
        self.ui.PLS2_wl_calibration_order.setValue(all_parameters['calibration_order'])
        self.ui.PLS2_wl_peak_search_window.setValue(all_parameters['peak_search_window'])
        self.ui.PLS2_wavelength_calibraton_mode_comboBox.setCurrentIndex(all_parameters['wl_calibration_mode'])
        self.ui.PLS2_aligner_threshold.setValue(all_parameters['aligner_threshold'])
        self.ui.PLS2_aligner_interp_factor.setValue(all_parameters['aligner_interp_factor'])
        self.ui.PLS2_alignMethod.setCurrentIndex(all_parameters['enable_aligner'])
        self.ui.PLS2_alignAlgorithm.setCurrentIndex(all_parameters['aligner_algorithm'])
        self.ui.PLS2_aligner_num_sets.setValue(all_parameters['aligner_num_sets'])
        self.ui.PLS2_nr_meas_to_int.setValue(all_parameters['num_sets'])
        self.ui.PLS2_wl_peak_search_start_index.setValue(all_parameters['peak_search_start_index'])
        self.ui.PLS2_raw_offset_start_index.setValue(all_parameters['zero_offset_range'][0])
        self.ui.PLS2_raw_offset_end_index.setValue(all_parameters['zero_offset_range'][1])

    def reinit_pls2_controls_from_instance(self):
        '''
        Reinits all the controls of the PLS2 instance with the values from the instance
        This is needed to update the controls if parameters are loaded from file.
        '''
        if self.pls2.initialised:
            self.block_signals(True, 'PLS2')
            # global parameters
            self.ui.PLS2_nr_meas_to_int.setValue(self.pls2.num_sets)
            self.ui.PLS2_alignMethod.setCurrentIndex(self.pls2.enable_aligner)
            self.ui.PLS2_alignAlgorithm.setCurrentIndex(self.pls2.aligner_algorithm)
            self.ui.PLS2_aligner_threshold.setValue(self.pls2.aligner_threshold)
            self.ui.PLS2_aligner_interp_factor.setValue(self.pls2.aligner_interpolation_factor)
            self.ui.PLS2_aligner_num_sets.setValue(self.pls2.aligner_num_sets)
            self.ui.PLS2_raw_offset_start_index.setValue(self.pls2.zero_offset_range[0])
            self.ui.PLS2_raw_offset_end_index.setValue(self.pls2.zero_offset_range[1])
            # PLS2 parameters
            self.ui.PLS2_model_path_read.setText(self.pls2.calibration_model_path)
            self.ui.PLS2_used_triangle_side.setCurrentIndex(self.pls2.used_absorbance)
            self.ui.PLS2_process_batchwise.setChecked(self.pls2.process_batchwise)
            self.pls2_populate_model_list_widget()
            self.ui.PLS2_model_number.item(self.pls2.current_model_used).setSelected(True)
            self.populate_nr_events_table_widget()
            self.pls2_populate_concentrations_plot_listWidget()
            # Wavelength calibration
            self.ui.PLS2_wl_smoothing_kernel.setValue(self.pls2.smoothing_kernel_half_size)
            self.ui.PLS2_wl_peak_search_minimum.setValue(self.pls2.peak_search_minimum)
            self.ui.PLS2_wl_peak_search_window.setValue(self.pls2.peak_search_window)
            self.ui.PLS2_wl_peak_search_delta.setValue(self.pls2.peak_search_delta)
            self.ui.PLS2_wl_peak_fitting_kernel.setValue(self.pls2.peak_fitting_kernel_half_size)
            self.ui.PLS2_wl_peak_search_start_index.setValue(self.pls2.peak_search_start_index)
            self.ui.PLS2_wl_calibration_order.setValue(self.pls2.calibration_order)
            self.ui.PLS2_wavelength_calibraton_mode_comboBox.setCurrentIndex(self.pls2.wavelength_calibration_mode)
            self.ui.PLS2_wl_peak_positions.setColumnCount(40)
            self.ui.PLS2_wl_peak_positions.setRowCount(1)
            self.ui.PLS2_wl_peak_positions.setSortingEnabled(False)
            cal_wavelengths = 10**7 / self.pls2.wl_calibration_peak_wavenumbers
            cal_wavelengths = np.append(cal_wavelengths, np.zeros(40 - len(cal_wavelengths)))
            for idx, peak in enumerate(cal_wavelengths):
                # print idx, peak
                newitem = QtWidgets.QTableWidgetItem(str(peak))
                self.ui.PLS2_wl_peak_positions.setItem(0, idx, newitem)
            self.ui.PLS2_wl_peak_positions.resizeColumnsToContents()
            self.ui.PLS2_wl_peak_positions.resizeRowsToContents()

            # processing parameters
            self.ui.PLS2_statistics_size.setValue(self.pls2.statistics_num_sets)
            self.block_signals(False, 'PLS2')

def run():
    '''
    start the RDP simulator
    '''
    app = QtWidgets.QApplication([])  # @UnusedVariable
    win = RDPsimulator()  # @UnusedVariable
    #hf.pydevBrk()
    win.show()
    #this shutdown procedure is a bit strange but with the standard method (sys.exit(app.exec_())) 
    #the destructor of the RDPsimulator will never be called and all the spaned processes wil not be 
    #terminated correctly.
    app.exec_()
    win.__del__()
    app.quit()
    #sys.exit(app.exec_())
    #QtWidgets.QApplication.instance().exec_()
    

if __name__ == '__main__' :
    print("python version: "+str(sys.version_info))
    run()
    
    
