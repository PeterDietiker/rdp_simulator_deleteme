'''
provides class Statistics
'''



import numpy as np
import matplotlib
matplotlib.use('TKAgg')
import matplotlib.pyplot as plt
import os
import sys
import time
import math
import threading
import pickle


#sys.path.append(os.path.dirname(sys.path[0]))
if __package__ is None:
    sys.path.insert(1, os.path.join(os.path.dirname(__file__), os.path.pardir))
    __package__ = 'simulator'  # @ReservedAssignment
    import RDP_simulator  # @UnusedImport
import RDP_simulator.helper_functions  as hf
import RDP_simulator.circular_buffer as cb
#import circular_buffer_numpy.circular_buffer as cb



#import rpdb2


class Result_buffer(cb.CircularBuffer):
    '''
    Class for calculating some statistics of the results including an allan plot
    '''
    # pylint: disable=too-many-instance-attributes

    def __init__(self, shape, dtype = "float64", inflatable = False):

        super().__init__(shape, dtype)
        self.inflatable = inflatable
        
        self.num_sets = None
        self.num_comp = None
        self.tuning_frequency = None
        self.tauliste = None
        self.tauplotliste = None
        self.tauliste_std = None
        self.tauplotliste_std = None
        self.allan_plot = None
        self.mean_variance = None
        self.allan_figure = None
        self.ax = None
        self.ax2 = None
        self.color_list = None

    def __del__(self):
        pass

    @property
    def mean(self):  #pylint: disable=missing-docstring
        self._mean = np.mean(self.buffer[:self.nr_items], axis=self.buffer.ndim-1)
        return self._mean

    @property
    def std(self):  #pylint: disable=missing-docstring
        self._std = np.nanstd(self.buffer[:self.nr_items], axis=self.buffer.ndim-1)
        return self._std
    
    @property
    def cv(self):  #pylint: disable=missing-docstring
        self._cv = self.std/self.mean
        return self._cv

    @property
    def data(self):  #pylint: disable=missing-docstring
        return self.get_all()
    
    @property
    def nr_items(self):#pylint: disable=missing-docstring
        if self.full:
            nr_items = self.length
        else:
            nr_items = self.pointer+1
        return nr_items

    
    def append(self, data):
        '''
        renews data from the memory mapped file
        '''
        if self.inflatable:
            if self.pointer == self.get_length()-1:
                self.change_length(self.get_length()*2)
        cb.CircularBuffer.append(self,np.atleast_1d(data))
        
    def set_inflatable(self,inflatable):
        self.inflatable = inflatable
        
    def change_length(self, length):
        '''
        changes the lengt of the buffer
        if length == -1, the size of the buffer will dynamically increase
        '''
        cb.CircularBuffer.change_length(self, length)

    def save_data(self, path):
        '''
        Saves the data in a pickle file for later analysis
        :param path: path the the save file
        :type path: string
        '''
        with open(path, 'wb') as handle:
            pickle.dump(self.get_all_valid(), handle, protocol=pickle.HIGHEST_PROTOCOL)

    def load_data(self, path):
        '''
        loads data from a pickle file
        :param path: path to pickle file
        :type path: string
        '''
        try:
            with open(path, 'rb') as handle:
                self.append(pickle.load(handle))
        except IOError:
            pass


    ########################
    # stuff for allan plots
    ########################

    def calc_moving_average(self, data, n):  #pylint: disable=no-self-use
        '''
        Calculates a moving average with convolution
        :param data: data
        :type data: np.array
        :param n: number of elements to average
        :type n: int
        '''
        ker = np.full(n, 1. / n)
        ave = np.convolve(data, ker, 'valid')
        return ave


    def calc_kernel(self, tau):  #pylint: disable=no-self-use
        '''
        calculates the kernel for the correlation
        :param tau: tau
        :type tau: int
        '''
        kernel = np.insert(np.zeros(tau - 1), [0, tau - 1], [-1, 1])
        return kernel

    def calc_allanvar(self, data, tau):
        '''
        calculates the allan variance at a certain tau
        :param data: data
        :type data: np.array
        :param tau: tau
        :type tau: int
        '''
        # relativ
        #        avar=0.5*np.mean((np.correlate(self.calc_moving_average(x,t),self.calc_kernel(t))/np.absolute(np.mean(x)))**2)
        # absolut
        avar = 0.5 * np.mean((np.correlate(self.calc_moving_average(data, tau), self.calc_kernel(tau)))**2)
        return avar

    def calc_meanvar(self, data):
        '''
        calculates the variance of the mean of data from element 0 to t for t in self.tauliste
        :param data: data
        :type data:np.ndarray
        '''
        meanvar = [np.var(data[0:tau]) / (tau - 1) for tau in self.tauliste_std]
        return meanvar

    def calc_taulist(self, data):
        '''
        calculates the list of the tau-values for which the allan variance is calculated
        The used tau values are the power of twos up the the length of data. But we skip the last datapoint
        because of the large uncertainty
        :param data: data
        :type data: np.array
        '''
        npts = int(np.floor(np.log2(data))) - 1  # skip one sample-variance
        liste = np.zeros(npts)
        for i in np.arange(0, npts):
            liste[i] = 2**(i + 1)
        self.tauliste = np.array(liste[:-1]).astype(int)  # skip last point as variance is too lrage
        self.tauplotliste = (self.tauliste / self.tuning_frequency * 1000).astype(int)

        # add total length for the std deviation of mean
        self.tauliste_std = np.append(liste, data).astype(int)
        self.tauplotliste_std = (self.tauliste_std / self.tuning_frequency * 1000).astype(int)

        # fuer
#        self.tauplotliste=self.tauliste
#        self.taulistestack=np.tile(self.tauliste,(1,num_comp))

    def calc_allanplot(self, data):
        '''
        calculates the allan variance for every tau in self.tauliste
        :param data: data
        :type data: np.array
        '''
        allanplot = [self.calc_allanvar(data, i) for i in self.tauliste]
        # allanplot=np.column_stack((taulist(x),allanplot))
        return allanplot

    def calc_allan(self):
        '''
        calculates the allan plot for every component
        '''
        self.allan_plot = np.zeros((self.num_comp, len(self.tauliste)))
        if self.num_sets > 20:
            for comp in np.arange(0, self.num_comp):
                self.allan_plot[comp] = self.calc_allanplot(self._data[comp, :])
        return 0

    def calc_mean(self):
        '''
        calculates the variance of the mean for every component
        '''
        self.mean_variance = np.zeros((self.num_comp, len(self.tauliste_std)))
        if self.num_sets > 20:
            for comp in np.arange(0, self.num_comp):
                self.mean_variance[comp] = self.calc_meanvar(self._data[comp, :])
        return 0

    def plot_allanplot(self, fig=None, ax=None, components=None, save_path=None):
        '''
        Calculates the allan plot and plots it.
        :param fig: figure to use. If None a new figure is generated
        :type fig: matplotlib figure
        :param ax: axis to use. If None, a new axis is generated
        :type ax: matplotlib axis
        :param components: name of the components
        :type components: array of strings
        :param save_path: path to save the plot
        :type save_path: string
        '''
        if len(self.tauplotliste) > 0 and self.num_comp > 0 and np.isnan(self.allan_plot).any() != True:
            # plt.cla()
            if ax is None:
                self.ax.lines = []
                ax = self.ax
                ax2 = self.ax2
            else:
                ax2 = ax[1]
                ax = ax[0]
            if components is None:
                components = ["component " + str(int(comp + 1)) for comp in range(self.num_comp)]
             #last component is water if more than one component is present
            nr_to_plot = self.num_comp-1 if self.num_comp>1 else 1
            for comp in np.arange(0, nr_to_plot): #last component is water
                #                self.allan_plotstack=np.vstack((self.allan_plotstack,self.allan_plot[comp]))
                #            self.line.set_data(self.taulistestack,self.allan_plot)
                if min(self.allan_plot[comp]) > 0:  # only plot if fitted
                    ax.plot(self.tauplotliste, self.allan_plot[comp], color=self.color_list[int(comp % len(self.color_list))],
                            linewidth=2.0, label=components[comp])
                if min(self.mean_variance[comp]) > 0:  # only plot if fitted
                    ax.plot(self.tauplotliste_std, self.mean_variance[comp], color=self.color_list[int(comp % len(self.color_list))],
                            linestyle='-.', linewidth=2.0)
#            self.line.set_ydata(self.allan_plot[comp])

            # Diese artits werden gebraucht fuer die zweite legende
            legend2_artists = [plt.Line2D((0, 1), (0, 0), color='b', linestyle=['-', '-.'][idx],
                                          label=['Allan variance', 'Variance of mean'][idx]) for idx in range(2)]
            ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
            #legend2=plt.legend(legend2_artists, ['Allan variance','Variance of mean'],bbox_to_anchor = (1.38, 0.2))
            #ax.draw_artist(ax.patch)
            ax.set_yscale('log')
            ax.set_xscale('log')
            ax.set_adjustable("datalim")
            ax.set_ylabel(r'$\sigma_y^2\ (\tau)\ /ppm^2$')
            ax.set_xlabel(r'$\tau\ /\ ms$')
            #ax.set_xlim(self.tauplotliste[0]*0.9, self.tauplotliste[-1]*1.1)
            # 2 zweite Achse (als funktion der Anzahl messungen
            hf.set_plot_limits(ax, 'x')
            hf.set_plot_limits(ax, 'y')
            ax_xticks = ax.get_xticks()
            ax2.set_xscale('log')

            # self.allan_figure.subplots_adjust(top=0.85)

            ax2_xticks = []
            for tick in ax_xticks:
                ax2_xticks.append(math.floor(tick * self.tuning_frequency / 1000))

            ax2.set_xticks(ax_xticks)
            ax2.set_xbound(ax.get_xbound())
            ax2.set_xticklabels(ax2_xticks)
            ax2.set_xlabel(r'$\tau\ /\ nr.\ of\ scans$')
            #===================================================================
            #
            # if min(self.allan_plot.flatten())*0.5<self.ax.get_ylim()[0]:
            #     self.ax.set_ylim(min(self.allan_plot.flatten())*0.5,self.ax.get_ylim()[1])
            # if max(self.allan_plot.flatten())*1.5>self.ax.get_ylim()[1]:
            #     self.ax.set_ylim(self.ax.get_ylim()[0],max(self.allan_plot.flatten())*1.5)
            # self.ax.set_ylim(min(self.allan_plot.flatten())*0.8,max(self.allan_plot.flatten())*1.5)
            #===================================================================
            ax.set_aspect("auto")
            self.allan_figure.subplots_adjust(right=0.75)
            self.allan_figure.canvas.draw()
            try:
                self.allan_figure.canvas.flush_events()
            except:
                # not a pyqt canvas
                pass
#            self.allan_figure.show()
#            plt.draw()
        if save_path is not None:
            try:
                self.allan_figure.savefig(save_path, dpi=300)
            except IOError:
                print('wrong path')
        return 0

    def start_plot(self):
        '''
        Starts the allan plot
        '''
        if __name__ == "__main__":
            self.allan_figure = plt.figure(figsize=(12, 6))
        else:
            self.allan_figure = plt.figure()
        self.ax = self.allan_figure.add_subplot(111)
        self.ax2 = self.ax.twiny()
        self.allan_figure.canvas.set_window_title("Allan plot of the concentrations, pid: {:d}".format(os.getpid()))
        self.ax.set_ylabel(r"$\sigma_y^2 (\tau)")
        self.ax.set_xlabel("$\tau / s")
        # liste der farben fuer die linien
        self.color_list = ['b', 'r', 'c', 'm', 'g']

        plt.show(block=False)


if __name__ == "__main__":
    buffer = Result_buffer(10,"int")
    for i in range(5):
        buffer.append(i)
    print(buffer.get_all_valid().flatten())
    buffer.change_length(13)
    buffer.append(np.array([11,12]))
    buffer.append(np.array([13]))
    print(buffer.get_all_valid().flatten())
    print(buffer.pointer)

