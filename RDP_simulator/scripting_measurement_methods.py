from RDP_simulator.scripting_methods import ScriptingMethods as sm
from sierra_860 import MFCManager
from sierra_860 import generate_config_dict
from ellx import ELLx
import time

def update_offset(nr_scans = 100, raw_start = None, raw_stop = None):
    sm.set_pls2_nr_integrate(nr_scans)
    sm.take_measurements(nr_scans)
    sm.update_pls2_raw_offset(raw_start,raw_stop)
    sm.update_pls2_offset()
    sm.start_measurement(0)

def generate_model(nr_scans = 100,model_path = None,wl_range = [3.305,3.42], npts = 2000, ncomp = 1):
    if model_path is not None:
        sm.set_pls2_model_parameter(model_path,wl_range,npts,ncomp)
    sm.set_conc("Methane",0,1)
    mfcm.close_all()
    mfcm.set_flow({"N2":50})
    time.sleep(60)
    sm.take_measurements(1)
    sm.save_data_for_model_continuous(True)
    sm.flush_stream()
    sm.take_measurements(nr_scans)
    sm.save_data_for_model_continuous(False)
    sm.start_measurement(0)
    mfcm.close_all()
    mfcm.set_flow({"Methane":500})
    time.sleep(2*60)
    mfcm.set_flow({"Methane":50})
    time.sleep(10)
    sm.set_conc("Methane",4.97,1)
    sm.take_measurements(1)
    sm.flush_stream()
    sm.save_data_for_model_continuous(True)
    sm.take_measurements(nr_scans)
    sm.save_data_for_model_continuous(False)
    sm.build_pls2_model()
    sm.start_measurement(0)

def calibrate_wl(nr_scans = 100):
    sm.set_pls2_model(calculate_conc=False)
    sm.set_pls2_nr_integrate(nr_scans)
    sm.take_measurements(nr_scans)
    sm.set_channel_2(2)
    elliptec.host_backward()
    time.sleep(2)
    sm.set_channel_2(1)
    sm.set_pls2_nr_integrate(nr_scans)
    sm.flush_stream()
    sm.take_measurements(nr_scans)
    sm.set_channel_2(2)
    elliptec.host_forward()
    time.sleep(2)
    sm.set_channel_2(1)
    sm.flush_stream()
    sm.take_measurements(nr_scans)
    sm.update_pls2_calibration_parameters()
    sm.take_measurements(1)
    sm.set_channel_2(2)
    elliptec.host_backward()
    time.sleep(2)
    sm.set_channel_2(0)
    sm.flush_stream()
    sm.start_measurement(0)

