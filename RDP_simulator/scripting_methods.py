'''
Created on 6 Apr 2018

@author: pdietiker
'''




import time
import os
import numpy as np
from pyqode.qt import QtCore, QtGui
import weakref

import RDP_simulator.helper_functions  as hf
#from pyqtgraph.qt import QtGui, QtCore
class ScriptingMethods(object):
    '''
    This class contains all the methods available for scripting
    
    It sends all the commands to the main thread with the help of the method _send_command und control of the mutex
    '''

    def __init__(self, pyqode_handler_instance,logger_instance,execution_mutex, execution_wait,command_signal):
        '''
        Constructor
        '''
        #DGA is the main application class and contains all the methods needed
        #self.RDP = weakref.ref(pyqode_handler_instance.parent())
        self.RDP = pyqode_handler_instance.parent
        #the pyqode_handler contains the method used to pause the script
        #it needs to be a weak reference to save us from cylcing references
        self.pyqode_handler = weakref.ref(pyqode_handler_instance)
        self.logger = logger_instance
        #mutex and execution_wait for synchronisation
        self.execution_mutex = execution_mutex
        self.execution_wait = execution_wait
        #signal which emited for every command
        self.command_signal = command_signal
        #special variable which is used to set concentrations
        # it is the number of times set_conct() was called
        self.set_conc_index = 0 
    
    def __del__(self):
        print("Scripting methods deleted")
        pass

    def load_parameters(self,file_path):
        '''
        load parameters from file
        
        :param file_path: path to parameter file
        :type file_path: str
        '''
        
        if file_path is not None:
            if os.path.isfile(file_path):
                self._send_command(self.RDP.load_parameters, file_path=file_path)
                #self.RDP.load_parameters(file_path)
                
    
    def start_measurement(self, n=None):
        '''
        Acquires n measurement. 
        Caveat: This method does not wait until all measurements are acquired.
        Use take_measurment() insteat. 
        
        :param n: number of measurements to acquire
        :type n: int
        '''
        if n < 0:
            self.logger.warning('n must be >= zero')
        else:
            self._send_command(self.RDP.ui.nr_meas_to_acquire.setValue,n)
            if not self.RDP.ui.start_simulation.isChecked():
                self._send_command(self.RDP.ui.start_simulation.click)

    def take_measurements(self, n=1):
        '''
        Starts acquisition of n measurements and waits until all measurements are acquired
        
        :param n: number of measurements to acquire
        :type n: int
        '''
        if n < 0:
            self.logger.warning('n must be >= zero')
        else:
            if  self.RDP.ui.start_simulation.isChecked():
                self._send_command(self.RDP.ui.start_simulation.click) 
            self._send_command(self.RDP.ui.nr_meas_to_acquire.setValue,n)

        self._send_command(self.RDP.ui.start_simulation.click) 
        while self._is_running():
            time.sleep(0.01)
    
    
    def _is_running(self):
        '''
        Checks of measurements are running
        '''
        return self.RDP.ui.start_simulation.isChecked()
    
    
    
    def set_stream(self,enable=None,file_path=None, mode=None):
        '''
        Set stream parameters
        
        :param file_path: path to file
        :type file_path: str
        :param mode: "load" for stream from file, "dump" for stresm to file
        :type mode: str
        :param enable: true for enable, false for disable stream
        :type enable: bool
        '''
        if file_path is not None:
            self._send_command(self.RDP.ui.simulation_stream_path.setText,file_path)

        mode_list = ['dump', 'load']
        
        if mode is not None:
            if mode in mode_list:
                index = mode_list.index(mode)
                self._send_command(self.RDP.ui.simulation_stream_mode.setCurrentIndex,index)
            else:
                self.logger.warning('mode must be one of ' + str(mode_list))

        if enable is not None:
            #stream was enabled before. disable stream to reload the file
            if (enable is True) and self.RDP.ui.simulation_stream_checkbox.isChecked():
                self._send_command(self.RDP.ui.simulation_stream_checkbox.setChecked,False)
                self._send_command(self.RDP.connect_stream_parameters)
            self._send_command(self.RDP.ui.simulation_stream_checkbox.setChecked,bool(enable))
        
        self._send_command(self.RDP.connect_stream_parameters)
    
    def flush_stream(self):
        '''
        flushs the zmq stream
        '''
        self._send_command(self.RDP.ui.simulation_zmq_flush_btn.click)
    
    
    def set_laser_parameters(self,power=None, wavelength = None, waist = None, m_squared = None, invoke_updater=True):
        '''
        sets the laser parameters
        @param power: Power of the laser in mW
        @param wavelength:Wavelength in nm
        @param waist: Waist of the Laser in um
        @param m_squared : Beam quality factor m_squared
        '''
        self._send_command(self.RDP.sig_sim.set_laser_parameters,power,wavelength,waist,m_squared)
        self._send_command(self.RDP.init_controls,'laser')
        
    def set_additional_parameters(self, time_resolution = None):
        '''
        Sets the additional parameter
        :param time_resolution: Time resolution of the simulation in us
        '''
        self._send_command(self.RDP.sig_sim.set_additional_parameters,time_resolution)
        self._send_command(self.RDP.init_controls,'additional')
        
    def set_optics_parameters(self, f_objective = None, f_laser_collimation = None, f_laser_collimation_dist = None,\
                             laser_to_objective_dist = None, na_objective = None, f_relay = None, r_pinhole = None, detector_efficiency = None,\
                            optics_efficiency = None, detector_dead_time = None):
        '''
        sets the optics parameters
        Parameters
        ----------
        :param f_objective:                 Focal length of objective in mm
        :param f_laser_collimation:        Focal length of laser collimation lens in mm
        :param f_laser_collimation_dist:    Dist from Laser to collimation lens in mm
        :param laser_to_objective_dist:    Dist from Laser to objective in mm
        :param na_objective:                Numerical aperture of objective
        :param f_relay:                    Focal length of relay lens in mm
        :param r_pinhole:                    Radius of pinhole in um
        :param detector_efficiency:        Detector quantum efficiency
        :param optics_efficiency:            Optics efficiency
        :param detector_dead_time:        Dead time of the detector in ns
        '''
        
        self._send_command(self.RDP.sig_sim.set_optics_parameters,f_objective,f_laser_collimation, f_laser_collimation_dist ,\
                             laser_to_objective_dist, na_objective , f_relay , r_pinhole , detector_efficiency ,\
                            optics_efficiency , detector_dead_time)
        
        self._send_command(self.RDP.init_controls,'optics')
        
    def set_scan_parameters(self, xy_speed = None, xy_scan_dist = None, objective_spin_radius = None,\
                             objective_rps = None, scan_time = None, bin_length = None):
        '''
        sets the scan parameters
        Parameters
        ----------
        :param xy_speed:                 Speed of XY stage in mm/s
        :param xy_scan_dist:            Scan distance of XY stage (lateral length of square) in mm
        :param objective_spin_radius:    Radius of objective excenter in um
        :param objective_rps:            Objective rotation speed in rps
        :param scan_time:                Total scan time in s
        :param bin_length:               Bin length in um
        :param invoke_updater:
        '''
        
        self._send_command(self.RDP.sig_sim.set_scan_parameters, xy_speed , xy_scan_dist , objective_spin_radius ,\
                             objective_rps , scan_time , bin_length )
        
        self._send_command(self.RDP.init_controls,'scan')
    
    def set_noise_parameters(self, method=None, std = None, mean = None, detector_darknoise = None,  scale_noise = None, file_path = None, invoke_updater=True):
        '''
        sets the noise parameters
        @param method: 'Normal' for normal distributed noise, 'poisson' for poisson noise or 'file' for a measured noise
        @param std: Standard deviation of noise
        @param mean: Mean value of the noise, use this to set a poissonian noise
        @param detector_darknoise : dark count rate of the detector in Hz
        @param scale_noise:  If True, noise is scaled with pinhole radius and laser power
        @param file_path: path to file with measured noise
        '''
        
        self._send_command(self.RDP.sig_sim.set_noise_parameters,  method, std , mean , detector_darknoise , scale_noise, file_path )
        
        self._send_command(self.RDP.init_controls,'noise')
    
    def set_dye_parameters(self, dye=None, conc = None, epsilon = None, lifetime = None,  lifetime_isc=None,lifetime_triplet = None, quantum_efficiency = None, invoke_updater=True):
        '''
        sets the noise parameters
        @param dye: dye for a particular dye or 'manual'
        @param conc: Konzentration if fM
        @param epsilon: Mean value of the noise, use this to set a poissonian noise
        @param lifetime : lifetime of the excitet state in ns
        @param lifetime_isc: Inverse of intersystem crossing rate in us
        @param lifetime_triplet: lifetime of the triplet state in us
        @param quantum_efficiency: quantum efficiency
        '''
        
        self._send_command(self.RDP.sig_sim.set_dye_parameters,dye, conc , epsilon , lifetime ,  lifetime_isc, lifetime_triplet , quantum_efficiency )
        
        self._send_command(self.RDP.init_controls,'dye')
    
    
    def set_calc_parameters(self, method = None, stats = None, threshold_quantile = None,\
                             normalisation = None, single_count = None):
        '''
        Sets the paramters for the number of events calculations
        :param method: selects the used method:
                        - local_baseline: use the baseline of the particular scan
                        - 0ppm_baseline: use a 0ppm baseline
                        - rdp: use RDP algorithm
                        - 
        :param stats:    Select the distribution of the noise. either "poisson" or "normal"
        :param threshold_quantile: Threshold in terms of the quantile of the noise distribution
        :param normalisation: if >0, the nr of events is normalised to the given scan length in s
        :param single:count : if True, it is assured that a molecule is counted only once, even if multiple bins are above threshold
        '''
        
        self._send_command(self.RDP.sig_sim.set_calc_parameters,method , stats , threshold_quantile ,\
                             normalisation , single_count )
        
        self._send_command(self.RDP.init_controls,'calc')
        
    def reset_results_df(self):
        '''
        Reset the resuts data frame
        '''
        self._send_command(self.RDP.sig_sim.results_df.reset_df)
        
    def plot_results(self,x_axis,clear = True, **kwargs):
        '''
        Plots the nr of events as a function of the given parameter
        :param x_axis: Parameter used as x axis
        '''
        self._send_command(self.RDP.plot_results,x_axis, init = True,clear = clear, **kwargs)
        
    def init_pls2(self,init=True):
        '''
        inits or deinits the pls2 library
        
        :param init: true to init pls2 else false
        :type init:bool
        '''
        self._send_command(self.RDP.ui.PLS2_init_Library.setChecked,bool(init))
        #self.RDP.init_pls2(init)
              
    def set_pls2_nr_integrate(self,n):
        '''
        Set number of measurements to be integrated
        
        :param n: number of measurements to integrate
        :type n: int
        '''
        if n <= 0:
            self.logger.warning('n must be > zero')
        else:
            self._send_command(self.RDP.ui.PLS2_nr_meas_to_int.setValue,n)
    
    def set_pls2_model(self,path=None,model_to_use=None,calculate_conc=None):
        '''
        Sets the PLS2 model parameters
        
        :param file_path: path to model file
        :type file_path: str
        :param model_to_use: index of model to use
        :type model_to_use: int
        :param calculate_conc: True if concentrations will be calculated
        :type calculate_conc: bool
        '''
        if path is not None:
            if os.path.exists(path):
                self._send_command(self.RDP.ui.PLS2_model_path_read.setText,path)
            else:
                self.logger.warning('PLS2 model path not found: ' + path)
        
        if model_to_use is not None:
            if model_to_use <=self.RDP.ui.PLS2_model_number.count():
                self._send_command(self.RDP.ui.PLS2_model_number.setCurrentRow,model_to_use)
            else:
                self.logger.warning('model nr: {} not available'.format(model_to_use))
        
        if calculate_conc is not None:
            self._send_command(self.RDP.ui.PLS2_calculate_concentrations.setChecked,bool(calculate_conc))
    
    def update_pls2_offset(self):
        '''
        Updates the absorbance offsets
        '''
        self._send_command(self.RDP.ui.PLS2_update_absorbance_offset_button.click)
    
    def update_pls2_raw_offset(self,start=None,end=None):
        '''
        Updates the raw offset correction parameters and performs a correction
        :param start: first index of background signal range
        :type start: int
        :param end: last index of background signal range
        :type end: int
        '''
        if start is not None:
            if start < 0:
                self.logger.warning('start index must be >= 0')
            else:
                self._send_command(self.RDP.ui.PLS2_raw_offset_start_index.setValue,start)
        if end is not None:
            if end < 0:
                self.logger.warning('stop index must be >= 0')
            elif end<self.RDP.ui.PLS2_raw_offset_start_index.value():
                self.logger.warning('stop index must be >= start index')
            else:
                self._send_command(self.RDP.ui.PLS2_raw_offset_end_index.setValue,end)
        self._send_command(self.RDP.ui.PLS2_update_raw_offset_button.click)
        
    
    def update_pls2_calibration_parameters(self,calibrate=True,sk=None,psm=None,psw=None,psd=None,pfk=None,pssi=None,co=None,mode=None):
        '''
        updates the wavelength calibration parameters and recalibrates the spectrum. 
        
        :param calibrate: If True the spectrum will be recalibrated
        :type calibrate: bool
        :param sk: smoothing kernel
        :type sk: int
        :param psm: peak search minimum
        :type psm: float
        :param psw: peak search window
        :type psw: int
        :param psd: peak search delta
        :type psd: float
        :param pfk: peak fitting kernel
        :type pfk: int
        :param pssi: peak search start index
        :type pssi: int
        :param co: calibration order
        :type co: int
        :param mode: calibration mode:\n
                        1: Use Wavelength reference as signal and power reference as reference\n
                        2: Use Wavelength reference as signal and channel 1 as reference\n
                        3: Use channel 1 as signal and power reference as reference
        :type mode: int
        '''
        
        #Block signals. Otherwise self.RDP.pls2_update_wavelength_calibration_parameters() will be envoked after every change.
        self._send_command(self.RDP.block_signals,True, 'PLS2')
        if sk is not None:
            if sk < 1:
                self.logger.warning('smooting kernel must be >= 1')
            else:
                self._send_command(self.RDP.ui.PLS2_wl_smoothing_kernel.setValue,sk)
        if psm is not None:
            if psm < 0:
                self.logger.warning('peak search minimum must be >= 0')
            else:
                self._send_command(self.RDP.ui.PLS2_wl_peak_search_minimum.setValue,psm)
        if psw is not None:
            if int(psw) < 1:
                self.logger.warning('peak search width must be >=1')
            else:
                if not isinstance(psw,(int,int)):
                    self.logger.warning('peak search window was casted to integer: '+str(int(psw)))
                self._send_command(self.RDP.ui.PLS2_wl_peak_search_window.setValue,int(psw))

        if psd is not None:
            if psd < 0:
                self.logger.warning('peak search delta must be >=0')
            else:
                self._send_command(self.RDP.ui.PLS2_wl_peak_search_delta.setValue,psd)

        if pfk is not None:
            if int(pfk) < 1:
                self.logger.warning('peak fitting kernel must be >=1')
            else:
                if not isinstance(pfk,(int,int)):
                    self.logger.warning('peak fitting kernel was casted to integer: '+str(int(pfk)))
                self._send_command(self.RDP.ui.PLS2_wl_peak_fitting_kernel.setValue,int(pfk))

        if pssi is not None:
            if int(pssi) < 0:
                self.logger.warning('peak search start index must be >=0')
            else:
                if not isinstance(pssi,(int,int)):
                    self.logger.warning('peak search start index was casted to integer: '+str(int(pssi)))
                self._send_command(self.RDP.ui.PLS2_wl_peak_search_start_index.setValue,int(pssi))
        
        if co is not None:
            if int(co) < 1:
                self.logger.warning('calibration order must be >=1')
            else:
                if not isinstance(co,(int,int)):
                    self.logger.warning('calibration order was casted to integer: '+str(int(co)))
                self._send_command(self.RDP.ui.PLS2_wl_calibration_order.setValue,int(co))
            
        if mode is not None:
            if mode not in list(range(1,4)):
                self.logger.warning('calibration mode must be in [1,2,3]')
            else:
                self._send_command(self.RDP.ui.PLS2_wavelength_calibraton_mode_comboBox.setCurrentIndex,mode-1)
        
        self._send_command(self.RDP.block_signals,False, 'PLS2')
        
        if any([sk,psm,psw,psd,pfk,pssi,co,mode]):
            self._send_command(self.RDP.pls2_update_wavelength_calibration_parameters)
        
        if calibrate:
            self._send_command(self.RDP.ui.PLS2_wl_calibrate_button.click)
        
    def update_pls2_calibration_peak_wavelengths(self,peak_positions=None):
        '''
        Updates the wavelengths calibration peak wavelengths
        
        :param peak_positions: peak wavelengths
        :type peak_positions: list
        '''
        
        if not isinstance(peak_positions,list):
            self.logger.warning("wl must be a list of wavelengths")
        else:
            self._send_command(self.RDP.block_signals,True, 'PLS2')
            self._send_command(self.RDP.ui.PLS2_wl_peak_positions.setColumnCount,40)
            self._send_command(self.RDP.ui.PLS2_wl_peak_positions.setRowCount,1)
            self._send_command(self.RDP.ui.PLS2_wl_peak_positions.setSortingEnabled,False)
            for idx, peak in enumerate(peak_positions):
                # print idx, peak
                newitem = QtWidgets.QTableWidgetItem(str(peak))
                self._send_command(self.RDP.ui.PLS2_wl_peak_positions.setItem,0, idx, newitem)
            self._send_command(self.RDP.ui.PLS2_wl_peak_positions.resizeColumnsToContents)
            self._send_command(self.RDP.ui.PLS2_wl_peak_positions.resizeRowsToContents)
            self._send_command(self.RDP.block_signals,False, 'PLS2')
            self._send_command(self.RDP.pls2_update_wavelength_calibration_parameters)
            
    def set_statistics_size(self,n):
        '''
        Sets the size of the concentrations statistics
        
        :param n:
        :type n:
        '''
        if n < -1:
            self.logger.warning('n must be >= -1')
        else:
            self._send_command(self.RDP.ui.nr_events_statistics_size.setValue,n)
            #event wird nicht ausgelöst, da nur der event editingFinished verbunden ist. 
            #Dieser wird aber durch obigen Befehl nicht getriggert. Darum hier manuell:
            self._send_command(self.RDP.update_statistics_size)
    
    def reset_statistics(self):
        '''
        resets the concentration statistics
        '''
        self._send_command(self.RDP.ui.nr_events_reset_statistics_button.click)
        
    def save_concentrations(self,file_path=None,note=None):
        '''
        Saves the concentrations in the statistics to a file
        
        :param file_path: file path to use. if None, the last set will be used
        :type file_path: str
        :param note: note to save with concentrations. If None, the last set will be used
        :type note: str
        '''
        
        if file_path is not None:
            self._send_command(self.RDP.ui.save_concentrations_path.setText,file_path)
        if note is not None:
            self._send_command(self.RDP.ui.save_concentrations_note.setPlainText,note)
        
        self._send_command(self.RDP.ui.save_concentrations_button.click)
    
    def save_absorbance(self,file_path=None,note=None):
        '''
        Saves the absorbance to a file
        
        :param file_path: file path to use. if None, the last set will be used
        :type file_path: str
        :param note: note to save with absorbances. If None, the last set will be used
        :type note: str
        '''
        
        if file_path is not None:
            self._send_command(self.RDP.ui.PLS2_save_absorbance_path.setText,file_path)
        if note is not None:
            self._send_command(self.RDP.ui.PLS2_save_absorbance_note.setPlainText,note)
        
        self._send_command(self.RDP.ui.PLS2_save_absorbance_button.click)
    
    
    def save_absorbance_continuous(self,state):
        '''
        Sets state of continuous absorbance saving
        :param state: new state
        :type state: bool
        '''
        
        self._send_command(self.RDP.ui.PLS2_save_absorbance_cont_CheckBox.setChecked,state)
        
        
    def set_pls2_model_parameter(self,file_path=None,wl_range=None,npts=None,ncomp=None):
        '''
        Set the parameter for pls2 model generation
        
        :param file_path: file path to new model
        :type file_path: str
        :param wl_range: wavelength range of model in um as list [start,stop]
        :type wl_range: list
        :param npts: number of points to interpolate
        :type npts: int
        :param ncomp: number of pls2 components
        :type ncomp: int
        '''
        if file_path is not None:
            #creat directory
            if not os.path.isdir(file_path):
                try:
                    os.makedirs(file_path)
                except (OSError, WindowsError):
                    self.logger.warning('wrong path:'+file_path)
                    return
            
            self._send_command(self.RDP.ui.PLS2_model_path_write.setText,file_path)
        
        if wl_range is not None:
            try:
                wl_range=np.array(wl_range,dtype=np.float)
            except ValueError:
                self.logger.warning("wl_range must be numeric")
                return
            if np.any(wl_range <= 0) or (wl_range[1]<=wl_range[0]):
                self.logger.warning("wl_range must be larger than zero and second element must be larger than first element")
            else:
                self._send_command(self.RDP.ui.PLS2_calibration_wavelength_range_start.setValue,wl_range[0])
                self._send_command(self.RDP.ui.PLS2_calibration_wavelength_range_stop.setValue,wl_range[1])
        
        if npts is not None:
            if npts<=1:
                self.logger.warning("npts must be larger than 1")
            else:
                self._send_command(self.RDP.ui.PLS2_number_of_calibration_points.setValue,int(npts))
        
        if ncomp is not None:
            if ncomp<=0:
                self.logger.warning("npts must be larger than 0")
            else:
                self._send_command(self.RDP.ui.PLS2_number_of_components.setValue,int(ncomp))
    
    def save_data_for_model(self):
        '''
        Saves the actual data for PLS2 model generation
        '''
        self._send_command(self.RDP.ui.PLS2_save_data_for_model_Button.click)
    
    def save_data_for_model_continuous(self,state):
        '''
        Sets state of continuous data saving for model generation
        :param state: new state
        :type state: bool
        '''
        
        self._send_command(self.RDP.ui.PLS2_save_data_for_model_cont_CheckBox.setChecked,state)
    
    def build_pls2_model(self):
        '''
        build a PLS2 model
        '''
        if not os.path.exists(str(self.RDP.ui.PLS2_model_path_write.text())):
            self.logger.warning("Path for PLS2 model not set")
        elif not self.RDP.ui.PLS2_number_of_components.value() > 0:
            self.logger.warning("Number of components not set") 
        else:
            self._send_command(self.RDP.ui.PLS2_build_model_Button.click)
    
    def set_alignment(self,method=None,threshold=None,factor=None,algorithm=None, n=None):
        '''
        Sets the alignment parameters
        
        :param method: alignment method:\n
                        1: No alignment
                        2: align on absorbance data
                        3: align on raw channel 1 data
                        4: align on raw channel 2 data
                        5: align on absorbance and raw channel 1 data
                        6: align on absorbance and raw channel 2 data
        :type method: int
        :param threshold: Only data points with signal intensity above threshold (relativ to maximum signal) are used for alignment
        :type threshold: double
        :param factor: interpolation factor
        :type factor: int
        :param algorithm: align algorithm:\n
                          1: correlation
                          2: regression on smoothed data (sensitive to slow variations)
                          3. regression on original data (intermediate sensitivity)
                          4. regression on difference between smoothed and original data (sensitive to fast variations)
        :param n: number of scans to integrate for alignment
        :type n: int
        '''
        if method is not None:
            if (method > 6) or (method <1):
                self.logger.warning('method must be between 1 and 6')
            else:
                self._send_command(self.RDP.ui.PLS2_alignMethod.setCurrentIndex,method-1)
        if algorithm is not None:
            if (algorithm > 4) or (algorithm <1):
                self.logger.warning('algorithm must be between 1 and 4')
            else:
                self._send_command(self.RDP.ui.PLS2_alignAlgorithm.setCurrentIndex,algorithm-1)
        if threshold is not None:
            if (threshold < 0) or (threshold >1):
                self.logger.warning('threshold must be between 0 and 1')
            else:
                self._send_command(self.RDP.ui.PLS2_aligner_threshold.setValue,threshold)
        if factor is not None:
            if int(factor) < 1:
                self.logger.warning('interpolation factor must be >=1')
            else:
                if not isinstance(factor,(int,int)):
                    self.logger.warning('interpolation factor was casted to integer: '+str(int(factor)))
                self._send_command(self.RDP.ui.PLS2_aligner_interp_factor.setValue,int(factor))
        if n is not None:
            if n <= 0:
                self.logger.warning('n must be > zero')
            else:
                self._send_command(self.RDP.ui.PLS2_aligner_num_sets.setValue,n)
    
    def set_lc3_stream(self,enable=None):
        '''
        enables or disables the LC3 streaming functionality
        :param enable: True if stram shall be enabled, otherwise False
        :type enable: bool
        '''
        
        if enable is not None:
            self._send_command(self.RDP.ui.simulation_enable_zmq_stream_checkbox.setChecked,bool(enable))

    def pause(self):
        '''
        pauses the script.
        
        The script can be resumed from the GUI
        '''
        self._send_command(self.pyqode_handler().on_pause_execution)
        

    def _send_command(self,command,*args,**kwargs):
        '''
        emits the signal command_signal with the command and the arguments under
        control of the mutex
        :param command: command to execute
        :type command: function
        '''
        self.execution_mutex.lock()
        self.command_signal.emit({"command":command,"args":args,"kwargs":kwargs})
        self.execution_wait.wait(self.execution_mutex)
        self.execution_mutex.unlock()
        
        
