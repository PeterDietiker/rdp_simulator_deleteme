'''
Created on 02.03.2020

@author: pdietiker
'''





import matplotlib.pyplot as plt
import matplotlib as mpl
import pyqtgraph as pg  # only used for plot_drift_curve()
from scipy.integrate import solve_ivp
import scipy.stats as st
import scipy.integrate
import scipy.special
import scipy.stats
import scipy.interpolate
import scipy.optimize
import sympy as sp
import sympy.stats as sps
import sympy.physics.optics as op
import sklearn.neighbors
from sklearn.neighbors import KernelDensity
import numpy as np
import pandas as pd
import logging
import os
import sys
import pickle
import glob
import zmq
import re
from io import IOBase
import io
import time
import shutil
from pathlib import Path

from obsub import event


# import modules
#sys.path.insert(1, os.path.join(os.path.dirname(__file__), os.path.pardir))
if __package__ is None or __package__ == "":
    sys.path.insert(1, os.path.join(os.path.dirname(__file__), os.path.pardir))
    print(sys.path)
    __package__ = 'RDP_simulator'  # @ReservedAssignment
    import RDP_simulator  # @UnusedImport
    
import RDP_simulator.helper_functions as hf
import RDP_simulator.result_buffer as rb
import RDP_simulator.Results_df as Results_df
import RDP_simulator.dye as dye

from RDP_simulator.pickle_streamer import pickle_streamer
from RDP_simulator.zmq_streamer import zmq_streamer
from RDP_simulator.RDP_streamer import rdp_streamer
#from simulator.gaussian_kde_2D import gaussian_kde_2D

mpl_style_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'plot_style.mplstyle')
plt.style.use(mpl_style_path)

#get rid of some overfloat warnings in self.detection_rate_lambdified
np.seterr(over='ignore')


class signal_simulator(object):
    '''
    This class allows for the simulation of fluorescence signal
    '''

    #Sympy variables
    error = sp.symbols('error',real=True)
    r = sp.symbols('r',real=True)
    x, y, z, lambdamax= sp.symbols('x y z lambdamax',real=True)
    
    
    def __init__(self, logger=None):
        '''
        Constructor
        '''
        #self.main_folder is the main folder of the RDP simulator all saves will be relativ to this folder
        #self.main_folder = r"C:\Daten\Python\RDP_simulator"
        self.main_folder=os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        self.init_logger(logger)
        self.scan_dict = {"xy_speed":2.5E-3,"xy_scan_dist":0.9E-3, "objective_spin_radius":213E-6,\
                         "objective_rps": 10, "scan_time" : 1, "bin_length" : 100E-6}
        self.additional_dict = {"time_resolution" : 10E-6, "nr_steps" : 1000, "nr_bins" : 10000}
        self.optics_dict = {"f_objective" : 3E-3, "f_laser_collimation" : 5.5E-3, "f_laser_collimation_dist":5.5E-3,"laser_to_objective_dist":0.4,"na_objective": 0.7,\
                            "f_relay": 0.045, "r_pinhole": 75E-6, "detector_efficiency" : 0.7,\
                        "optics_efficiency": 0.8,"objective_efficiency":0.08, "detector_dead_time" : 22E-9, "encircled_energy_function_path" : None,"encircled_energy_function":None}
        self.laser_dict = {"excitation_beam_waist" : 1E-6, "m_squared" : 1, "excitation_rayleigh_length" : 20E-6, "wavelength": 650E-9, "power":0.005,"laser_beam_waist":1.55E-6}
        #dye_dict_path = os.path.join(self.main_folder,"RDP_simulator\Dye_data.csv")
        #self.dye_data = pd.read_csv(dye_dict_path, sep=";", index_col = "dye")
        self.dye_dict = {"dye": "manual","conc" : 60E-15, "epsilon" : 2.9E5,"lifetime" : 1E-9, "lifetime_isc": 1, "lifetime_triplet":1E-6,\
                         "quantum_efficiency":0.33, "dye_instance": dye.Dye(logger=self.logger)}
        #self.lineshape_convolution_dict = {'method': 'None', 'FWHM': np.array([1, 1]), 'nr_shapes': 1, 'file': os.path.join(self.main_folder, 'sample_laser_files', 'PN-12-003-SN0001_lineshapes.pck')}
        #self.piezo_tuning_dict = {'method': 'Linear', 'hysteresis_param': 0.0, 'wl_range': np.array([3255.0, 3365.0]), 'v_range': np.array(
        #    [0.0, 100.0]), 'nr_steps': 100, 'file': os.path.join(self.main_folder, 'sample_laser_files', 'PN-12-003-SN0001_tuning.pck')}
        #self.power_spectrum_dict = {'method': 'Box', 'wl_range': np.array([3200.0, 3450.0]), 'steepnes': 1, 'max_power': 1000, 'file': os.path.join(self.main_folder, 'sample_laser_files', 'PN-12-003-SN0001_tuning.pck')}
        self.noise_dict = {'method': 'poisson', 'std': 50000, 'mean': 150000,'detector_darknoise':1000 , 'noise_generator': np.ones, 'noise_pdf': scipy.stats.poisson(mu=10).pdf,'scale_noise' : False ,'file': ""}
        #self.drift_dict = {'drift_params': np.array([0.0, 100.0, 0.0, 0.0, 0.0, 0.0]), 'drift_enable': False, 'drift_time': -1}
        self.stream_dict = {'enable': False, 'mode': 'load', 'file': None, "streamer" : None}
        self.calc_dict = {'method':'local_baseline', 'stats' :"poisson",'threshold':0.9999, 'normalisation' : 60,'single_count' : False, 'nsigma':np.NaN}
        self.result_dict = {'threshold_counts' : 0}
        self.zmq_dict = {'enable': False, 'address':"tcp://127.0.0.1:5555"}
        
        self.nr_events_buffer= rb.Result_buffer(shape = 100, dtype = 'int')
        self.results_df = Results_df.results_df(self)
        self.signal_array = np.zeros(1,dtype = np.int) #Die Fluoreszenz aller Moleküle
        self.binned_signal_array = np.zeros(1,dtype = np.int)
        self.all_binned_signal_array = np.zeros((1,1),dtype = np.int)
        # sort dataframe. This is mainly done to assure a meaningful order of the Molecules in the DGA_simulator
        #sort_list = ['Methane', 'Ethane', 'Ethene', 'Ethyne', 'Propane', 'Butane', 'Water', 'Polystyrene']
        #sort_list = [element for element in sort_list if element in self.dye_data.index]
        #not_in_sort_list = [element for element in self.dye_data.index if element not in sort_list]
        #sort_list += sorted(not_in_sort_list)
        #self.dye_data = self.dye_data.reindex(sort_list)
        #self.dye_data.columns = ['spectrum']
        #self.dye_data.index.names = ['component']
        #self.dye_data['concentrations_1'] = np.zeros([len(self.dye_data.index)])
        #self.dye_data['concentrations_2'] = np.zeros([len(self.dye_data.index)])
        # global parameters
        #self.wavelength_range = np.asarray([3000, 3600], dtype=np.int32)
        #self.step_size = 1
        #self.cavity_length = 24  # cavity length
        #self.mode_number = 7.5
        #self.cell_length = 3
        #self.zmq_last_half_length = 0 # last received array length from zmq streamer
        #self.zmq_wrong_length_counter = 0 #counts the number of wrong receives
        #self.detector_inverting = False; #true if detector is inverting

        self.pickle_streamer = pickle_streamer(logger=self.logger)
        self.zmq_streamer = zmq_streamer(logger=self.logger,start_server = True)
        self.rdp_streamer = rdp_streamer(logger = self.logger)
        
        self.bin_idxs = None
        self.init_settings()
        self.SMC_calculator = None
        self.init_SMC_library()
        
        self.rpy2_loaded = False
        self.import_rpy2()
    
        
    def __del__(self):
        #to make sure that the zmq_server will be killed
        del self.zmq_streamer
        print("zmq_streamer deleted")
        del self.pickle_streamer
        del self.rdp_streamer
        print ("specsim deleted")
        if self.SMC_calculator is not None:
            del self.SMC_calculator
    
    def init_logger(self, logger):

        if logger is None:
            self.logger = logging.getLogger(__name__)
            self.logger.handlers = []  # clear all handlers
            self.logger.setLevel(logging.DEBUG)
            logging_handler = logging.StreamHandler()
            logging_handler.setLevel(logging.DEBUG)
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            logging_handler.setFormatter(formatter)
            self.logger.addHandler(logging_handler)
        else:
            self.logger = logger

    def init_settings(self):
        self.set_additional_parameters(time_resolution = 10, invoke_updater=False)
        self.calculate_number_of_steps()
        self.init_signal_df()
        self.get_objective_efficiency()
        self.get_noise_generator()
        self.get_calc_calculator()
        self.set_zmq_parameter(address='tcp://127.0.0.1:5556')
        self._solve_path()
        self.calc_path_parameters()
        self.calc_beams()
        self.vectorize_functions()
        self.calc_infos()
        self.calculate_noise()
        
        self.set_dye_parameters(dye_name = "Alexa Fluor 647")
        self.set_noise_parameters(method='poisson', detector_darknoise=1000, mean=200000, file_path=None)
        
    def init_SMC_library(self):
        import clr
        main_folder = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        dll_path = os.path.join(main_folder,"SMC_algorithm","SMCWrapper","SMCWrapper","SMCWrapper","bin","x64","Debug","SMCWrapper")
        SMC_default_values_path = os.path.join(main_folder,"SMC_algorithm","DefaultValues.xml")
        self.SMC_actual_values_path = os.path.join(os.path.dirname(SMC_default_values_path),"ActualValues.xml")
        try:
            shutil.copy2(SMC_default_values_path, self.SMC_actual_values_path)
        except Exception:
            self.logger.error("Not able to copy SMC default values",  exc_info=True)
            return
        try:
            clr.AddReference(dll_path)
            from Calculator import Calculator
            self.SMC_calculator = Calculator()
        except Exception:
            self.logger.error("Not able to load SMC calculator",  exc_info=True)
        self.adjust_SMC_values()
        
            


    def calculate_number_of_steps(self):
        self.additional_dict["nr_bins"] = int(self.scan_dict["scan_time"]/self.scan_dict["bin_length"])
        self.additional_dict["nr_steps"] = int(self.additional_dict["nr_bins"] * self.additional_dict["time_resolution"])

    def init_signal_df(self):
        'init of the spectrum data frame'
        indexes = ["signal","noise","sum"]
        self.signal_df = pd.DataFrame(columns=indexes,index = np.arange(self.additional_dict["nr_bins"]))
        self.signal_df.index.name = "bin"
        self.signal_df['signal'] = np.zeros(self.additional_dict["nr_bins"])
        self.signal_df['noise'] = np.zeros(self.additional_dict["nr_bins"])
        self.signal_df['sum'] = np.zeros(self.additional_dict["nr_bins"])
        
    def _xy_speedy(self,t):
        ''' 
        Geschwindigkeitskomponente in y als Funktion von t,L,und xy_speed
        '''
        xy_speedy = (np.sign(np.sin(2*np.pi*t*self.scan_dict["xy_speed"]/(4*self.scan_dict["xy_scan_dist"])))+1)/2+(np.sign(np.sin(2*np.pi*t*self.scan_dict["xy_speed"]/(4*self.scan_dict["xy_scan_dist"])+np.pi/2))-1)/2
        return xy_speedy*self.scan_dict["xy_speed"]
    
    def _xy_speedx(self,t):
        ''' 
        Geschwindigkeitskomponente in x als Funktion von t,L,und xy_speed
        '''
        xy_speedx =  -(np.sign(np.cos(2*np.pi*t*self.scan_dict["xy_speed"]/(4*self.scan_dict["xy_scan_dist"])))+1)/2-(np.sign(np.cos(2*np.pi*t*self.scan_dict["xy_speed"]/(4*self.scan_dict["xy_scan_dist"])+np.pi/2))-1)/2
        return xy_speedx*self.scan_dict["xy_speed"]
    
    def dxdy(self,t,coord):
        '''
        Differentialgleichung für die Koordinaten
        '''
        x, y = coord
        dx = self._xy_speedx(t)+2*np.pi*self.scan_dict["objective_spin_radius"]*self.scan_dict["objective_rps"]*np.cos(2*np.pi*self.scan_dict["objective_rps"]*t)
        dy = self._xy_speedy(t)+2*np.pi*self.scan_dict["objective_spin_radius"]*self.scan_dict["objective_rps"]*np.sin(2*np.pi*self.scan_dict["objective_rps"]*t)
        return [dx,dy]

    def _solve_path(self):
        '''
        Löst die Diffgleichung für die Koordinaten
        '''
        if self.scan_dict["scan_time"] is None:
            self.scan_dict["scan_time"] = 4*self.scan_dict["xy_scan_dist"]/self.scan_dict["xy_speed"]

        max_step = 0.001 
        try:
            if self.scan_dict["bin_length"] < 0.001:
                max_step = self.scan_dict["bin_length"]/2
        except TypeError:
            pass

        self.sol = solve_ivp(lambda t,y:self.dxdy(t,y),[0,self.scan_dict["scan_time"]],[0,-self.scan_dict["objective_spin_radius"]],max_step=max_step,first_step=max_step/4)
    
    def calc_path_parameters(self):
        '''
        Berechnet parameter des pfads
        '''
        #pfad pro datenpunkt
        diff = np.diff(self.sol.y,axis=1)
        path = np.sqrt(np.sum(diff**2,axis=0))
        #total pfadlänge
        self.result_dict["total_path"] = np.sum(path) #umrechung von mm nach m
        #Geschwindigkeit bei jedem datenpunkt
        self.velocity = path/np.diff(self.sol.t) #umrechung von mm/s nach m/s
        #KDE der Geschwindigkeitsverteilung
        self.kde_velocity = KernelDensity(bandwidth=np.ptp(self.velocity)/20).fit(self.velocity[:,np.newaxis])
    
    def calc_beams(self):
        '''
        Calculates the beam as function of the set parameters
        '''
        f_objective = self.optics_dict["f_objective"]
        f_laser_collimation = self.optics_dict["f_laser_collimation"]
        f_laser_collimation_dist = self.optics_dict["f_laser_collimation_dist"]
        NA_objective = self.optics_dict["na_objective"]
        M_squared = self.laser_dict["m_squared"]
        wavelength = self.laser_dict["wavelength"]
        f_relay = self.optics_dict["f_relay"]
        laser_beam_waist = self.laser_dict["laser_beam_waist"]
        laser_to_objective_dist = self.optics_dict["laser_to_objective_dist"]
        n_COP = 1.55 #Brechungsindex der Wellplate
        n_sample = 1.4
        d_focus_offset = 100e-6
        d_plate = 300e-6 #Dicke des Wellplatte
        #Emissions strahl
        h = 0
        angle = np.arcsin(NA_objective/n_sample)
        gRay = op.GeometricRay(h, angle)
        #p_pinhole=op.FreeSpace(f_relay)*op.ThinLens(f_relay)*op.FreeSpace(laser_to_objective_dist)*op.ThinLens(f_objective)*op.FreeSpace(2*(f_objective+error))*op.ThinLens(f_objective)*op.FreeSpace(laser_to_objective_dist)*p_laser
        #solve for distance between COP and Objective for perfect collimation
        dist_ = sp.symbols('dist_',real=True)
        
        dist = sp.nsolve((op.ThinLens(f_objective)*op.FreeSpace(f_objective-d_plate-d_focus_offset+dist_)*op.FlatRefraction(n_COP,1)*op.FreeSpace(d_plate)*op.FlatRefraction(n_sample,n_COP)*op.FreeSpace(d_focus_offset)*gRay).angle,dist_,(0.001,-0.001),solver='bisect', verify=True)
        #print(dist)
        #dist=0
        #p_pinhole=op.FreeSpace(f_relay)*op.ThinLens(f_relay)*op.FreeSpace(laser_to_objective_dist)*op.ThinLens(f_objective)*op.FreeSpace(f_objective-d_plate-d_focus_offset)*op.FlatRefraction(n_plate,1)*op.FreeSpace(d_plate)*op.FlatRefraction(n_sample,n_plate)*op.FreeSpace(d_focus_offset+error)*gRay
        #Emissionsstrahl beim Pinhole
        self.p_pinhole=op.FreeSpace(f_relay)*op.ThinLens(f_relay)*op.FreeSpace(laser_to_objective_dist)*op.ThinLens(f_objective)*\
                        op.FreeSpace(f_objective-d_plate-d_focus_offset+dist)*op.FlatRefraction(n_COP,1)*op.FreeSpace(d_plate)*\
                        op.FlatRefraction(n_sample,n_COP)*op.FreeSpace(d_focus_offset+signal_simulator.error)*gRay
        
        #Laser aus der Faser               
        self.p_laser = op.BeamParameter(wavelength*M_squared, 0, w=laser_beam_waist)
        #Kollimierter Strahl
        self.P_collimated = op.ThinLens(f_laser_collimation)*op.FreeSpace(f_laser_collimation_dist)*self.p_laser
        #Lasersrahl in der Probe
        self.p_sample = op.FreeSpace(d_focus_offset+signal_simulator.error)*op.FlatRefraction(n_COP,n_sample)*op.FreeSpace(d_plate)*op.FlatRefraction(1,n_COP)*\
                        op.FreeSpace(f_objective-d_plate-d_focus_offset+dist)*op.ThinLens(f_objective)*op.FreeSpace(laser_to_objective_dist)*self.P_collimated
        #self.p_sample = op.FreeSpace(f_objective-dist-signal_simulator.error)*op.ThinLens(f_objective)*op.FreeSpace(laser_to_objective_dist)*self.P_collimated
        
        
        self.laser_dict["excitation_beam_waist"] = float(self.p_sample.w_0.subs(signal_simulator.error,0).n())
        self.laser_dict["excitation_rayleigh_length"] = float(self.p_sample.z_r.subs(signal_simulator.error,0).n())#das muss ein numerischer Fehler sein, dass ich error auf 0 setzen muss
        self.laser_dict["laser_divergence"] = float(self.p_laser.divergence.subs(signal_simulator.error,0).n())
        self.laser_dict["laser_rayleigh_length"] = float(self.p_laser.z_r.subs(signal_simulator.error,0).n())
        self.laser_dict["collimated_waist_position"] = -float(self.P_collimated.z.subs(signal_simulator.error,0).n())
        self.laser_dict["collimated_waist"] = float(self.P_collimated.w_0.subs(signal_simulator.error,0).n())
        self.laser_dict["excitation_divergence"] = float(self.p_sample.divergence.subs(signal_simulator.error,0).n())
        self.laser_dict["waist_position"] = float(self.p_sample.z.subs(signal_simulator.error,0).n())
        self.laser_dict["emission_focus_shift"] = float(dist)
       
    def get_info_string(self):
        string = f'''
        Laser Properties:
        ---------------------
        Laser Waist [um]: {self.laser_dict['laser_beam_waist']*1E6:.2f}
        Laser Divergence [Degree]: {np.rad2deg(self.laser_dict['laser_divergence']):.2f}
        Laser Rayleigh length [um]: {self.laser_dict['laser_rayleigh_length']*1E6:.2f}
        Collimated waist position [mm] : {self.laser_dict['collimated_waist_position']*1E3:.0f}
        Collimated waist [um] : {self.laser_dict['collimated_waist']*1E6:.2f}
         
        Beam properties in sample
        -------------------------
        Waist [um]: {self.laser_dict['excitation_beam_waist']*1E6:.2f}
        Waist position error [um]: {self.laser_dict['waist_position']*1E6:.2f}
        Divergence [Degree]: {np.rad2deg(self.laser_dict['excitation_divergence']):.2f}
        Rayleigh length [um]: {self.laser_dict['excitation_rayleigh_length']*1E6:.2f}
        Emission focus shift [um]: {self.laser_dict["emission_focus_shift"]*1E6:.2f}
         
        Scan related parameters
        ------------------------
        max. counts/bin : {self.result_dict["max_counts/bin"]:.1f}
        z_max [um] : {self.result_dict["z_max"]*1E6:.2}
        r_max [um] : {self.result_dict["r_max"]*1E6:.2}
        geometric_interogation_volume [um^3] : {self.result_dict["geometric_interogation_volume"]*1E18:.2f}
        total_volume [mm^3] : {self.result_dict["total_volume"]*1E9:.2f}
        nr_molecules_mean [] : {self.result_dict["nr_molecules_mean"]}
        nr_observable_molecules [] : {self.result_dict["nr_observable_molecules"]}
        '''
        return string

        
    def get_objective_efficiency(self):
        '''
        Calculates the objective collecting efficiency based on the NA saved in self.optics_dict["na_objective"]
        and stores the results in self.optics_dict["objective_efficiency"]
        '''
        NA = self.optics_dict["na_objective"]
        efficiency = self.calc_objective_efficiency(NA)
        self.optics_dict["objective_efficiency"] = efficiency
    
    @staticmethod
    def calc_objective_efficiency(NA):
        '''
        Calculates the objective collecting efficiency based on the given NA.
        :param NA: NA
        :type NA: float
        '''
        n_sample = 1.4
        theta = np.arcsin(NA/n_sample)
        efficiency = -0.5*(np.cos(theta)-1)
        return efficiency
        
        
    
        
        
    def calc_infos(self):
        '''
        Berechnet diverse Laser und Pfad Parameter
        '''
        #solve for z_max where signal drops to 1%
        signal_max = self.detection_rate_lambdified(0,0)
        if signal_max == 0:
            signal_max = 1E-5 #save us from divide by 0 errors if detection rate equals zero

        #self.result_dict["z_max"] = float(abs(sp.nsolve(self.detection_rate.subs(self.r,0)/signal_max-0.01,self.z ,(0.001,0),solver='bisect', verify=False,prec=5)))
        self.result_dict["z_max"] = abs(scipy.optimize.fsolve(lambda z: self.detection_rate_lambdified(z,0)/signal_max-0.01,1E-5)[0])
        #print(f"z_max: {self.result_dict['z_max']}")
        #solve for x_max where intensity drops to 1%
        signal_max_at_z = self.detection_rate_lambdified(self.result_dict["z_max"],0)
        #print(f"signal_max_at_z: {signal_max_at_z}")
        if signal_max_at_z*self.scan_dict["bin_length"] < 0.01:
            self.result_dict["r_max"] =  self.laser_dict["excitation_beam_waist"]*4
        else:
            #this gives us a to a large volume. Use upper formula instead
            #self.result_dict["r_max"] = float(abs(sp.nsolve(self.detection_rate.subs(self.z,self.result_dict["z_max"])/signal_max_at_z-0.1,self.r ,(0.001,0),solver='bisect', verify=False,prec=5)))
            #self.result_dict["r_max"] = abs(scipy.optimize.fsolve(lambda r: self.detection_rate_lambdified(self.result_dict["z_max"],r)/signal_max_at_z-0.01,1e-6)[0])
            self.result_dict["r_max"] = abs(scipy.optimize.fsolve(lambda r: self.detection_rate_lambdified(self.result_dict["z_max"]*self.scan_dict["bin_length"],r)-0.01,1e-6)[0])
        #print(f"r_max: {self.result_dict['r_max']}")
        #interrogation volume
        #print(f"volume: {volume}")
        #Interogation volume als integral des hyperboloids der höhe 2*z_max
        #gemäss der Formel 24 in https://mathworld.wolfram.com/One-SheetedHyperboloid.html
        a = self.p_sample.w_0.subs(signal_simulator.error,0).n() #numerischer fehler, dass ich error auf 0 setzten muss?
        r = self.p_laser_w(self.result_dict["z_max"])
        self.result_dict["geometric_interogation_volume"] = 1/3*np.pi*2*self.result_dict["z_max"]*(2*a**2+r**2)
        beam_surface = 4*scipy.integrate.quad(self.p_laser_w,-self.result_dict["z_max"],0,epsrel =0.1,limit=10)[0]
        self.result_dict["total_volume"]=beam_surface*self.result_dict["total_path"]
        self.result_dict["max_counts/bin"]=self.detection_rate_lambdified(0,0)*self.scan_dict["bin_length"]
        #Beobachtbare Moelküle
        if self.dye_dict["conc"] is not None:
            #mean n molecules in interrogation volume
            self.result_dict["nr_molecules_mean"] =  self.result_dict["geometric_interogation_volume"]*self.dye_dict["conc"]*6.022E23*1000 #umrechung auf #/m^3
            #mean n molecules in total sanned volume
            #self.total_volume = (self.result_dict["total_path"]*2*self.laser_dict["excitation_beam_waist"]+(self.laser_dict["excitation_beam_waist"]**2)*3.141)*2*self.laser_dict["excitation_rayleigh_length"]
            self.result_dict["nr_observable_molecules"] = int(self.result_dict["total_volume"]*self.dye_dict["conc"]*6.022E23*1000)
    
        
    @staticmethod
    def intensity(P0,p,z,r):
        '''
        Intensity of a TEM00 Gaussian beam as a fuction of beam parameter and position within the beam
        Parameters:
        ----------
        P0: total Power in the Beam in W
        p:  Sympy.physics.optics.BeamParameter instance
        z: axial postion relative to waist in meters
        r: lateral position to waist in meters
        '''
        return 2*P0/(np.pi*p.w_0**2)*((p.w_0/p.w)**2*sp.exp(-2*r**2/p.w**2)).subs(signal_simulator.error,z)

    @staticmethod
    def Pthrugh(p,radius):
        '''
        Partial power of a beam, which passes a pinhole. 
        If p is a Gaussian beam (Sympy.physics.optics.BeamParameter instance), a gaussian intensity distribution is assumed
        If p is a geometric beam (Sympy.physics.optics.GeometricRay instance), a uniform distribution is assumed
        
        Parameters
        --------
        :param p: Beam parameter at the pinhole
        :param radius: Radius of the pinhol ein m

        '''
        if isinstance(p, op.BeamParameter):
            pthrough =  (1 - sp.exp(-2*radius**2/p.w**2))
        elif isinstance(p, op.GeometricRay):
            pthrough =  sp.Piecewise((1,radius>sp.Abs(p.height)),(radius**2/p.height**2,True)) 
        return pthrough
        
    @staticmethod
    def mad(x):
        '''
        calculates the median absulute deviation of an array
        Parameters
        -------
        @param x: numpy array
        '''
        return np.fabs(x - x.mean()).mean()
    

    
    def vectorize_functions(self):
        #self.calc_emission_rate_vectorized = np.vectorize(self.calc_emission_rate,excluded='self')
        if self.optics_dict["encircled_energy_function"] is not None:
            eef = self.optics_dict["encircled_energy_function"]
            self.detection_rate = (1*self.dye_dict["dye_instance"].calc_emission_rate(self.intensity(self.laser_dict["power"],self.p_sample,signal_simulator.z,signal_simulator.r),self.laser_dict["wavelength"]*1E9)).subs(signal_simulator.error,signal_simulator.z)
            detection_rate_lambdified_ = sp.lambdify((signal_simulator.z,signal_simulator.r),self.detection_rate,"numpy")   
            detection_rate_lambdified =  lambda z,r: self.optics_dict["optics_efficiency"]*\
                                    self.optics_dict["objective_efficiency"]*self.optics_dict["detector_efficiency"]*\
                                    (detection_rate_lambdified_(z,r)*eef(self.optics_dict["r_pinhole"],z).squeeze())
            self.detection_rate_lambdified = detection_rate_lambdified#np.vectorize(detection_rate_lambdified)  
        else:   
            self.detection_rate = self.optics_dict["optics_efficiency"]*\
                                             self.optics_dict["objective_efficiency"]*self.optics_dict["detector_efficiency"]*\
                                             (self.Pthrugh(self.p_pinhole,self.optics_dict["r_pinhole"])*\
                                              self.dye_dict["dye_instance"].calc_emission_rate(self.intensity(self.laser_dict["power"],self.p_sample,signal_simulator.z,signal_simulator.r),self.laser_dict["wavelength"]*1E9)).subs(signal_simulator.error,signal_simulator.z)
            self.detection_rate = self.detection_rate#*(1-self.additional_dict["detector_dead_time"]*self.detection_rate)
            self.detection_rate_lambdified = sp.lambdify((signal_simulator.z,signal_simulator.r),self.detection_rate,"numpy") 
        #self.detection_rate = self.detection_rate#*(1-self.additional_dict["detector_dead_time"]*self.detection_rate)
        self.p_laser_w = sp.lambdify((signal_simulator.error),self.p_laser.w,"numpy")
        
            

    
    def sample_distribution(self):
        '''
        This function generates a sampled distribution of the signal
        
        It samples a random distribution of molecule positions according to the actual concentration
        and actual interogation volume. 
        The interogation volume is determined from the beam parameters and is defined als the space where a molecules 
        emits at least 1% of Signal relative to the center of the beam. 
        The signal strength is calculated by taking into acount the Fluorophor parameters, the spacial distribution of 
        the excitation intensity and the light capturing efficiency of the optical system. 

        '''
        z_max = self.result_dict["z_max"]
        #print(f"z_max: {z_max}")
        r_max = self.result_dict["r_max"]
        #total scanned volume
        volume = 2*z_max*2*r_max*self.result_dict["total_path"]+2*z_max*r_max**2
        #print(f"volume: {volume}")
        #mean number of molecules in the total scanned volume 
        n_molecules_mean =  volume*self.dye_dict["conc"]*6.022E23*1000 #umrechung auf #/m^3
        #print(f"n_molecules_mean: {n_molecules_mean}")
        #sampled number of molecues
        n_molecules = st.poisson.rvs(n_molecules_mean)
        #gesampelte Geschwindigkeiten, unmögliche Werte werden geclipt
        sample_velocity = self.kde_velocity.sample(n_molecules)[:,0]
        sample_velocity = np.clip(sample_velocity,0,self.scan_dict["xy_speed"]+self.scan_dict["objective_spin_radius"]*2*self.scan_dict["objective_rps"]*np.pi)
        #number of bins to simulate during one molecule transit
        n_bins = int(2*r_max/(np.min(self.velocity)*self.scan_dict["bin_length"]))
        #print(f"nbins:{n_bins}")
        #total number of bins to simulate
        n_bins_total = int(self.scan_dict["scan_time"] / self.scan_dict["bin_length"])
        #number of simulation steps per bin
        steps_per_bin = int(self.scan_dict["bin_length"]/self.additional_dict["time_resolution"])
        #number of simulation steps during one molecule transit
        time_steps = int(n_bins*steps_per_bin) #Anzahldatenpunkte für jedes Molekül
        #total number of simulation steps
        time_steps_total = int(n_bins_total*steps_per_bin)
        #self.signal_array enthält den Verlauf der Fluoreszenz für jeden transit eines Moleküls
        #Das array ist ein bin länger as nötig, damit das Signal über die Anschnittphase eine bins randomisiert
        #werden kann
        
        self.signal_array = np.zeros(time_steps_total,dtype = np.int) #Die Fluoreszenz aller Moleküle
        self.binned_signal_array = np.zeros(n_bins_total, dtype = np.int) #Die Fluoreszenz aller Moleküle gebinnt auf die binlänge
        self.all_signal_array = np.zeros((n_molecules,time_steps),dtype = np.int) #Die Fluoreszenz jedes Moleküls separiert
        self.all_binned_signal_array = np.zeros((n_molecules,n_bins),dtype = np.int) #Die Fluoreszenz jedes Moleküls separiert gebinnt
        convolution_kernel = np.ones(steps_per_bin) #Kernel zum berechnen des gebinnten Signals über Kovolution
        if n_molecules == 0:
            return
        #Coordinates of the sampled molecues
        #x: perpendiculare to scan direction
        #y: scan direction
        #z: along laser beam
        z_coordinates = -z_max+2*z_max*np.random.random_sample(n_molecules)
        y_min_coordinate = self.velocity.min()*self.additional_dict["time_resolution"]*time_steps
        y_coordinates = y_min_coordinate+(self.result_dict["total_path"]-y_min_coordinate)*np.random.random_sample(n_molecules)
        x_coordinates = -r_max+2*r_max*np.random.random_sample(n_molecules)
        
        speed = (self.velocity.mean()*self.additional_dict["time_resolution"]) 
        #index of the first step 
        start_steps = (y_coordinates /speed - time_steps/2).astype(np.int)
        #molecules y coordinates relative to laser beam
        y_coordinates_relatives = r_max-np.outer(sample_velocity*self.additional_dict["time_resolution"],np.arange(time_steps))
        #shift of interaction maximum due to velocity distribution
        shifts = (time_steps/2-r_max/(sample_velocity*self.additional_dict["time_resolution"])).astype(np.int)
        shifts = shifts//steps_per_bin*steps_per_bin #nur um ganze bins schieben, damit das Phasenjitter erhalten bleibt
        for i in range(n_molecules):
        #for i in tqdm(range(n_samples)):
            
            start_step = start_steps[i]
            #shift= -int((sample_velocity[i]-self.velocity.mean())*n_bins*steps_per_bin)
            #molecules y coordinates relative to laser beam
            y_coordinates_relative = y_coordinates_relatives[i]
            
            shift = shifts[i]
            #spherical coordinates relative to laser beam
            r_coordinates = np.sqrt(x_coordinates[i]**2 + y_coordinates_relative**2)
            #calculate signal strength for each position
            self.all_signal_array[i,:] = (self.additional_dict["time_resolution"]*self.detection_rate_lambdified(z_coordinates[i],r_coordinates)).astype(np.int)
            #shift signal to center all interaction
            self.all_signal_array[i] = np.roll(self.all_signal_array[i],shift)
            #crop signals at the end of the simulation
            signal_croped = self.all_signal_array[i,:(time_steps_total-start_step)] #sicherstellen dass nicht über das array hinaus geschrieben wird
            #add signal to self.signal_array
            self.signal_array[start_step:start_step+time_steps] += signal_croped
            #bin signal to bin_length
            #reshape geht schneller als convolve
            #self.all_binned_signal_array[i,:] = np.convolve(self.all_signal_array[i,:],convolution_kernel,mode="valid")[::steps_per_bin]
        
        #correct signal for dead time of detector
        #This is only correct when the noise is not dominating the signal
        #because in the way this is implemented, the noise is not taken into account for the bin time correction. 
        self.calculate_corrected_count_rate(self.all_signal_array,self.additional_dict["time_resolution"])
        self.calculate_corrected_count_rate(self.signal_array,self.additional_dict["time_resolution"])  
        #bin total signal to bin_length
        #reshape-sum-reshape method ist (a little bit) faster than convolution
        #self.binned_signal_array = np.convolve(self.signal_array,convolution_kernel,mode="valid")[::steps_per_bin]
        self.all_binned_signal_array = np.sum(self.all_signal_array.reshape(n_molecules,-1,steps_per_bin),axis=2).reshape(n_molecules,-1)
        self.binned_signal_array = np.sum(self.signal_array.reshape(-1,steps_per_bin),axis=1).reshape(-1)
   
    
    def generate_noise_from_file(self):
        '''
        generates a kernel density enstimator for the noise and returns the resample function
        '''
        path = self.noise_dict['file']
        basename = os.path.basename(path)
        ext = basename.split('.')[-1]
        # hf.pydevBrk()
        if ext == 'pck':
            # load data from a saved pickle lineshape file

            # data structure in the pickle file:
            df1 = pd.DataFrame()
            # load data. old data has only one dataframe and therefore will exit while loop through except statement
            idx = 0
            with open(path, 'rb') as handle:
                data[idx] = pickle.load(handle)
                
            noise_ch1_array = data[0].values.T
            std = np.std(noise_ch1_array.ravel())

            parameter_array = list(data[0].keys())
            if parameter_array[0] == 'noise':

                kde =  KernelDensity(bandwidth=1).fit(noise_ch1_array)

            self.noise_dict['std'] = variance

            generator = kde.sample
            pdf = kde


        return generator, pdf

    def get_noise_generator(self):
        '''
        saves the noise generator in self.noise_dict()
        The parameters in self.noise_dict() are used to init the generator. 
        If noise_dict['scale_noise']==True, the noise is scaled with laser power, pinhole diameter and objective NA, where the nomal values are 5mW laser power and 75um pinhole radius 
        and an NA of 0.7. Nominal values are choosen such to represent RDP standard configuration. 
        The noise generator is a function with one argument. It generates the
        number of samples given in the argument based on the underlying probability density function and returns a numpy array
        '''
        method = self.noise_dict['method']
        noise_std = self.noise_dict['std']
        noise_mean = self.noise_dict['mean']
        detector_darknoise = self.noise_dict['detector_darknoise']
        bin_length = self.scan_dict["bin_length"]
        scale_noise = self.noise_dict["scale_noise"]

        if noise_std == 0:
            def generator(nr_bins, *args):
                return np.zeros(nr_bins)

        elif method == 'normal':
            loc=noise_mean*bin_length
            scale=noise_std*bin_length
            if scale_noise:
                loc *=(self.optics_dict["r_pinhole"]/(75E-6))**2*self.laser_dict["power"]/(5E-3)*self.optics_dict['objective_efficiency']/self.calc_objective_efficiency(0.7)
                scale *= (self.optics_dict["r_pinhole"]/(75E-6))**2*self.laser_dict["power"]/(5E-3)*self.optics_dict['objective_efficiency']/self.calc_objective_efficiency(0.7)
            dist = scipy.stats.norm(loc=loc,scale=scale )
            generator= dist.rvs
            pdf=dist.pdf

        elif method =="poisson":
            mu=noise_mean*bin_length
            if scale_noise:
                mu*=(self.optics_dict["r_pinhole"]/(75E-6))**2*self.laser_dict["power"]/(5E-3)*self.optics_dict['objective_efficiency']/self.calc_objective_efficiency(0.7)
    
            dist = scipy.stats.poisson(mu=mu)
            generator= dist.rvs
            pdf=dist.pdf

        elif method == 'file':
            generator, pdf = self.generate_noise_from_file()
            
        else:
            self.logger.error("Wrong noise method: " +method)


        if detector_darknoise == 0:
            def detector_generator(nr_bins, *args):
                return np.zeros(nr_bins)
        else:
            detector_generator = scipy.stats.poisson(mu = detector_darknoise*self.scan_dict["bin_length"]).rvs

        self.noise_dict['noise_generator'] = generator
        self.noise_dict['detector_noise_generator'] = detector_generator
        self.noise_dict['noise_pdf'] = pdf

    def calculate_noise(self):
        '''
        calculates noise on the measurement channels using the parameters in self.noise_dict
        '''

        # if method=='Laser':
        # hf.pydevBrk()
        
        noise = self.noise_dict['noise_generator'](self.additional_dict["nr_bins"])
        # laser_noise=np.ones(self.number_of_steps)

        detector_noise = self.noise_dict['detector_noise_generator'](self.additional_dict["nr_bins"])
        
        self.signal_df['noise'] = noise+detector_noise
    
    def calculate_corrected_count_rate(self, data, bin_length):
        '''
        This function performs an in-place correction of the signal for the dead time of the detector
        The correction factor is the inverse of the factor provided in the datasheed of the Excelitas detector:
        n_photon_real = n_photon_measured/(1-dead_time*n_photon_measured)
        
        The inverse gives:
        n_photon_measured = n_photon_real/(1+dead_time*n_photon_real)
        
        :param data:        Numpy array of the data to be corrected
        :param bin_length:  Bin length of the data
        '''
        
        
        scaled_dead_time = self.optics_dict["detector_dead_time"]/bin_length
        #apply dead time correction for high count rates only (because its expansive
        if scaled_dead_time>0:
            if data.max()>(1/(10*scaled_dead_time)):
                #rate = np.clip(rate*(1-scaled_dead_time*rate),0,None)
                data[...] = data/(1+scaled_dead_time*data) 

        

    def calculate_events(self):
        '''
        calculates the number of events in the data based in the parameters in the calc_params dict
        '''
        stats = self.calc_dict["stats"]
        threshold = self.calc_dict["threshold"]
        normalisation = self.calc_dict["normalisation"]
        calculator = self.calc_dict["calculator"]
        single_count = self.calc_dict["single_count"]
        
        if threshold < 1:
            ''' update nsigma based in a robust estimate'''
            nsigma = self.calc_nsigma(self.signal_df['sum'],threshold)
            self.calc_dict["nsigma"] = nsigma
            if self.calc_dict['method'] == 'smc':
                self.adjust_SMC_values('nsigma')
         
        
        if calculator is not None:
            nr_events, threshold_counts = calculator(self.signal_df['sum'], threshold, single_count)
            if self.calc_dict["normalisation"]>0:
                nr_events *= self.calc_dict["normalisation"] / self.scan_dict["scan_time"]
            self.result_dict["threshold_counts"] = threshold_counts
            self.nr_events_buffer.append(nr_events)
        
    def calculate_events_poisson(self, data, threshold, single_count = False):
        '''
        calculates the number of events under the assumption of poisson distributed noise
        It takes the median of the data instead of the mean to estimate the distribution parameters. 
        This breaks down for vey low noise levels. 
        Parameter:
        -------
        @params data: data set of counts. 
        @params threshold: threshold. If <1 it is enterpreted at quantile, else as threshold above mean value in units of standard deviation of the data
        '''
        _lambda = np.mean(data)
        background_dist = scipy.stats.poisson(_lambda)
        if threshold < 1:
            threshold_counts = background_dist.ppf(threshold)
        else:
            
            threshold_counts = _lambda+threshold*np.sqrt(_lambda)
        above_threshold = 1*(data>threshold_counts)
        if single_count:
            above_threshold = np.diff(above_threshold,append=[0])
        nr_events= np.sum(above_threshold == 1)
        return nr_events, threshold_counts
    
    def calculate_events_poisson_robust(self, data, threshold, single_count = False):
        '''
        calculates the number of events under the assumption of poisson distributed noise
        It uses the R function glmrob  to robustly estimate the distribution parameters. 
        This does not break down for vey low noise levels. 
        Parameter:
        -------
        @params data: data set of counts. 
        @params threshold: threshold. If <1 it is enterpreted at quantile, else as threshold above mean value in units of standard deviation of the data
        '''
        if not self.rpy2_loaded:
            self.logger.error("rpy2 not loaded. Cannot calculate events based on robust statistics")
            return 0, 0
        df = pd.DataFrame({"y": data})
        with localconverter(robjects.default_converter + pandas2ri.converter):
            r_data = robjects.conversion.py2ri(df)
        fit = r_robustbase.glmrob("y~1",data=r_data,family="poisson",method= "Mqle")
        _lambda = np.exp(r_stats.coef(fit)[0])

        background_dist = scipy.stats.poisson(_lambda)
        if threshold < 1:
            threshold_counts = background_dist.ppf(threshold)
        else:
            threshold_counts = _lambda+threshold*np.sqrt(_lambda)
        above_threshold = 1*(data>threshold_counts)
        if single_count:
            above_threshold = np.diff(above_threshold,append=[0])
        nr_events= np.sum(above_threshold == 1)
        return nr_events, threshold_counts
    
    def calculate_events_normal(self, data, threshold, single_count = False):
        '''
        calculates the number of events under the assumption of normal distributed noise
        Parameter:
        -------
        @params data: data set of counts. 
        @params threshold: threshold. If <1 it is enterpreted at quantile, else as threshold above mean value in units of standard deviation of the data
        '''
        mean = np.mean(data)
        sigma = np.std(data)
        background_dist = scipy.stats.norm(mean,sigma)
        if threshold < 1:
            threshold_counts = background_dist.ppf(threshold)
        else:
            threshold_counts = mean+threshold*np.sqrt(sigma)
        above_threshold = 1*(data>threshold_counts)
        if single_count:
            above_threshold = np.diff(above_threshold.values,append=[0])
        nr_events= np.sum(above_threshold == 1)
        return nr_events, threshold_counts
    
    def calculate_events_normal_robust(self, data, threshold, single_count = False):
        '''
        calculates the number of events under the assumption of normal distributed noise
        It uses the R function lmrob  to robustly estimate the distribution parameters. 
        Parameter:
        -------
        @params data: data set of counts. 
        @params threshold: threshold. If <1 it is enterpreted at quantile, else as threshold above mean value in units of standard deviation of the data
        '''
        if not self.rpy2_loaded:
            self.logger.error("crpy2 not loaded. Cannot calculate events based on robust statistics")
            return 0, 0
        df = pd.DataFrame({"y": data})
        with localconverter(robjects.default_converter + pandas2ri.converter):
            r_data = robjects.conversion.py2ri(df)
        fit = r_robustbase.lmrob("y~1",data=r_data,method = 'SM')
        mean = r_stats.coef(fit)[0]
        sigma = r_stats.sigma(fit)
        background_dist = scipy.stats.norm(mean,sigma)
        if threshold < 1:
            threshold_counts = background_dist.ppf(threshold)
        else:
            threshold_counts = mean+threshold*np.sqrt(sigma)
        above_threshold = 1*(data>threshold_counts)
        if single_count:
            above_threshold = np.diff(above_threshold.values,append=[0])
        nr_events= np.sum(above_threshold == 1)
        return nr_events, threshold_counts
    
    def calculate_events_smc(self,data,threshold, single_count = False):
        '''
        calculates the number of events with the SMC algorithm
        Parameter:
        -------
        @params data: data set of counts. 
        @params threshold: threshold. If <1 it is enterpreted at quantile, else as threshold above mean value in units of standard deviation of the data
        @params args: any other parameters. Are only defined for compatibility reasons
        '''
        result_dummy = int(1)
        threshold_dummy = int(1)
        
        nr_events, threshold_counts = self.SMC_calculator.addPhotonData(data.values, len(data),result_dummy,threshold_dummy)[1:3]
        #revert normalisation to 60s 
        nr_events /=(60/self.scan_dict['scan_time'])
        #nr_events /=(600000/len(data))
        #nr_events /= (60/self.scan_dict["scan_time"])
        return nr_events, threshold_counts

    def calc_nsigma(self,data, threshold_quantile):
        '''
        Calculates nsigma for the SMC algorith from the threshold_quantile and the data based on a robust estimate of lambda
        :param data: data set of counts
        :type data: np.ndarray
        :param threshold_quantile: quantile
        :type threshold_quantile: float
        '''
        if  self.rpy2_loaded:
            df = pd.DataFrame({"y": data})
            with localconverter(robjects.default_converter + pandas2ri.converter):
                r_data = robjects.conversion.py2ri(df)
            fit = r_robustbase.glmrob("y~1",data=r_data,family="poisson",method= "Mqle")
            _lambda = np.exp(r_stats.coef(fit)[0])
        else:
            #logging info removed because it pops up in every simulation step
            #self.logger.info("crpy2 not loaded. Cannot calculate threshold based on robust statistics")
            _lambda = np.median(data)

        background_dist = scipy.stats.poisson(_lambda)
        threshold_counts = background_dist.ppf(threshold_quantile)
        nsigma =  (threshold_counts-_lambda)/np.sqrt(_lambda)
        return nsigma
        
        

    def take_measurement(self):
        '''
        
        generates new noise and takes a new measurement
        :param averaging: numbers of measurements to average
        :type averaging: int
        '''


        if self.stream_dict['enable'] and self.stream_dict['mode'] == 'load':
            self.load_from_stream()
        elif self.zmq_dict['enable']:
            self.load_from_zmq()
        else:
            self.calculate_noise()
            self.sample_distribution()
            self.signal_df["signal"] = self.binned_signal_array
            self.signal_df["sum"] = self.signal_df["signal"] + self.signal_df["noise"]
            #clip to zero signal
            self.signal_df["sum"] = np.clip(self.signal_df["sum"],0,None)
            

        if self.stream_dict['enable'] and self.stream_dict['mode'] == 'dump':
            self.dump_to_file()
            
        self.calculate_events()      
        
    def load_eef(self, path): 
        '''
        loads an encircled energy function for the energy distribution in the confocal stop.
        The data to load has the following format
        1. row: axial position of the emission in object space relative to focal plane in mm. First element is always zero.
        1. Column: radius of circle in um. First element is always zero (see 1. row)
        from 2. row on:
        2 - n Column: encircled energy function
        :param path: path to the data file
        :type path: str or Path object
        '''
        #path = r"C:\Daten\Python\RDP_simulator\psf\Geo_enc_en_data_NA055_642_19-Jan-2021.txt"
        ee_data = np.loadtxt(path,delimiter=",")
        z = ee_data[0,1:]/1000 #umrechnung auf m
        x = ee_data[1:,0]/1E6/2 #umrechnung auf m und radius
        ee = ee_data[1:,1:]
        
        #only single sided function is needed as radius is an absolute quantity
        single_sided_x_ee = x[x>=0]
        single_sided_ee = ee[x>=0,]
        
        
        #interpolate function
        interpolated_function=scipy.interpolate.RectBivariateSpline(single_sided_x_ee,z,single_sided_ee)
        def ee_function(new_x,new_z):
            '''
            Return 0 if new parameters are outside of the interpolation
            '''
            if np.abs(new_z) <= z.max():
                result =  interpolated_function(new_x,new_z)
            else:
                result = np.array(0.0)
            return result
            
        
        self.optics_dict['encircled_energy_function'] = ee_function
        self.optics_dict['encircled_energy_function_path'] = path
        
        


    def save_parameters(self, path):
        '''
        Saves all the parameters in a csv file and in a pickle file
        :param path: path to the csv file
        :type path: str
        '''
        import csv
        with open(path, 'w',encoding='utf-8') as f:
            w = csv.writer(f)
            dict_list = [self.lineshape_convolution_dict, self.piezo_tuning_dict, self.power_spectrum_dict, self.noise_dict, self.drift_dict,self.stream_dict]
            dict_name_list = ['lineshape_convolution_dict', 'piezo_tuning_dict', 'power_spectrum_dict', 'noise_dict', 'drift_dict', 'stream_dict',"zmq_dict"]
            for _dict, name in zip(dict_list, dict_name_list):
                w.writerow([name])
                w.writerows(list(_dict.items()))
                w.writerow([])
            w.writerow(['Additional Parameters'])
            w.writerow(['cavity_length:{:f}'.format(self.cavity_length)])
            w.writerow(['cell_length:{:f}'.format(self.cell_length)])
            w.writerow(['wavelength_range: '+np.array_str(self.wavelength_range)])
            w.writerow(['step_size:{:f}'.format(self.step_size)])
            w.writerow(['detector_inverting:{}'.format(self.detector_inverting)])
            w.writerow([])
            w.writerow(['simulation data'])
        with open(path, 'a',encoding='utf-8') as f:
            self.dye_data.to_csv(f,encoding='utf-8')
        '''
        #save parameters as pickle file
        basename=os.path.basename(path)
        dirname=os.path.dirname(path)
        #remove extension
        basename=basename.split('.')[0]
        #add '.pck' for pickle
        picklename=basename+'.pck'
        picklepath=os.path.join(dirname,picklename)
        self.save_parameters_pickle(picklepath)
        '''

    def save_parameters_pickle(self, path_or_handler=None):
        '''
        Saves all parameters as a pickle file.
        :param path_or_handler: path to the pickle file or a handler (file instance of an allready opened pickle file)
        :type path: string or file instance
        '''
        # save configuration in pickle file as dict of dicts
        all_dicts = {'lineshape_convolution_dict': self.lineshape_convolution_dict.copy(),
                     'piezo_tuning_dict': self.piezo_tuning_dict.copy(),
                     'power_spectrum_dict': self.power_spectrum_dict.copy(),
                     'noise_dict': self.noise_dict.copy(),  # make copy to be able to delete 'noise generator' and 'noise_pdf'
                     'drift_dict': self.drift_dict,
                     'stream_dict': self.stream_dict.copy(),
                     'zmq_dict': self.zmq_dict.copy()}
        # delete the instances in noise_dict (pickle cannot handle them)
        all_dicts['noise_dict'].pop('noise_generator', None)
        all_dicts['noise_dict'].pop('detector_noise_generator', None)
        all_dicts['noise_dict'].pop('noise_pdf', None)
        
        additional_parameters_dict = {'cavity_length': self.cavity_length,
                                      'cell_length': self.cell_length,
                                      'step_size': self.step_size,
                                      'wavelength_range': self.wavelength_range,
                                      'detector_inverting': self.detector_inverting,
                                      'dye_data': self.dye_data.copy()}
        all_dicts['additional_parameters_dict'] = additional_parameters_dict
        
        # replace paths with relative paths (for platform independentness)
        key_list = [['lineshape_convolution_dict','file'],
                     ['piezo_tuning_dict','file'],
                     ['power_spectrum_dict','file'],
                     ['noise_dict','file'],
                     ['stream_dict','file']]
        
        for key1,key2 in key_list: 
            if (all_dicts[key1][key2] is not None) and (all_dicts[key1][key2]!=''):
                all_dicts[key1][key2] = os.path.relpath(all_dicts[key1][key2],self.main_folder)
                #replace platform depending separator
                all_dicts[key1][key2]=all_dicts[key1][key2].replace(os.sep,'/')

        for i in all_dicts['additional_parameters_dict']['dye_data'].index:
            path = os.path.relpath(all_dicts['additional_parameters_dict']['dye_data'].at[i, 'spectrum'],self.main_folder)
            path = path.replace(os.sep,'/')
            all_dicts['additional_parameters_dict']['dye_data'].at[i, 'spectrum'] = path

        if not isinstance(path_or_handler, IOBase):
            with open(path_or_handler, 'wb') as handle:
                pickle.dump(all_dicts, handle, protocol=pickle.HIGHEST_PROTOCOL)

        else:
            pickle.dump(all_dicts, path_or_handler, protocol=pickle.HIGHEST_PROTOCOL)

    def load_parameters(self, path_or_handler):
        '''
        loads parameters from a pickle file of from a dict.
        :param path_or_handler: path or handler to a pickle file
        :type path_or_handler: str or handler
        '''
        if not isinstance(path_or_handler, IOBase):
            try:
                with open(path_or_handler, 'rb') as handle:
                    all_dicts = pickle.load(handle)
            except UnicodeDecodeError:
                #pickle file from python 2.7
                with io.open(path_or_handler, "rb") as handle:
                    u = pickle._Unpickler(handle)
                    u.encoding = 'latin-1'
                    all_dicts = u.load()
                    self.logger.warning('python 2.7 pickle file loaded')
            except IOError:
                self.logger.warning('not able to load save file')
                return
        else:
            try:
                position = path_or_handler.tell()
                all_dicts = pickle.load(path_or_handler)
            except UnicodeDecodeError:
                #pickle file from python 2.7
                path_or_handler.seek(position)
                u = pickle._Unpickler(path_or_handler)
                u.encoding = 'latin-1'
                all_dicts = u.load()
                self.logger.warning('python 2.7 pickle file loaded')
        #replace relative paths with absolute paths
        key_list = [['lineshape_convolution_dict','file'],
                     ['piezo_tuning_dict','file'],
                     ['power_spectrum_dict','file'],
                     ['noise_dict','file'],
                     ['stream_dict','file']]
        #regex to check for absolute path
        if os.name == 'nt':
            absolute_path_regex = re.compile(r"^.:")
        else:
            absolute_path_regex = re.compile(r"^/")
        
        for key1,key2 in key_list:
            if isinstance(all_dicts[key1][key2],str):
                if((all_dicts[key1][key2] !='')
                   and (not absolute_path_regex.match(all_dicts[key1][key2]))):
                    all_dicts[key1][key2] = os.path.join(self.main_folder,all_dicts[key1][key2].replace('/',os.sep))


        for i in all_dicts['additional_parameters_dict']['dye_data'].index:
            if ((not absolute_path_regex.match(all_dicts['additional_parameters_dict']['dye_data'].at[i, 'spectrum']))
            and (not all_dicts['additional_parameters_dict']['dye_data'].at[i, 'spectrum'] == '')):
                abspath=all_dicts['additional_parameters_dict']['dye_data'].at[i, 'spectrum'].replace('/',os.sep)
                abspath = os.path.join(self.main_folder,abspath)
                all_dicts['additional_parameters_dict']['dye_data'].at[i, 'spectrum'] = abspath

        #check if files are available
        #alternative methods if files a not available
        alternative_methods={'lineshape_convolution_dict':'Gaussian',
                             'piezo_tuning_dict':'Linear',
                             'power_spectrum_dict':'Box',
                             'noise_dict':'Normal'}
        for key1,key2 in key_list:
            if isinstance(all_dicts[key1][key2],str):
                if not os.path.isfile(all_dicts[key1][key2]):
                    if key1!='stream_dict':
                        if all_dicts[key1]["method"]=="Laser":
                            all_dicts[key1]["method"]=alternative_methods[key1]
                            if all_dicts[key1][key2]!="":
                                self.logger.warning('file is missing: "'+all_dicts[key1][key2])
                                all_dicts[key1][key2]=""
                    else:
                        if (all_dicts[key1]['enable'] == True) and (all_dicts[key1]['mode'] == 'load'):
                            all_dicts[key1]['enable'] = False
                            if all_dicts[key1][key2]!="":
                                self.logger.warning('file is missing: "'+all_dicts[key1][key2])
                                all_dicts[key1][key2]=""

        # copy all parameters
        try:
            self.lineshape_convolution_dict = all_dicts['lineshape_convolution_dict']
            self.piezo_tuning_dict = all_dicts['piezo_tuning_dict']
            self.power_spectrum_dict = all_dicts['power_spectrum_dict']
            self.noise_dict = all_dicts['noise_dict']
            if 'laser_noise_cov' not in self.noise_dict:
                self.noise_dict['laser_noise_cov' ] = np.array([[1.0, 0], [0, 1]], dtype=float)
            if 'detector_noise_cov' not in self.noise_dict:
                self.noise_dict['detector_noise_cov'] = np.array([[1.0, 0], [0, 1]], dtype=float)



            self.drift_dict = all_dicts['drift_dict']

            additional_parameters_dict = all_dicts['additional_parameters_dict']
            self.cavity_length = additional_parameters_dict['cavity_length']
            self.cell_length = additional_parameters_dict['cell_length']
            self.step_size = additional_parameters_dict['step_size']

        except KeyError as e:
            self.logger.warning('pickle file is invalid!. Key "' +e.message +'" is missing')

        try:
            self.wavelength_range = additional_parameters_dict['wavelength_range']
        except KeyError as e:
            self.set_wavelength_range(invoke_updater=False)

        try:
            # test for old configuration files
            self.stream_dict = all_dicts['stream_dict']
        except KeyError:
            self.stream_dict = {'enable': False, 'mode': 'dump', 'file': None}

        try:
            # test for old configuration
            self.zmq_dict = all_dicts['zmq_dict']
        except KeyError:
            self.zmq_dict = {'enable': False,'address':'tcp://127.0.0.1:5556'}
            
        try:
            # test for old configuration
            self.detector_inverting = additional_parameters_dict['detector_inverting']
        except KeyError:
            self.detector_inverting = False

        # init spectrum simulator with new parameters
        self.updater('all_parameters')
        #Qneed to update twice as the needed order of updates is not known beforehand 
        self.updater('all_parameters')

# ======================
# Setter methods
# ======================


    def set_additional_parameters(self, time_resolution = None, invoke_updater=True):
        '''
        sets additional paramters
        Parameters
        ---------
        time_resolution:    Resolution of the simulation in us
        '''
        
        update_needed = False
        if time_resolution is not None:
            if time_resolution > 0:
                if self.additional_dict["time_resolution"] != float(time_resolution)*1E-6:
                    self.additional_dict["time_resolution"] = float(time_resolution)*1E-6
                    update_needed = True               
            else:
                self.logger.warning('Time resolution must be larger than zero')             
        if invoke_updater and update_needed: 
            self.updater('additional_dict')
        
        return update_needed
            
    
    def set_optics_parameters(self, f_objective = None, f_laser_collimation = None, f_laser_collimation_dist = None,\
                             laser_to_objective_dist = None, na_objective = None, f_relay = None, r_pinhole = None, detector_efficiency = None,\
                            optics_efficiency = None, detector_dead_time = None, eef_function_path = None,invoke_updater = True):
        '''
        sets the optics parameters
        Parameters
        ----------
        :param f_objective:                 Focal length of objective in mm
        :param f_laser_collimation:        Focal length of laser collimation lens in mm
        :param f_laser_collimation_dist:    Dist from Laser to collimation lens in mm
        :param laser_to_objective_dist:    Dist from Laser to objective in mm
        :param na_objective:                Numerical aperture of objective
        :param f_relay:                    Focal length of relay lens in mm
        :param r_pinhole:                    Radius of pinhole in um
        :param detector_efficiency:        Detector quantum efficiency
        :param optics_efficiency:            Optics efficiency
        :param detector_dead_time:        Dead time of the detector in ns
        :param eef_function_path:          Path to a encircled energy function which describes the eef in the confocal stop
        :param invoke_updater:
        '''
        update_needed = False
        if f_objective is not None:
            if f_objective > 0:
                if self.optics_dict['f_objective'] != float(f_objective)/1000:
                    self.optics_dict['f_objective'] = float(f_objective)/1000
                    update_needed = True
            else:
                self.logger.warning('f_objective parameter must be > zero')
                
        if f_laser_collimation is not None:
            if f_laser_collimation > 0:
                if self.optics_dict['f_laser_collimation'] != float(f_laser_collimation)/1000:
                    self.optics_dict['f_laser_collimation'] = float(f_laser_collimation)/1000
                    update_needed = True
            else:
                self.logger.warning('f_laser_collimation parameter must be > zero')
        
        if f_laser_collimation_dist is not None:
            if f_laser_collimation_dist > 0:
                if self.optics_dict['f_laser_collimation_dist'] != float(f_laser_collimation_dist)/1000:
                    self.optics_dict['f_laser_collimation_dist'] = float(f_laser_collimation_dist)/1000
                    update_needed = True
            else:
                self.logger.warning('f_laser_collimation_dist parameter must be > zero')
        
        if laser_to_objective_dist is not None:
            if laser_to_objective_dist > self.optics_dict['f_laser_collimation_dist']:
                if self.optics_dict['laser_to_objective_dist'] != float(laser_to_objective_dist)/1000:
                    self.optics_dict['laser_to_objective_dist'] = float(laser_to_objective_dist)/1000
                    update_needed = True
            else:
                self.logger.warning('laser_to_objective_dist parameter must be > f_laser_collimation_dist')
        
        if na_objective is not None:
            if (na_objective > 0) and (na_objective <1):
                if self.optics_dict['na_objective'] != float(na_objective):
                    self.optics_dict['na_objective'] = float(na_objective)
                    update_needed = True
            else:
                self.logger.warning('na_objective parameter must be > zero and < 1')
        
        if f_relay is not None:
            if f_relay > 0:
                if self.optics_dict['f_relay'] != float(f_relay)/1000:
                    self.optics_dict['f_relay'] = float(f_relay)/1000
                    update_needed = True
            else:
                self.logger.warning('f_relay parameter must be > zero')
        
        if r_pinhole is not None:
            if r_pinhole > 0:
                if self.optics_dict['r_pinhole'] != float(r_pinhole)/1E6:
                    self.optics_dict['r_pinhole'] = float(r_pinhole)/1E6
                    update_needed = True
            else:
                self.logger.warning('r_pinhole parameter must be > zero')
        
        if detector_efficiency is not None:
            if detector_efficiency >= 0:
                if self.optics_dict['detector_efficiency'] != float(detector_efficiency):
                    self.optics_dict['detector_efficiency'] = float(detector_efficiency)
                    update_needed = True
            else:
                self.logger.warning('detector_efficiency parameter must be >= zero')
        
        if optics_efficiency is not None:
            if optics_efficiency >= 0:
                if self.optics_dict['optics_efficiency'] != float(optics_efficiency):
                    self.optics_dict['optics_efficiency'] = float(optics_efficiency)
                    update_needed = True
            else:
                self.logger.warning('detector_efficiency parameter must be >= zero')
        
        if detector_dead_time is not None:
            if self.optics_dict["detector_dead_time"] != float(detector_dead_time)*1E-9:
                self.optics_dict["detector_dead_time"] = float(detector_dead_time)*1E-9
                update_needed = True  
                
        if (eef_function_path is not None) and (eef_function_path is not ""):
            if Path(eef_function_path).is_file():
                self.load_eef(eef_function_path)
                update_needed = True
            else:
                self.logger.warning(f'{eef_function_path} is not a file')
                self.optics_dict['encircled_energy_function'] = None
                self.optics_dict['encircled_energy_function_path'] = None
                update_needed = True
        
        if update_needed and invoke_updater:
            self.updater('optics_dict')
        return update_needed
            
            
    def set_scan_parameters(self, xy_speed = None, xy_scan_dist = None, objective_spin_radius = None,\
                             objective_rps = None, scan_time = None, bin_length = None, invoke_updater = True):
        '''
        sets the scan parameters
        Parameters
        ----------
        :param xy_speed:                 Speed of XY stage in mm/s
        :param xy_scan_dist:            Scan distance of XY stage (lateral length of square) in mm
        :param objective_spin_radius:    Radius of objective excenter in um
        :param objective_rps:            Objective rotation speed in rps
        :param scan_time:                Total scan time in s
        :param bin_length:               Bin length in us
        :param invoke_updater:
        '''
        update_needed = False
        if xy_speed is not None:
            if xy_speed > 0:
                if self.scan_dict['xy_speed'] != xy_speed/1000:
                    self.scan_dict['xy_speed'] = xy_speed/1000
                    update_needed = True
            else:
                self.logger.warning('xy_speed parameter must be > zero')
        
        if xy_scan_dist is not None:
            if xy_scan_dist >= 0:
                if self.scan_dict['xy_scan_dist'] != xy_scan_dist/1000:
                    self.scan_dict['xy_scan_dist'] = xy_scan_dist/1000
                    update_needed = True
            else:
                self.logger.warning('xy_scan_dist parameter must be >= zero')
        
        if objective_spin_radius is not None:
            if objective_spin_radius >= 0:
                if self.scan_dict['objective_spin_radius'] != objective_spin_radius/1E6:
                    self.scan_dict['objective_spin_radius'] = objective_spin_radius/1E6
                    update_needed = True
            else:
                self.logger.warning('objective_spin_radius parameter must be >= zero')

        if objective_rps is not None:
            if objective_rps >= 0:
                if self.scan_dict['objective_rps'] != objective_rps:
                    self.scan_dict['objective_rps'] = objective_rps
                    update_needed = True
            else:
                self.logger.warning('objective_rps parameter must be >= zero')
                
        if scan_time is not None:
            if scan_time > 0:
                if self.scan_dict['scan_time'] != scan_time:
                    self.scan_dict['scan_time'] = scan_time
                    update_needed = True
            else:
                self.logger.warning('scan_time parameter must be > zero')
                
        if bin_length is not None:
            if bin_length > 0:
                if self.scan_dict['bin_length'] != bin_length/1E6:
                    self.scan_dict['bin_length'] = bin_length/1E6
                    update_needed = True
            else:
                self.logger.warning('bin_length parameter must be > zero')
               
        
        if update_needed and invoke_updater:
            self.updater('scan_dict')
        return update_needed
        

    def set_noise_parameters(self, method=None, std = None, mean = None, detector_darknoise = None,  scale_noise = None, file_path=None, invoke_updater=True):
        '''
        sets the noise parameters
        @param method: 'Normal' for normal distributed noise, 'poisson' for poisson noise or 'file' for a measured noise
        @param std: Standard deviation of noise
        @param mean: Mean value of the noise, use this to set a poissonian noise
        @param detector_darknoise : dark count rate of the detector in Hz
        @param scale_noise: If True, noise is scaled with pinhole radius and laser power
        @param file_path: path to file with measured noise
        '''
        method_list = ['normal','poisson', 'file']
        update_needed = False
        if method is not None:
            if method in method_list:
                if method == 'file':
                    if file_path is None:  
                        if not os.path.isfile(self.noise_dict['file']):
                            self.logger.warning('Noise file not found: ' + file_path+". Method 'file' cannot be set")
                    else:
                        self.noise_dict['method'] = method
                        update_needed = True
                else:
                    if self.noise_dict['method'] != method:
                        self.noise_dict['method'] = method
                        update_needed = True
            else:
                self.logger.warning('Noise method must be one of ' + str(method_list))

        if std is not None:
            if std >= 0:
                if self.noise_dict['std'] != std:
                    self.noise_dict['std'] = std
                    update_needed = True
            else:
                self.logger.warning('std parameter must be >= zero')

        if mean is not None:
            if mean >= 0:
                if self.noise_dict['mean'] != mean:
                    self.noise_dict['mean'] = mean
                    update_needed = True
            else:
                self.logger.warning('mean parameter must be >= zero')
        
        if detector_darknoise is not None:
            if detector_darknoise >= 0:
                if self.noise_dict['detector_darknoise'] != detector_darknoise:
                    self.noise_dict['detector_darknoise'] = detector_darknoise
                    update_needed = True
            else:
                self.logger.warning('detector_darknoise parameter must be >= zero')
        
        if scale_noise is not None:
            if isinstance(scale_noise, bool):
                if self.noise_dict['scale_noise'] != scale_noise:
                    self.noise_dict['scale_noise'] = scale_noise
                    update_needed = True
            else:
                self.logger.warning('scale_noise must be True or False')

        if file_path is not None:
            if os.path.isfile(file_path):
                if self.noise_dict['file'] != file_path:
                    self.noise_dict['file'] = file_path
                    update_needed = True
            else:
                if  file_path != '':
                    self.logger.warning('laser noise file not found: ' + file_path)
                else:
                    if self.noise_dict['file'] != file_path:
                        self.noise_dict['file'] = file_path
                        update_needed = True
                #change method to "Normal" if "laser" is set but the file is not available
                if self.noise_dict['method'] == 'Laser':
                    self.noise_dict['method'] = 'Normal'
                    update_needed = True

        if update_needed and invoke_updater:
            self.updater('noise_dict')
        return update_needed
    
    def set_dye_parameters(self, dye_name=None, conc = None, epsilon = None, lifetime = None,  lifetime_isc=None,lifetime_triplet = None, quantum_efficiency = None, invoke_updater=True):
        '''
        sets the dye parameters
        @param dye_name: dye for a particular dye or 'manual'
        @param conc: Konzentration if fM
        @param epsilon: Mean value of the noise, use this to set a poissonian noise
        @param lifetime : lifetime of the excitet state in ns
        @param lifetime_isc: Inverse of intersystem crossing rate in us
        @param lifetime_triplet: lifetime of the triplet state in us
        @param quantum_efficiency: quantum efficiency
        '''
        dye_list = list(self.dye_dict["dye_instance"].all_dye_data.index)
        update_needed = False
        if dye_name is not None:
            if self.dye_dict["dye"] != dye_name:
                if dye_name in dye_list:
                    self.dye_dict["dye_instance"].set_dye(dye_name)
                    new_dye_dict = self.dye_dict["dye_instance"].data.to_dict()
                    new_dye_dict["dye"] = dye_name
                    self.dye_dict.update(new_dye_dict)
                    update_needed = True
                    
                elif  dye_name == 'manual':
                    self.dye_dict["dye_instance"] = dye.Dye(name = dye_name)
                    dye_data_dict = {key:self.dye_dict[key] for key in self.dye_dict["dye_instance"].all_dye_data.columns}
                    self.dye_dict["dye_instance"].set_dye(dye_name,**dye_data_dict)
                    self.dye_dict["dye"] = dye_name
                    update_needed = True
        
        if conc is not None:
            if conc >= 0:
                if self.dye_dict['conc'] != float(conc)*1e-15:
                    self.dye_dict['conc'] = float(conc)*1e-15
                    update_needed = True
            else:
                self.logger.warning('conc parameter must be >= zero')
        
        if self.dye_dict["dye"] == 'manual':
            
            if epsilon is not None:
                if epsilon >= 0:
                    if self.dye_dict['epsilon'] != float(epsilon):
                        self.dye_dict['epsilon'] = float(epsilon) 
                        update_needed = True
                else:
                    self.logger.warning('epsilon parameter must be >= zero')
            if lifetime is not None:
                if lifetime >= 0:
                    if self.dye_dict['lifetime'] != float(lifetime)*1e-9:
                        self.dye_dict['lifetime'] = float(lifetime)*1e-9
                        update_needed = True
                else:
                    self.logger.warning('epsilon parameter must be >= zero')
            if lifetime_isc is not None:
                if lifetime_isc >= 0:
                    if self.dye_dict['lifetime_isc'] != float(lifetime_isc)*1e-6:
                        self.dye_dict['lifetime_isc'] = float(lifetime_isc)*1e-6
                        update_needed = True
                else:
                    self.logger.warning('lifetime_isc parameter must be >= zero')
            if lifetime_triplet is not None:
                if lifetime_triplet >= 0:
                    if self.dye_dict['lifetime_triplet'] != float(lifetime_triplet)*1e-6:
                        self.dye_dict['lifetime_triplet'] = float(lifetime_triplet)*1e-6
                        update_needed = True
                else:
                    self.logger.warning('lifetime_triplet parameter must be >= zero')
            if quantum_efficiency is not None:
                if quantum_efficiency >= 0:
                    if self.dye_dict['quantum_efficiency'] != float(quantum_efficiency):
                        self.dye_dict['quantum_efficiency'] = float(quantum_efficiency)
                        update_needed = True
                else:
                    self.logger.warning('quantum_efficiency parameter must be >= zero')
        
            #update dye data if dye=='manual'
            dye_data_dict = {key:self.dye_dict[key] for key in self.dye_dict["dye_instance"].all_dye_data.columns}
            self.dye_dict["dye_instance"].set_dye('manual',**dye_data_dict)
            
                

        if update_needed and invoke_updater:
            self.updater('dye_dict')
        return update_needed

    def set_laser_parameters(self, power=None, wavelength = None, waist = None, m_squared = None, invoke_updater=True):
        '''
        sets the noise parameters
        @param power: Power of the laser in mW
        @param wavelength:Wavelength in nm
        @param waist: Waist of the Laser in um
        @param m_squared : Beam quality factor m_squared
        '''
        update_needed = False
        if power is not None:
            if power >= 0:
                if self.laser_dict['power'] != np.float_(power)*1E-3:
                    self.laser_dict['power'] = np.float(power)*1E-3
                    update_needed = True
            else:
                self.logger.warning('power parameter must be >= zero')
        
        if wavelength is not None:
            if wavelength >= 0:
                if self.laser_dict['wavelength'] != np.float_(wavelength)*1E-9:
                    self.laser_dict['wavelength'] = np.float(wavelength)*1E-9
                    update_needed = True
            else:
                self.logger.warning('wavelength parameter must be >= zero')
        
        if waist is not None:
            if waist >= 0:
                if self.laser_dict['laser_beam_waist'] != np.float_(waist)*1E-6:
                    self.laser_dict['laser_beam_waist'] = np.float(waist)*1E-6
                    update_needed = True
            else:
                self.logger.warning('waist parameter must be >= zero')
                
        if m_squared is not None:
            if m_squared >= 0:
                if self.laser_dict['m_squared'] != np.float_(m_squared):
                    self.laser_dict['m_squared'] = np.float(m_squared)
                    update_needed = True
            else:
                self.logger.warning('excitation_beam_waist parameter must be >= zero')


        if update_needed and invoke_updater:
            self.updater('laser_dict')
        return update_needed
            
           
    def set_calc_parameters(self, method = None, stats = None, threshold = None, normalisation = None,
                             single_count = None, invoke_updater = True):
        '''
        Sets the paramters for the number of events calculations
        :param method: selects the used method:
                        - local_baseline: use the baseline of the particular scan
                        - 0ppm_baseline: use a 0ppm baseline
                        - smc: use RDP SMC algorithm
                        - 
        :param stats:    Select the distribution of the noise. either "poisson" or "normal"
        @params threshold: threshold. If <1 it is enterpreted as quantile of the noise distribution, else as threshold above mean value in units of standard deviation of the data
        :param normalisation: if >0, the nr of events is normalised to the given scan length in s
        :param single:count : if True, it is assured that a molecule is counted only once, even if multiple bins are above threshold
        '''
    
        method_list = ['local_baseline','0ppm_baseline', 'smc']
        update_needed = False
        if method is not None:
            if method in method_list:
                if self.calc_dict['method'] != method:
                    self.calc_dict['method'] = method
                    update_needed = True
            else:
                self.logger.warning('Calc method must be one of ' + str(method_list))
        stats_list = ['poisson','poisson robust','normal','normal robust']
        if stats is not None:
            if stats in stats_list:
                if self.calc_dict['stats'] != stats:
                        if 'robust' in stats:
                            if self.rpy2_loaded:
                                self.calc_dict['stats'] = stats
                                update_needed = True
                            else:   
                                self.logger.warning("rpy2 not loaded. Cannot use robust statistics")                           
                        else:
                            self.calc_dict['stats'] = stats
                            update_needed = True
            else:
                self.logger.warning('Stats method must be one of ' + str(stats_list))
        
        if threshold is not None:
            if (threshold > 0):
                if self.calc_dict['threshold'] != np.float_(threshold):
                    self.calc_dict['threshold'] = np.float(threshold)
                    if self.calc_dict['threshold'] >= 1:
                        #this is interpreted as nsigma, so set nsigma in calc_dict
                        self.calc_dict['nsigma'] = self.calc_dict['threshold']
                        #adjust nsigma in the xml file for the SMC algorithm
                        self.adjust_SMC_values('nsigma')
                    else:
                        #we do not know nsigma without having data to calculate nsigma, so set it to NaN
                        self.calc_dict['nsigma'] = np.NaN
                    update_needed = True
            else:
                self.logger.warning('threshold parameter must be >0')
        
        if normalisation is not None:
            if self.calc_dict['normalisation'] != np.float_(normalisation):
                self.calc_dict['normalisation'] = np.float(normalisation)
                update_needed = True
        
        if single_count is not None:
            if self.calc_dict['single_count'] != bool(single_count):
                self.calc_dict['single_count'] = bool(single_count)
                update_needed = True
        
        if update_needed and invoke_updater:
            self.updater('calc_dict')
        return update_needed
    
    def get_calc_calculator(self):
        '''
        Updates the calculator with new parameters from self.calc_dict
        '''
        method = self.calc_dict["method"]
        stats = self.calc_dict["stats"]
        calculator = None
        if method == "local_baseline":
            if stats == "poisson":
                    calculator = self.calculate_events_poisson
            elif stats == "poisson robust":
                calculator = self.calculate_events_poisson_robust
            elif "normal" in stats:
                calculator = self.calculate_events_normal
        elif method =="smc":
            if self.SMC_calculator is not None: 
                calculator = self.calculate_events_smc
            else:
                self.logger.warning('SMC Library not loaded')
            
        self.calc_dict["calculator"] = calculator
        
    def import_rpy2(self):
        '''
        import all components from rpy2 used for the robust fit of the poisson distribution parameter
        '''
        try:        
            global rpy2, robjects, importr, numpy2ri, pandas2ri, localconverter
            global r_robustbase, r_base,  r_stats
        
            os.environ["R_USER"] = "peterdietiker"
            import rpy2
            import rpy2.robjects as robjects
            from rpy2.robjects.packages import importr
            r_robustbase = importr('robustbase')
            r_base = importr('base')
            r_stats = importr('stats')
            from rpy2.robjects import numpy2ri
            #numpy2ri.activate()
            from rpy2.robjects import pandas2ri
            from rpy2.robjects.conversion import localconverter
        except:
            self.logger.info("rpy2 not loaded. cannot use robust estimator")
        else:
            self.rpy2_loaded = True
        
    def load_from_stream(self):
        try:
            if isinstance(self.stream_dict["active_streamer"],rdp_streamer):
                data, concentration = self.stream_dict["active_streamer"].load()
                self.update_conc_from_stream(concentration)
                #self.logger.info(f"concentration: {concentration} nM")
            else:
                data = self.stream_dict["active_streamer"].load()
        except (EOFError, StopIteration):
            # end of file or no wells 
            raise
            return
        new_data = data
        if new_data.shape[1] != self.signal_df.shape[1]:
            new_scan_time = self.scan_dict['bin_length']*new_data.shape[1]
            self.set_scan_parameters(scan_time = new_scan_time)
        self.signal_df.loc[:,'sum'] = new_data[1,:]
    
    @event
    def update_conc_from_stream(self,conc):
        '''
        helper function used to update concentration if data is loaded from rdp result stream
        It's intention is the be decorated as an event. 
        The event can then be handled by a gui for example
        :param conc: new concentration in nM
        :type conc: float
        '''
        self.dye_dict['conc'] = 1E-15*conc
        
        

    def load_from_zmq(self):
        '''
        loads new data from the zmq server

        the function checks the lengths of the received array.
        If it not equals to self.piezo_tuning_dict['nr_steps'], it tries to
        reload the data. 
        If the received data has identical length for more than 10 times,
        it sets self.piezo_tuning_dict['nr_steps'] to the new length.
        '''

        data=self.zmq_streamer.receive()
        if not isinstance(data,np.ndarray):
            raise EOFError("zmq")
            return
        new_bin_length= data[0]
        new_nr_bins = data[1]
        new_scan_time = new_bin_length * new_nr_bins
        if (new_bin_length != self.scan_dict['bin_length']) or (new_scan_time != self.scan_dict['scan_time']):
            self.set_scan_parameters(bin_length = new_bin_length*1E6, scan_time = new_scan_time)

        self.signal_df.loc[:,'sum'] = data[2:]


    def set_zmq_parameter(self,enable=None,address=None):
        '''
        sets the parameters for the zmq streaming
        :param enable: True for enable or False for disable
        :type enable: bool
        :param address: address of the server in the format "tcp://ip:port
        :type address: str
        '''
        update_needed = False
        if enable is not None:
            if self.zmq_dict['enable'] != enable:
                self.zmq_dict['enable'] = enable
                update_needed = True

        if address is not None:
            if self.zmq_dict['address'] != address:
                self.zmq_dict['address'] = address
                update_needed = True

        if update_needed:
            self.updater('zmq_dict')


    def set_stream_parameter(self, enable=None, mode=None, file_path=None):
        '''
        Sets the parameters of the stream
        :param enable: True for enable or False for disable
        :type enable: bool
        :param mode : 'load' to load spectra from stream or 'dump' to dump spectra to stream
        :type: mode: string
        :param file_path: file path to the file
        :type file_path: string
        '''
        update_needed = False
        if enable is not None:
            if self.stream_dict['enable'] != enable:
                self.stream_dict['enable'] = enable
                update_needed = True

        mode_list = ['dump', 'load']
        if mode is not None:
            if mode in mode_list:
                if self.stream_dict['mode'] != mode:
                    self.stream_dict['mode'] = mode
                    update_needed = True
            else:
                self.logger.warning('stream mode must be one of ' + str(mode_list))

        if file_path is not None:
            if os.path.isfile(file_path):
                if self.stream_dict['file'] != file_path:
                    self.stream_dict['file'] = file_path
                    update_needed = True
            else:
                #create file if it not exists
                if self.stream_dict['mode'] == "dump":
                    if not os.path.exists(file_path):
                        dirname = os.path.dirname(file_path)
                        #creat directory
                        if not os.path.isdir(dirname):
                            try:
                                os.makedirs(dirname)
                            except (OSError, WindowsError):
                                self.logger.warning('wrong path:'+dirname)
                                return
                        open(file_path, 'w').close()
                else:
                    if  file_path != '':
                        self.logger.warning('Stream file not found: ' + file_path)
                    else:
                        if self.stream_dict['file'] != file_path:
                            self.stream_dict['file'] = file_path
                            update_needed = True
                    #change method to "Normal" if "laser" is set but the file is not available
                    if self.stream_dict['enable'] == True:
                        self.stream_dict['enable'] = False
                        update_needed = True

        if update_needed:
            self.updater('stream_dict')

    def update_stream_parameters(self):
        enable = self.stream_dict['enable']
        mode = self.stream_dict['mode']
        file_path = self.stream_dict['file']
        if enable is True:
            if mode == "load" and self.zmq_dict["enable"]==True:
                self.logger.warning("cannot enable streaming from file and zmq at the same time, disable zmq streaming first")
                self.stream_dict['enable']=False
                return
            if file_path is not None:
                if ".zip" in file_path:
                    #RDP file
                    self.rdp_streamer.set_path(file_path)
                    self.stream_dict['active_streamer'] = self.rdp_streamer
                    if self.rdp_streamer.rdp is None:
                        self.logger.warning("No file selected")
                        self.stream_dict['enable'] = False
                        
                else:
                    self.pickle_streamer.set_path(file_path)
                    self.stream_dict['active_streamer'] = self.pickle_streamer
                    if self.pickle_streamer.handle is None:
                        self.logger.warning("No file selected")
                        self.stream_dict['enable'] = False
        else:
            # close file
            del self.pickle_streamer
            self.pickle_streamer = pickle_streamer(logger=self.logger)
            del self.rdp_streamer
            self.rdp_streamer = rdp_streamer(logger = self.logger)
            
    def update_zmq_parameters(self):
        enable = self.zmq_dict['enable']
        address = self.zmq_dict['address']
        if enable is True:
            if self.stream_dict['enable'] and self.stream_dict['mode']=="load":
                self.logger.warning("cannot enable streaming from file and zmq at the same time, disable file streaming first")
                self.zmq_dict['enable']= False
                return
            if address is not None:
                self.zmq_streamer.connect_socket(address)
        
        

    def dump_to_file(self, file_path=None):
        '''
        saves the actual spectrum as a pickle of an numpy array.
        The columns of the array are:
        piezo voltage, wavelength,channel 1, channel 2
        :param file_path: path to the file
        :type file_path: string
        '''

        data_to_stream = np.vstack((self.signal_df.index,self.signal_df['sum']))

        if (not self.pickle_streamer.file_path == file_path) and (file_path is not None):
            self.pickle_streamer.set_path(file_path)

        self.pickle_streamer.dump(data_to_stream)
        
    def return_counts(self, ax=None,  no_noise=False):
        '''return the simulated counts
        @param no_noise: If true the noise-less counts are returned
        '''

        if no_noise:
            y_axis = 'signal'
        else:
            y_axis = 'sum'

        return self.signal_df.index.values,self.signal_df[y_axis].values
    

    def adjust_SMC_values(self, which=('all',)):
        '''
        Adjusts the file DefaultValues.xml used by the SMC algorithm
        '''
        if isinstance(which, str):
            which = (which,)
        
        if os.path.exists(self.SMC_actual_values_path):
            with open(self.SMC_actual_values_path,"r") as f:
                data = f.read()
            
            if len(set(which) & set(['all', 'bin_length'])):
                regex = re.compile(r"(?<=<BinReadTime>)\d+(?=<\/BinReadTime>)",re.MULTILINE)
                bin_length = self.scan_dict["bin_length"]*10**6
                try:
                    data = re.sub(regex, f"{bin_length:.0f}", data, 1)
                except Exception:
                    self.logger.error("regex not found", exc_info=True)
            
            if len(set(which) & set(['all', 'nsigma'])):
                regex = re.compile(r"(?<=<NSigma>).*(?=<\/NSigma>)",re.MULTILINE)
                nsigma = self.calc_dict["nsigma"]
                try:
                    data = re.sub(regex, f"{nsigma:.2f}", data, 1)
                except Exception:
                    self.logger.error("regex not found", exc_info=True)

            with open(self.SMC_actual_values_path,"w") as f:
                f.write(data)
                
                

# ## updater

    def updater(self, changed_parameter_name):
        # this helper call all the functions needed to update the spectrum after a parameter has been changed
        if changed_parameter_name == 'additional_dict':
            self.calculate_number_of_steps()
            self.init_signal_df()
            self.get_noise_generator()
            self._solve_path()
            self.calc_path_parameters()
            self.calc_infos()
            self.sample_distribution()
        elif changed_parameter_name == 'scan_dict':
            self.calculate_number_of_steps()
            self.init_signal_df()
            self.get_noise_generator()
            self._solve_path()
            self.calc_path_parameters()
            self.calc_infos()
            self.sample_distribution()
            self.adjust_SMC_values()
        elif changed_parameter_name == 'optics_dict':
            self.get_objective_efficiency()
            self.calc_beams()
            self.vectorize_functions()
            self.get_noise_generator()
            self.calc_infos()
            self.sample_distribution()
        elif changed_parameter_name == 'laser_dict':
            self.calc_beams()
            self.vectorize_functions()
            self.get_noise_generator()
            self.calc_infos()
            self.sample_distribution()
        elif changed_parameter_name == 'noise_dict':
            self.get_noise_generator()
        elif changed_parameter_name == 'dye_dict':
            self.vectorize_functions()
            self.calc_infos()
            self.sample_distribution()
        elif changed_parameter_name == 'calc_dict':
            self.get_calc_calculator()
            self.adjust_SMC_values()
        elif changed_parameter_name == 'stream_dict':
            self.update_stream_parameters()
        elif changed_parameter_name == 'zmq_dict':
            self.update_zmq_parameters()
        elif changed_parameter_name == 'all_parameters':
            self.calculate_number_of_steps()
            self._solve_path()
            self.calc_path_parameters()
            self.init_signal_df()
            self.get_noise_generator()
            self.calculate_noise()
            self.calc_beam()
            self.calc_infos()
            self.sample_distribution()
        else:
            self.logger.warning('updater received wrong parameter: ' + changed_parameter_name)

# ## plot helper

 



    def plot_counts(self, ax=None,  no_noise=True):
        '''plots the simulated counts
        @param ax: axis to plot the curve. If 'None' a new fig is generated
        @param no_noise: If true the noise-less counts are plotted
        '''

        if no_noise:
            y_axis = 'signal'
        else:
            y_axis = 'sum'

       

        if ax is None:
            fig, ax = plt.subplots(1)
        else:
            ax.cla()
        ax.plot(self.signal_df.index,self.signal_df[y_axis], label='Counts / bin')
        hf.set_plot_limits(ax, 'x')
        hf.set_plot_limits(ax, 'y')

        ax.set_ylabel('Counts')
        ax.legend(loc=0)
        return ax.figure, ax


    def plot_path(self, ax=None, init=False):
        '''plots the path
        @param ax: axis to plot the curve. If 'None' a new fig is generated
        @param init: True if this is the first plot

        '''
        if ax is None:
            fig, ax = plt.subplots(1)
            init = True

        if init:
            ax.cla()
            ax.set_aspect("equal")

            ax.set_xlabel("X / mm")
            ax.set_ylabel("Y / mm")
            ax.set_xlim((-self.scan_dict["objective_spin_radius"]-self.scan_dict["xy_scan_dist"]/10)*1000,(1*self.scan_dict["objective_spin_radius"]+1.1*self.scan_dict["xy_scan_dist"])*1000)
            ax.set_ylim((-self.scan_dict["objective_spin_radius"]-self.scan_dict["xy_scan_dist"]/10)*1000,(1*self.scan_dict["objective_spin_radius"]+1.1*self.scan_dict["xy_scan_dist"])*1000)
            #bax.figure.tight_layout()
        
        if (self.dye_dict["conc"] is not None) and (self.dye_dict["conc"] < 100000) :
            self.plot_molecules(ax)
        ax.plot(self.sol.y[0]*1000,self.sol.y[1]*1000)
        #fig.tight_layout()
        bin_idxs = None
        if self.scan_dict["bin_length"] is not None:
            bin_idxs = self.plot_bins(ax)
        if self.laser_dict["excitation_beam_waist"] is not None:
            self.plot_beam(ax)

        return ax.figure, ax

    def plot_beam(self, ax):
        '''
        Plottet den laserstrahl
        '''
        #b = self.beam(self.laser_dict["excitation_beam_waist"])
        #y = self.sol.y[1][:,np.newaxis]+ b[:,0]
        #x = self.sol.y[0][:,np.newaxis]+ b[:,1]
        if self.bin_idxs is None:
            nr_points = self.sol.y.shape[1] if  self.sol.y.shape[1]<40 else 40
            idxs = np.linspace(0,self.sol.y.shape[1],nr_points,endpoint=False,dtype=np.int)
        else:
            idxs = self.bin_idxs
        for idx in idxs:
            circ = plt.Circle((self.sol.y[0,idx]*1000, self.sol.y[1,idx]*1000), self.laser_dict["excitation_beam_waist"]*1000, color='r',fill=False)
            ax.add_artist(circ)
    
    def plot_molecules(self, ax, z_max = "z_max"):#"Rayleigh"):
        '''
        Plottet Punkte, die Moleküle representieren
        '''
        xmin,xmax = ax.get_xlim()
        ymin,ymax = ax.get_ylim()
        
        z_max_value = 0
        if z_max == "Rayleigh":
            z_max_value = self.laser_dict["excitation_rayleigh_length"]
        elif z_max == "z_max":
            try:
                z_max_value = self.result_dict["z_max"]
            except AttributeError:
                #z_max not yet calculated
                z_max_value = self.laser_dict["excitation_rayleigh_length"]
            
    
        
        volume = (xmax-xmin)*(ymax-ymin)*2*z_max_value*10**(-6) #umrechnung auf m^3

        nr_molecules = int(volume*self.dye_dict["conc"]*6.022E23*1000) #umrechnung von mol/l auf mol/m^3
        x = (xmax - xmin) * np.random.random_sample(nr_molecules) + xmin
        y = (ymax - ymin) * np.random.random_sample(nr_molecules) + ymin
        ax.plot(x,y,color="green", marker="o",  markersize=1,linestyle = '')
        
    def plot_bins(self,ax):
        '''
        berechnet und plottet die Grenzen des Bins
        '''
        if self.scan_dict["scan_time"]/self.scan_dict["bin_length"]>1000:
            return #too many bins to plot
        #Zeit der Bingrenzen
        self.t_bin = np.arange(0,self.scan_dict["scan_time"], self.scan_dict["bin_length"])
        #indexes aus der Lösung bei welchen der bin wechselt
        idxs = np.searchsorted(self.sol.t, self.t_bin, side="left")
        #lösche indexes die ganz am ende des Zeitreihe sind
        #da auch der Datenpunkt idx+1 benötigt wird
        self.bin_idxs = idxs[idxs<(self.sol.t.shape[0]-1)]
        idxs = self.bin_idxs
        unique, counts = np.unique(idxs, return_counts=True)
        if counts.max() > 1:
            print("step size larger than bin_length")
            #return
        #Koordinate der Bin Grenzen
        bincoords =  np.hstack((self.sol.y[0][idxs][:,np.newaxis],self.sol.y[1][idxs][:,np.newaxis]))
        #Koordinaten der Datenpunkte vor und nach der Grenze des bins
        #Die Grenze des Bins wird it einem Vektor markiert, der senkrecht auf der Verbindungslinie
        #zwischen diesen beiden Datenpunkte steht
        y_bins = np.hstack((self.sol.y[1][idxs-1][:,np.newaxis],self.sol.y[1][idxs+1][:,np.newaxis]))
        x_bins =  np.hstack((self.sol.y[0][idxs-1][:,np.newaxis],self.sol.y[0][idxs+1][:,np.newaxis]))
        #vectoren, die senkrecht zu den vektoren aus y_bins und x_bins stehen
        yvec = np.diff(x_bins,axis = 1)
        xvec = -np.diff(y_bins,axis = 1)
        vectors =np.hstack((xvec,yvec))
        # Vektoren normieren
        vectors = vectors / np.linalg.norm(vectors, axis=1)[:, np.newaxis]
        # länge der Vektoren an die Seitenlänge des Scans anpassen
        vectors = vectors * self.scan_dict["xy_scan_dist"]/50
        #Koordinaten der beiden Begrenzungspunkte, welche die Grenze des Bins angeben
        #umrechnung auf mm
        low_point = (bincoords - vectors/2)*1000
        upper_point = (bincoords + vectors/2)*1000
        for idx in range(vectors.shape[0]):
            ax.plot([low_point[idx,0],upper_point[idx,0]],[low_point[idx,1],upper_point[idx,1]],color="black")
            
    
    def plot_bin_histograms(self,axs = None,init = False):
        '''
        Plots informtive histograms of the counts distribution
        '''
        
        #make sure to simulate long enough for a reaonable velocity distribution
        if axs is None:
            import matplotlib.gridspec as gridspec
            fig = plt.figure()
            axs =[]
            gs = gridspec.GridSpec(2, 2)
            axs.append(fig.add_subplot(gs[0, 0]))
            axs.append(fig.add_subplot(gs[1, 0],sharex=axs[0]))
            axs.append(fig.add_subplot(gs[1, 1]))
        
        else:
            fig = axs[0].figure
        
        for ax in axs:
            ax.clear()
        
        max_signal = self.binned_signal_array.max()
        total_signal = np.clip(np.sum(self.binned_signal_array),1,None) #save us from zero division
        bin_hist = np.sum(self.all_binned_signal_array > max_signal/100, axis=0)/self.all_binned_signal_array.shape[0]
        signal_distribution_hist = np.sum(self.all_binned_signal_array, axis=0)/total_signal
        axs[0].bar(1+np.arange(self.all_binned_signal_array.shape[1]),bin_hist,align="center")
        axs[0].set_ylabel("fraction observed bins")
        axs[0].get_xaxis().set_visible(False)
        axs[0].xaxis.set_major_locator(mpl.ticker.MaxNLocator(integer=True))
        axs[0].set_xlim(0,self.all_binned_signal_array.shape[1]+1)

        axs[1].bar(1+np.arange(self.all_binned_signal_array.shape[1]),signal_distribution_hist)
        axs[1].set_ylabel("fraction signal in bin")
        axs[1].set_xlabel("bin")
        
    
        if not np.prod(self.all_binned_signal_array.shape)>0:
            weights = np.full(self.binned_signal_array.shape[0],1/self.binned_signal_array.shape[0])   
            axs[2].set_ylabel("fraction observed bins")
        else:
            weights = np.full(self.binned_signal_array.shape[0],1/(np.prod(self.all_binned_signal_array.shape)))
            axs[2].set_ylabel("number of bins")
        axs[2].hist(self.binned_signal_array,bins=50, weights = weights)
        axs[2].set_xlim(1,axs[2].get_xlim()[1])
        axs[2].set_yscale('log')
        
        axs[2].set_xlabel("Counts / bin")
        fig.tight_layout()
        return fig
    

    def plot_power_spectrum(self, ax=None, init=False):
        '''plots the power spectrum
        @param ax: axis to plot the curve. If 'None' a new fig is generated
        @param init: True if this is the first plot

        '''
        x_array = self.signal_df['x_axes', 'wavelength']
        y_array = self.signal_df['y_axes', 'power']

        if ax is None:
            fig, ax = plt.subplots(1)
            init = True

        if init:
            ax.plot(x_array, y_array, label='Power spectrum')
            ax.set_xlabel('Wavelength / nm')
            ax.set_ylabel('Signal / mV')
            # ax.legend(loc=0)
            ax.figure.tight_layout()
        else:
            ax.lines[0].set_xdata(x_array)
            ax.lines[0].set_ydata(y_array)

        hf.set_plot_limits(ax, 'x')
        hf.set_plot_limits(ax, 'y')

        return ax.figure, ax
    
    def plot_bin_histogram_signal(self,ax = None,init = False):
        '''
        Plots histogram of the signal including noise
        '''
        
        #make sure to simulate long enough for a reaonable velocity distribution
        if ax is None:
            fig, ax  = plt.subplots()
        
        else:
            fig = ax.figure
        

        ax.clear()
        max_value = self.signal_df["sum"].max()
        ax.hist(self.signal_df["sum"], bins = np.arange(max_value+1))
        ax.axvline(self.result_dict['threshold_counts'],color='red')
        
        ax.set_xlabel("Counts / bin")
        #fig.tight_layout()
        return fig

    def plot_noise_pdf(self, ax=None, init=False, axes=('1')):

        if ax is None:
            fig, ax = plt.subplots(1)
            init = True
            ax.clear()
            self.noise_dict['noise_pdf'].pdf_plot1D(value=0, ax=ax, axis=axes[0])
        

if __name__ == '__main__':
    mpl.interactive(False)
    test = signal_simulator()
    test.take_measurement()
    print(test.results_df.results_df)
    print(test.get_info_string())
    fig, ax = plt.subplots()
    #ax.plot(np.arange(19))
    test.results_df.add_result()
    test.nr_events_buffer.reset()
    test.set_dye_parameters(conc=0)
    test.take_measurement()
    test.results_df.add_result()
    test.nr_events_buffer.reset()
    test.set_dye_parameters(conc=10)
    test.take_measurement()
    test.results_df.add_result()
    test.nr_events_buffer.reset()
    test.set_dye_parameters(conc=20)
    test.take_measurement()
    test.results_df.add_result()
    #test.results_df.plot_results(ax, x_axis = 'conc', init = True)
    #test.plot_path(ax= ax, init=True)
    test.plot_bin_histograms()
    plt.show()
    print(test.results_df)
    #ax.plot(test.sol.y[0],test.sol.y[1])
    #fig.show()
    #plt.pause(0.01)
    
