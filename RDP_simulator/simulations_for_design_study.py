from RDP_simulator.scripting_methods import ScriptingMethods as sm #Do not run this line!!!
import numpy as np
print(sm.RDP.sig_sim.calc_dict)
print(sm.RDP.sig_sim.result_dict)
import pandas

def set_default_parameter():
    sm.set_laser_parameters(power=5, wavelength=642,waist = 1.2,m_squared=1)
    sm.set_calc_parameters(method = 'local_baseline', stats = 'poisson robust', threshold_quantile = 5.5,\
                             normalisation = 60, single_count = True)
    sm.set_dye_parameters(dye="Alexa Fluor 647", conc=0)
    sm.set_noise_parameters(scale_noise = True, mean = 250000)
    sm.set_optics_parameters(f_objective=3, na_objective=0.7, r_pinhole=75)
    sm.set_optics_parameters(f_laser_collimation = 5.5, f_laser_collimation_dist = 5.5)
    sm.set_scan_parameters(bin_length=100)

sm.set_statistics_size(10)
sm.take_measurements(10)


sm.reset_statistics()
sm.reset_results_df()

####################
# effect of NA
####################
sm.reset_statistics()
sm.reset_results_df()
set_default_parameter()
dyes = ["Alexa Fluor 532", "Alexa Fluor 647", "Alexa Fluor 750"]
wavelengths = [520, 642,720]
laser_waists = [1.7, 1.2,2.5] #werte berechnet aus Beam profiler messung
noise_means = [500000,250000,120000]
nas = np.linspace(0.4,0.8,10)
for dye,wavelength, laser_waist, noise_mean in zip(dyes, wavelengths, laser_waists, noise_means):
    sm.set_dye_parameters(dye = dye, conc = 0)
    plot_color = sm.RDP.sig_sim.dye_dict["dye_instance"].color
    sm.set_laser_parameters(power = 5, wavelength = wavelength, waist = laser_waist)
    sm.set_noise_parameters(scale_noise = True, mean = noise_mean)
    for na in nas:
        sm.set_optics_parameters(na_objective=na)
        for method, stats in zip(['smc','local_baseline'],['poisson robust']*2):
            sm.set_calc_parameters(method = method, stats = stats, threshold_quantile = 5.5,\
                             normalisation = 60, single_count = True)
            for conc in [100]:#np.linspace(0,100,5):
                sm.set_dye_parameters(conc = conc)
                sm.reset_statistics()
                sm.take_measurements(10)
                slope = sm.RDP.sig_sim.nr_events_buffer.mean / 100
                noise_mean = sm.RDP.sig_sim.signal_df["noise"].mean()
                signal_mean = sm.RDP.sig_sim.signal_df["signal"].mean()
                sm.RDP.sig_sim.results_df.add_result(color = [plot_color],slope=slope, noise_mean=noise_mean, signal_mean=signal_mean)
                sm.plot_results(x_axis = 'na_objective', plot_color = "color")
sm.RDP.sig_sim.results_df.save("DE_vs_dye_NA.csv")

####################
# effect of focal length
####################
sm.reset_statistics()
sm.reset_results_df()

dyes = ["Alexa Fluor 532", "Alexa Fluor 647", "Alexa Fluor 750"]
wavelengths = [520, 642,720]
laser_waists = [1.7, 1.2,2.5]
noise_means = [500000,250000,120000]
fls = np.linspace(1,10,10)
set_default_parameter()
for dye,wavelength, laser_waist, noise_mean in zip(dyes, wavelengths, laser_waists, noise_means):
    sm.set_dye_parameters(dye = dye, conc = 0)
    plot_color = sm.RDP.sig_sim.dye_dict["dye_instance"].color
    sm.set_laser_parameters(power = 5, wavelength = wavelength, waist = laser_waist)
    sm.set_noise_parameters(scale_noise = True, mean = noise_mean)
    for fl in fls:
        sm.set_optics_parameters(f_objective=fl)
        for method, stats in zip(['smc','local_baseline'],['poisson robust']*2):
            sm.set_calc_parameters(method = method, stats = stats, threshold_quantile = 5.5,\
                             normalisation = 60, single_count = True)
            for conc in [100]:#np.linspace(0,100,5):
                sm.set_dye_parameters(conc = conc)
                sm.reset_statistics()
                sm.take_measurements(10)
                slope = sm.RDP.sig_sim.nr_events_buffer.mean / 100
                noise_mean = sm.RDP.sig_sim.signal_df["noise"].mean()
                signal_mean = sm.RDP.sig_sim.signal_df["signal"].mean()
                sm.RDP.sig_sim.results_df.add_result(color = [plot_color],slope=slope, noise_mean=noise_mean, signal_mean=signal_mean)
                sm.plot_results(x_axis = 'f_objective', plot_color = "color")
sm.RDP.sig_sim.results_df.save("DE_vs_dye_f_objective.csv")

####################
# effect of wavelength
####################
sm.reset_statistics()
sm.reset_results_df()
set_default_parameter()
dyes = ["Alexa Fluor 532", "Alexa Fluor 647", "Alexa Fluor 750"]
wavelengths = [np.linspace(520-50,520+50,10), np.linspace(647-50,647+50,10),np.linspace(750-50,750+50,10)]
laser_waists = [1.7, 1.2,2.5]
noise_means = [500000,250000,120000]
fls = np.linspace(1,10,10)
for dye,wavelength, laser_waist, noise_mean in zip(dyes, wavelengths, laser_waists, noise_means):
    sm.set_dye_parameters(dye = dye, conc = 0)
    plot_color = sm.RDP.sig_sim.dye_dict["dye_instance"].color
    sm.set_laser_parameters(waist = laser_waist)
    sm.set_noise_parameters(scale_noise = True, mean = noise_mean)
    for wl in wavelength:
        sm.set_laser_parameters(wavelength=wl)
        for method, stats in zip(['smc','local_baseline'],['poisson robust']*2):
            sm.set_calc_parameters(method = method, stats = stats, threshold_quantile = 5.5,\
                             normalisation = 60, single_count = True)
            for conc in [100]:#np.linspace(0,100,5):
                sm.set_dye_parameters(conc = conc)
                sm.reset_statistics()
                sm.take_measurements(10)
                slope = sm.RDP.sig_sim.nr_events_buffer.mean / 100
                noise_mean = sm.RDP.sig_sim.signal_df["noise"].mean()
                signal_mean = sm.RDP.sig_sim.signal_df["signal"].mean()
                sm.RDP.sig_sim.results_df.add_result(color = [plot_color],slope=slope, noise_mean=noise_mean, signal_mean=signal_mean)
                sm.plot_results(x_axis = 'wavelength', plot_color = "color")
sm.RDP.sig_sim.results_df.save("DE_vs_dye_wl.csv")


####################
# effect of power
####################
sm.reset_statistics()
sm.reset_results_df()
set_default_parameter()
dyes = ["Alexa Fluor 532", "Alexa Fluor 647", "Alexa Fluor 750"]
wavelengths = [520, 642, 720]
laser_waists = [1.7, 1.2,2.5]
noise_means = [500000,250000,120000]
powers = np.linspace(1, 15, 15)
for dye, wavelength, laser_waist, noise_mean in zip(dyes, wavelengths, laser_waists, noise_means):
    sm.set_dye_parameters(dye=dye, conc=0)
    plot_color = sm.RDP.sig_sim.dye_dict["dye_instance"].color
    sm.set_laser_parameters(wavelength = wavelength, waist = laser_waist)
    sm.set_noise_parameters(scale_noise=True, mean=noise_mean)
    for power in powers:
        sm.set_laser_parameters(power=power)
        for method, stats in zip(['smc', 'local_baseline'], ['poisson robust']*2):
            sm.set_calc_parameters(method=method, stats=stats, threshold_quantile=5.5,\
                             normalisation = 60, single_count = True)
            for conc in [100]:  #np.linspace(0,100,5):
                sm.set_dye_parameters(conc=conc)
                sm.reset_statistics()
                sm.take_measurements(10)
                slope = sm.RDP.sig_sim.nr_events_buffer.mean / 100
                noise_mean = sm.RDP.sig_sim.signal_df["noise"].mean()
                signal_mean = sm.RDP.sig_sim.signal_df["signal"].mean()
                sm.RDP.sig_sim.results_df.add_result(color = [plot_color],slope=slope, noise_mean=noise_mean, signal_mean=signal_mean)
                sm.plot_results(x_axis='power', plot_color="color")
sm.RDP.sig_sim.results_df.save("DE_vs_dye_power.csv")

####################
# effect of laser collimation
####################
sm.reset_statistics()
sm.reset_results_df()
set_default_parameter()
dyes = ["Alexa Fluor 532", "Alexa Fluor 647", "Alexa Fluor 750"]
wavelengths = [520, 642,720]
laser_waists = [1.7, 1.2,2.5]
noise_means = [500000,250000,120000]
f_collimations = np.linspace(1,10,10)
for dye,wavelength, laser_waist, noise_mean in zip(dyes, wavelengths, laser_waists, noise_means):
    sm.set_dye_parameters(dye = dye, conc = 0)
    plot_color = sm.RDP.sig_sim.dye_dict["dye_instance"].color
    sm.set_laser_parameters(wavelength = wavelength, waist = laser_waist)
    sm.set_noise_parameters(scale_noise = True, mean = noise_mean)
    for f_collimation in f_collimations:
        sm.set_optics_parameters(f_laser_collimation = f_collimation, f_laser_collimation_dist = f_collimation)
        for method, stats in zip(['smc','local_baseline'],['poisson robust']*2):
            sm.set_calc_parameters(method = method, stats = stats, threshold_quantile = 5.5,\
                             normalisation = 60, single_count = True)
            for conc in [100]:#np.linspace(0,100,5):
                sm.set_dye_parameters(conc = conc)
                sm.reset_statistics()
                sm.take_measurements(10)
                slope = sm.RDP.sig_sim.nr_events_buffer.mean / 100
                noise_mean = sm.RDP.sig_sim.signal_df["noise"].mean()
                signal_mean = sm.RDP.sig_sim.signal_df["signal"].mean()
                sm.RDP.sig_sim.results_df.add_result(color = [plot_color],slope=slope, noise_mean=noise_mean, signal_mean=signal_mean)
                sm.plot_results(x_axis = 'f_laser_collimation', plot_color = "color")
sm.RDP.sig_sim.results_df.save("DE_vs_dye_f_collimation.csv")

####################
# effect of  pinhole radius
####################
sm.reset_statistics()
sm.reset_results_df()
set_default_parameter()
dyes = ["Alexa Fluor 532", "Alexa Fluor 647", "Alexa Fluor 750"]
wavelengths = [520, 642,720]
laser_waists = [1.7, 1.2,2.5]
noise_means = [500000,250000,120000]
r_pinholes = np.linspace(20,150,10)
for dye,wavelength, laser_waist, noise_mean in zip(dyes, wavelengths, laser_waists, noise_means):
    sm.set_dye_parameters(dye = dye, conc = 0)
    plot_color = sm.RDP.sig_sim.dye_dict["dye_instance"].color
    sm.set_laser_parameters(wavelength = wavelength, waist = laser_waist)
    sm.set_noise_parameters(scale_noise = True, mean = noise_mean)
    for r_pinhole in r_pinholes:
        sm.set_optics_parameters(r_pinhole = r_pinhole)
        for method, stats in zip(['smc','local_baseline'],['poisson robust']*2):
            sm.set_calc_parameters(method = method, stats = stats, threshold_quantile = 5.5,\
                             normalisation = 60, single_count = True)
            for conc in [100]:#np.linspace(0,100,5):
                sm.set_dye_parameters(conc = conc)
                sm.reset_statistics()
                sm.take_measurements(10)
                slope = sm.RDP.sig_sim.nr_events_buffer.mean / 100
                noise_mean = sm.RDP.sig_sim.signal_df["noise"].mean()
                signal_mean = sm.RDP.sig_sim.signal_df["signal"].mean()
                sm.RDP.sig_sim.results_df.add_result(color = [plot_color],slope=slope, noise_mean=noise_mean, signal_mean=signal_mean)
                sm.plot_results(x_axis = 'r_pinhole', plot_color = "color")
sm.RDP.sig_sim.results_df.save("DE_vs_dye_r_pinhole.csv")

####################
# effect of  M^2
####################
sm.reset_statistics()
sm.reset_results_df()
set_default_parameter()
dyes = ["Alexa Fluor 532", "Alexa Fluor 647", "Alexa Fluor 750"]
wavelengths = [520, 642,720]
laser_waists = [1.7, 1.2,2.5]
noise_means = [500000,250000,120000]
M2s = np.linspace(1,5,10)
for dye,wavelength, laser_waist, noise_mean in zip(dyes, wavelengths, laser_waists, noise_means):
    sm.set_dye_parameters(dye = dye, conc = 0)
    plot_color = sm.RDP.sig_sim.dye_dict["dye_instance"].color
    sm.set_laser_parameters(wavelength = wavelength, waist = laser_waist)
    sm.set_noise_parameters(scale_noise = True, mean = noise_mean)
    for M2 in M2s:
        sm.set_laser_parameters(m_squared = M2)
        for method, stats in zip(['smc','local_baseline'],['poisson robust']*2):
            sm.set_calc_parameters(method = method, stats = stats, threshold_quantile = 5.5,\
                             normalisation = 60, single_count = True)
            for conc in [100]:#np.linspace(0,100,5):
                sm.set_dye_parameters(conc = conc)
                sm.reset_statistics()
                sm.take_measurements(10)
                slope = sm.RDP.sig_sim.nr_events_buffer.mean / 100
                noise_mean = sm.RDP.sig_sim.signal_df["noise"].mean()
                signal_mean = sm.RDP.sig_sim.signal_df["signal"].mean()
                sm.RDP.sig_sim.results_df.add_result(color = [plot_color],slope=slope, noise_mean=noise_mean, signal_mean=signal_mean)
                sm.plot_results(x_axis = 'm_squared', plot_color = "color")
sm.RDP.sig_sim.results_df.save("DE_vs_dye_m_squared.csv")

####################
# effect of  collimation lens position
####################
sm.reset_statistics()
sm.reset_results_df()
set_default_parameter()
dyes = ["Alexa Fluor 532", "Alexa Fluor 647", "Alexa Fluor 750"]
wavelengths = [520, 642,720]
laser_waists = [1.7, 1.2,2.5]
noise_means = [500000,250000,120000]
lens_positions = np.linspace(5.45,5.55,11)
for dye,wavelength, laser_waist, noise_mean in zip(dyes, wavelengths, laser_waists, noise_means):
    sm.set_dye_parameters(dye = dye, conc = 0)
    plot_color = sm.RDP.sig_sim.dye_dict["dye_instance"].color
    sm.set_laser_parameters( wavelength = wavelength, waist = laser_waist)
    sm.set_noise_parameters(scale_noise = True, mean = noise_mean)
    for lens_position in lens_positions:
        sm.set_optics_parameters(f_laser_collimation_dist = lens_position)
        for method, stats in zip(['smc','local_baseline'],['poisson robust']*2):
            sm.set_calc_parameters(method = method, stats = stats, threshold_quantile = 5.5,\
                             normalisation = 60, single_count = True)
            for conc in [100]:#np.linspace(0,100,5):
                sm.set_dye_parameters(conc = conc)
                sm.reset_statistics()
                sm.take_measurements(10)
                slope = sm.RDP.sig_sim.nr_events_buffer.mean / 100
                noise_mean = sm.RDP.sig_sim.signal_df["noise"].mean()
                signal_mean = sm.RDP.sig_sim.signal_df["signal"].mean()
                sm.RDP.sig_sim.results_df.add_result(color = [plot_color],slope=slope, noise_mean=noise_mean, signal_mean=signal_mean)
                sm.plot_results(x_axis = 'f_laser_collimation_dist', plot_color = "color")
sm.RDP.sig_sim.results_df.save("DE_vs_dye_col_lens_pos.csv")


#sm.logger.warning(sm.RDP.sig_sim.rdp_streamer.actual_concentration)
#print(pandas.__version__)
