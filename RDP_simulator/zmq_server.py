'''
Created on 30 May 2018

@author: CTCHguest

This is the server used for interprocess communication between the Counter software  and the RDP simulator. It is based on the ZMQ library.  
It receives messages from the Counter, saves the data in queue and sends the eldest data in the queue to the RDP simulator upon request. 
'''

import time
import numpy as np
import zmq
import struct
import sys
import os
from queue import Queue
import threading

#check if it is run from ipython and import clear_ouput if so. 
try:
    __IPYTHON__
except: 
    __IPYTHON__ = False
    
if __IPYTHON__:
    from IPython.display import clear_output
    
import zmqnumpy as zmqnp

from helper_functions import pydevBrk

#pydevBrk()

#init global variables
context = zmq.Context()

max_queue_size = 10000
data_queue = Queue(max_queue_size)
print_idx=0
running =True

#this is the print function mainly used for debug purposes
def _print(string):
    return #comment out to plot infos
    global print_idx
    if print_idx>10:
        if __IPYTHON__:
            clear_output()
        print_idx=0
    print(string)
    print_idx+=1

class Counter(threading.Thread):
    '''
    A class inheriting from threading.Thread for handling the messages from Counter
    '''
    def __init__(self,context,url):
        super(Counter,self).__init__()
        self.context = context
        self.socket = self.context.socket(zmq.PAIR)
        self.socket.bind(url)
        self.poller = zmq.Poller()
        self.poller.register(self.socket, zmq.POLLIN)
        self.msg=""
        
    def run(self):
        global running
        while running:
            socks = dict(self.poller.poll(1000))
            if self.socket in socks:
                self.msg=self.socket.recv_multipart()
                self.handle_msg(self.msg)

        
    def handle_msg(self,msg):
        array=zmqnp.msg_to_array(msg)
        # delete eldest segment if queue is full.. (otherwise put() below raises an exception)
        if data_queue.full() : data_queue.get()
        # add newest segment to queue
        data_queue.put(array) 
        _print("array received")
        _print("elements in queue: {:d}".format(data_queue.qsize()))
        
        

class RDP(threading.Thread):
    '''
    A class inheriting from threading.Thread for handling the messages from RDP simulator
    '''
    def __init__(self,context,url):
        super(RDP,self).__init__()
        self.context = context
        self.socket = self.context.socket(zmq.PAIR)
        self.socket.bind(url)
        self.poller = zmq.Poller()
        self.poller.register(self.socket, zmq.POLLIN)
        self.msg=""
        
    def run(self):
        global running
        while running:
            socks = dict(self.poller.poll(1000))
            if self.socket in socks:
                self.msg=self.socket.recv()
                self.handle_msg(self.msg)
    
    def handle_msg(self,msg):
        '''
        This function reads the message from the Counter controller. 
        depending on the message it eater
        "request": sends new data to RDP controller
        "flush": flushes the queue
        "close": shuts down the server 
        - 
        :param msg: message from RDP simulator
        :type msg: str, binary data
        '''
        global max_queue_size,data_queue,print_idx,_print,running
        if msg==b"request":
            if data_queue.empty():
                self.socket.send(b"")
                _print("RDP: empty message")
            else:
                self.socket.send_multipart(zmqnp.array_to_msg(data_queue.get()))
                _print("RDP: array sent")
        elif msg==b"flush":
            data_queue = Queue(max_queue_size)
        elif msg==b"close":
            running=False
        _print("elements in queue: {:d}".format(data_queue.qsize()))
        
if __name__ == "__main__":
    #The addresses of the sockets are hard-coded
    #but it might be easy to set via command line arguments
    print("zmq_server pid: {:d}:".format(os.getpid()))
    try:
        counter_thread=Counter(context,"tcp://127.0.0.1:5555")
        rdp_thread=RDP(context,"tcp://127.0.0.1:5556")
    except zmq.error.ZMQError: 
        #not possible to open socket. Address already in use.
        print("zmq error: shutting down server")
        sys.exit()
    counter_thread.start()
    rdp_thread.start()
    