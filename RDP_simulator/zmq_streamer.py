'''
This package provides a streamer class for zmq

It allows for receiving and sending numpy arrays via zmq. This is used together with the zmq_server.py script. 
Created on 23 Mai 2018

@author: P.Dietiker
'''



import os
import zmq
import logging
import subprocess

from RDP_simulator import zmqnumpy as zmqnp

class zmq_streamer(object):

    def __init__(self, address=None, logger=None,start_server = False):
        '''
        initialieses the streamer
        :param file_path: address
        :type file_path: string
        '''
        self.address = None
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.PAIR)
        #make the socket to time out after 10ms
        self.socket.setsockopt(zmq.RCVTIMEO, 10) 
        self.socket.setsockopt(zmq.LINGER, 0)
        self.connected = False
        self.server = None
    
        #start default server
        if start_server:
            self.start_server()

        # connect socket if file path is given
        self.connect_socket(address)

        # init logger
        if logger is None:
            self.logger = logging.getLogger(__name__)
            self.logger.handlers = []  # clear all handlers
            self.logger.setLevel(logging.DEBUG)
            logging_handler = logging.StreamHandler()
            logging_handler.setLevel(logging.DEBUG)
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            logging_handler.setFormatter(formatter)
            self.logger.addHandler(logging_handler)
        else:
            self.logger = logger

    def start_server(self):
        script_path=os.path.abspath(os.path.join(os.path.dirname(__file__),"zmq_server.py"))
        print (script_path)
        self.server=subprocess.Popen(
                ['python', script_path],stderr=subprocess.STDOUT)
        
    def connect_socket(self, address=None):
        '''
        Connect the socket
        :param address: address to server
        :type file_path: string
        '''
        if address is not None and address != self.address:
            if self.address is not None:
                self.socket.disconnect(self.address)
                self.connected = False
                self.address = None

        # connect the socket
            try:
                self.socket.connect(address)
            except zmq.ZMQError as e:
                self.logger.warning('zmq Error:'+str(e))
                return
            else:
                self.address = address
                self.connected = True


    def send(self, nparray):
        '''
        sends the given numpy array
        :param nparray: numpy array to send
        :type data: numpy array
        '''

        if self.connected:
            self.socket.send_multipart(zmqnp.array_to_msg(nparray))
        else:
            self.logger.warning('zmq streamer not connected')

    def receive(self):
        '''
        receive a numpy array via zmq
        If no message is available 0 is returned
        '''
        if self.connected:
            self.socket.send(b"request")
            try:
                msg=self.socket.recv_multipart()
                if len(msg)>1:
                    array=zmqnp.msg_to_array(msg)
                else:
                    array=0
            except zmq.Again:
                array = 0
        else:
            self.logger.warning('zmq streamer not connected')
            array = 0
        
        return array
    

    def flush(self):
        '''
        flush the zmq queue
        '''
        if self.connected:
            self.socket.send(b"flush")
        else:
            self.logger.warning('zmq streamer not connected')


    def __del__(self):
        self.socket.close()
        if self.server is not None:
            self.server.terminate()
            print("zmq_server terminated")
            self.server = None
            
