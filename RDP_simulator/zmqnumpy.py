'''
This is basically the zmqnumpy package but with some chenges to reflect my needs
'''

import numpy
import zmq
import functools
import uuid

def array_to_msg(nparray):
    """
    Convert a numpy ndarray to its multipart zeromq message representation.
    The return list is composed of:
      0. The string representation of the array element type, i.e. 'float32'
      1. The binary string representation of the shape of the array converted to a numpy array with dtype int32
      2. The binary string representation of the array
    These informations together can be used from the receiver code to recreate
    uniquely the original array.
    @param nparray: A numpy ndarray
    @type nparray: numpy.ndarray
    @rtype: list
    @return: [dtype, shape, array]
    """
    _shape = numpy.array(nparray.shape, dtype=numpy.int32).tostring()
    return [nparray.dtype.name.encode(), _shape, nparray.tostring()]

def msg_to_info(msg):
    msg[0] = msg[0]
    msg[1] = numpy.fromstring(msg[1], dtype=numpy.int32)
    msg[2] = msg[2]
    return msg

def msg_to_array(msg):
    """
    Parse a list argument as returned by L{array_to_msg} function of this
    module, and returns the numpy array contained in the message body.
    @param msg: a list as returned by L{array_to_msg} function
    @rtype: numpy.ndarray
    @return: The numpy array contained in the message
    """
    [_dtype, _shape, _bin_msg] = msg_to_info(msg)
    return numpy.fromstring(_bin_msg, dtype=_dtype).reshape(tuple(_shape))


