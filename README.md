# RDP simulator

This is the RDP simulator. It is a PyQT4 (PySide) application which allows for the simulation and evaluation of SMC data.

# Installation


## windows 


### install anaconda if not yet installed
from: <https://www.anaconda.com/>

#### create environment
open anaconda shell create environment with:
```bash
conda env create -f grifols.yml
```
#### install missing package
```bash
pip install obsub
pip install ./Pyqode/pqode.core
pip install ./Pyqode/pqode.qt
pip install ./Pyqode/pqode.python

```

### run DGA simulator
run simulator from anaconda shell with
```bash
conda activate Grifols
python run_simulator.py
```
