@ECHO OFF

SET PYTHON_PATH=%2
SET PYTHON_ACTIVATE=%3
SET PYTHON_ENVIRONMENT=%4
SET PYTHON=%5
SET VCVARSALL_PATH=%6
:: SET CYTHON_BUILD_EXT=%5

echo changing to %1
cd %1

echo loading vcvarsall.bat
SET DISTUTILS_USE_SDK=1
:: call "d:\Programmieren\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64 8.1

(
	call %VCVARSALL_PATH% amd64 8.1
) || (
	echo Failed to load vcvarsall!
	echo You need to have VisualStudio2015, for example Community Edition, with C++ installed
	echo or something similar which provides VC++ 14.0 vcvarsall.bat for this to work
	exit -1
)

(
	echo activating python environment %PYTHON_ENVIRONMENT%
	call "%PYTHON_PATH%\%PYTHON_ACTIVATE%" %PYTHON_ENVIRONMENT%
) || (
	echo Failed to activate python environment!
	echo - Run Anaconda Prompt 
	echo - Install environment using 'conda env create -f environment3.7.yml'
	exit -1
)
echo running python script '%PYTHON% %7 %8 %9'
(
	:: "%PYTHON_PATH%\%PYTHON%" %CYTHON_BUILD_EXT%
	"%PYTHON%"  %7 %8 %9
) || (
	echo Failed to run python script!
	exit -1
)
exit
