'''
Created on 25.01.2021

@author: peterdietiker
'''



import numpy as np
#import matplotlib.pyplot as plt
#import matplotlib
import sympy as sp
import sympy.physics.optics as op
import pandas as pd

#matplotlib.use('TkAgg')

from guietta import  Gui, Quit,VS, HS, _,___,III, M, Exceptions


from dye import Dye


z,r = sp.symbols('z r',real=True)


def gaussian_intensity(P0,p,z,r):
    '''
    Intensity of a TEM00 Gaussian beam as a fuction of beam parameter and position within the beam
    Parameters:
    ----------
    P0: total Power in the Beam in W
    p:  Sympy.physics.optics.BeamParameter instance
    z: axial postion relative to waist in meters
    r: lateral position to waist in meters
    '''
    return 2*P0/(np.pi*p.w_0**2)#*((p.w_0/p.w)**2*sp.exp(-2*r**2/p.w**2))

def calc_beams():
    '''
    Calculates the beam as function of the set parameters
    '''
    f_objective = optics_dict["f_objective"]
    f_laser_collimation = optics_dict["f_laser_collimation"]
    f_laser_collimation_dist = optics_dict["f_laser_collimation_dist"]
    M_squared = laser_dict["m_squared"]
    wavelength = laser_dict["wavelength"]
    laser_beam_waist = laser_dict["laser_beam_waist"]
    laser_to_objective_dist = optics_dict["laser_to_objective_dist"]
    n_COP = 1.55 #Brechungsindex der Wellplate
    n_sample = 1.4



    #Laser aus der Faser               
    p_laser = op.BeamParameter(wavelength*M_squared, 0, w=laser_beam_waist)
    #Kollimierter Strahl
    P_collimated = op.ThinLens(f_laser_collimation)*op.FreeSpace(f_laser_collimation_dist)*p_laser
    #Lasersrahl im fokus des Objektivs
    p_sample = op.FreeSpace(f_objective)*op.ThinLens(f_objective)*op.FreeSpace(laser_to_objective_dist)*P_collimated


    result_dict["excitation_beam_waist"] = float(p_sample.w_0.subs(z,0).n())
    result_dict["excitation_rayleigh_length"] = float(p_sample.z_r.subs(z,0).n())#das muss ein numerischer Fehler sein, dass ich error auf 0 setzen muss
    result_dict["excitation_divergence"] = float(p_sample.divergence.subs(z,0).n())
    return p_sample


def get_intensities(power):
    p_sample = calc_beams()
    intensity_0 = gaussian_intensity(1,p_sample,0,0).n()
    intensity = power*intensity_0
    return intensity
    
def tick_function(X):
        #I = X/1000/(waist**2*3.141)/1E12*1000
        I = get_intensities(X)/1000/1E12*1000
        return ["%.1f" % z for z in I]
    

Alexa405 = Dye("Alexa Fluor 405",data_path = r"V:\Projects\71045-01_Grifols_Feasbility build\200_Engineering\220_Optic_Engineering\Documents")    
Alexa532 = Dye("Alexa Fluor 532",data_path = r"V:\Projects\71045-01_Grifols_Feasbility build\200_Engineering\220_Optic_Engineering\Documents")    
Alexa647 = Dye("Alexa Fluor 647",data_path = r"V:\Projects\71045-01_Grifols_Feasbility build\200_Engineering\220_Optic_Engineering\Documents")    
Alexa750 = Dye("Alexa Fluor 750",data_path = r"V:\Projects\71045-01_Grifols_Feasbility build\200_Engineering\220_Optic_Engineering\Documents")    


optics_dict = {"f_objective" : 3E-3, "f_laser_collimation" : 5.5E-3, "f_laser_collimation_dist":5.5E-3,"laser_to_objective_dist":0.4}
laser_dict = {"laser_beam_waist":1.55E-6,"m_squared" : 1,"wavelength": 650E-9}
wl_dict = {'blue':405, 'green':532, 'red': 647,'NIR':730}
result_dict = {}

powers = np.linspace(0.001,20,100)/1000
intensities = get_intensities(powers)



def create_figure(gui):
    #fig, ax = plt.subplots(constrained_layout=True,figsize=(6, 4))
    ax = gui.plot.ax 
    ax_I = ax.twiny()
    for dye,wl in zip([Alexa405,Alexa532,Alexa647,Alexa750],[wl_dict[channel] for channel in ['blue', 'green', 'red','NIR']]):
        laser_dict['wavelength'] = wl*1E-9
        intensities = get_intensities(powers)
        count_rate = dye.calc_emission_rate(intensities,wl)
        ax.plot(powers*1000,count_rate,color = dye.color,label = dye.name)
        try:
            if ax.get_ylim()[1]< count_rate.max():
                ax.set_ylim(ax.get_ylim()[0],count_rate.max())
        except:
            pass
    ax.get_yaxis().get_major_formatter().set_scientific(False)
    ax.set_xlabel("Laser power / mW")
    ax.set_ylabel("emission rate / photon s$^{-1}$")
    ax.legend()
    
    new_tick_locations = ax.get_xticks()[1:-1]
    #ax_I.set_xlim(axs[0].get_xlim())
    ax_I.set_xbound(ax.get_xbound())
    ax_I.set_xticks(new_tick_locations)
    ax_I.set_xticklabels(tick_function(new_tick_locations))
    ax_I.set_xlabel(r"Excitation intensity / $\rm{mW}\mu \rm{m}^{-2}$")
    ax.figure.tight_layout()
    #return fig,ax


# In[133]:


def update_plot(gui, value):
    ax = gui.plot.ax.figure.axes[0]
    ax_I = gui.plot.ax.figure.axes[1]
    for dye,wl,line in zip([Alexa405,Alexa532,Alexa647,Alexa750],[wl_dict[channel] for channel in ['blue', 'green', 'red','NIR']],ax.lines):
        laser_dict['wavelength'] = wl*1E-9
        intensities = get_intensities(powers)
        count_rate = dye.calc_emission_rate(intensities,wl)
        line.set_ydata(count_rate)
        try:
            if ax.get_ylim()[1] < count_rate.max():
                ax.set_ylim(0,float(count_rate.max()))
        except:
            raise
    new_tick_locations = ax.get_xticks()[1:-1]
    #ax_I.set_xlim(axs[0].get_xlim())
    ax_I.set_xbound(ax.get_xbound())
    ax_I.set_xticks(new_tick_locations)
    ax_I.set_xticklabels(tick_function(new_tick_locations))
    ax.figure.tight_layout()
    ax.figure.canvas.draw()
    gui.beam_waist_value = f"{(result_dict['excitation_beam_waist']*1E6):.2f}"
    
    


# In[134]:


def update_optics_dict(f_objective = None, f_laser_collimation = None, f_laser_collimation_dist = None,laser_to_objective_dist= None):
    update_dict = locals()
    for key in list(update_dict.keys()):
        if update_dict[key] is None:
            del update_dict[key]
    optics_dict.update(update_dict)
def update_laser_dict(laser_beam_waist = None,m_squared = None,wavelength = None):
    update_dict = locals()
    for key in list(update_dict.keys()):
        if update_dict[key] is None:
            del update_dict[key]
    laser_dict.update(update_dict)
        


# In[135]:


def on_f_objective(gui,*args):
    value = gui.f_objective_value
    try: #might be an empty string
        value = float(value)
    except ValueError:
        return
    if value > 0:
        update_optics_dict(f_objective = value/1000)
        #gui.f_objective_value = value
        update_plot(gui,value)
    else:
        raise ValueError("Value must be larger that 0")
def on_f_collimation(gui, *args):
    value = gui.f_laser_collimation_value
    try:
        value = float(value)
    except ValueError:
        return
    if value > 0:
        update_optics_dict(f_laser_collimation = value/1000,f_laser_collimation_dist = value /1000)
        #gui.f_laser_collimation_value = value
        update_plot(gui,value)
    else:
        raise ValueError("Value must be larger that 0")
def on_laser_waist(gui, *args):
    value = gui.laser_beam_waist_value
    try:
        value = float(value)
    except ValueError:
        return
    if value > 0:
        update_laser_dict(laser_beam_waist = value/1E6) 
        #gui.laser_beam_waist_value = value/10
        update_plot(gui,value)
    else:
        raise ValueError("Value must be larger that 0")
def on_M2(gui, *args):
    value = gui.m_squared_value
    try:
        value = float(value)
    except ValueError:
        return
    if value > 0:
        update_laser_dict(m_squared = value)
        #gui.m_squared_value = value/20
        update_plot(gui,value)
    else:
        raise ValueError("Value must be larger that 0")

def on_WL(gui, *args):
    for elem, channel, in zip([gui.Wavelength_blue_value,gui.Wavelength_green_value,gui.Wavelength_red_value,gui.Wavelength_NIR_value],['blue', 'green', 'red','NIR']):
        value = elem
        try:
            value = float(value)
        except ValueError:
            return
        if value > 0:
            wl_dict[channel]=value
            #gui.m_squared_value = value/20
            
        else:
            raise ValueError("Value must be larger that 0")
    update_plot(gui,value)

    


# In[146]:


#===============================================================================
# f_slider = sg.Slider(range=(1,20),
#          default_value=optics_dict['f_objective']*1000,
#          size=(20,15),
#          orientation='horizontal',
#          font=('Helvetica', 12))
#===============================================================================
# output = widgets.Output()
# f_slider = widgets.FloatSlider(value=optics_dict['f_objective']*1000, min=1, max=20, description='f_obj / mm')
# f_slider.observe(on_f_objective,'value')
# coll_slider = widgets.FloatSlider(value=optics_dict['f_laser_collimation']*1000, min=1, max=20, description='f_coll / mm')
# coll_slider.observe(on_f_collimation, 'value')
# waist_slider = widgets.FloatSlider(value=laser_dict['laser_beam_waist']*1E6, min=0.5, max=5, description='laser_waist')
# waist_slider.observe(on_laser_waist, 'value')
# M2_slider = widgets.FloatSlider(value=laser_dict['m_squared'], min=1, max=5, description='M$^2$')
# M2_slider.observe(on_M2, 'value')
# waist_text = widgets.Text(value=f'{(result_dict["excitation_beam_waist"]*1E6):.2f}', description='waist / um',disabled=False)
#===============================================================================


#===============================================================================
# gui = Gui(
# 
#   [  'f objective (mm)',        ___,             M('plot'), ___ ,  ___ ],
#   [  HS('f_objective'),         'f_objective_value',  III , III ,  III ],
#   [  'f collimation (mm)',      ___,            III , III ,  III ],
#   [  HS('f_laser_collimation'), 'f_laser_collimation_value', III ,  III ,     III      ],
#   [  'f laser waist (um)',      ___,            III , III ,  III ],
#   [  HS('laser_beam_waist'),'laser_beam_waist_value' ,III ,  III ,     III      ],
#   [  'M^2',                     ___,            III , III ,  III ],
#   [  HS('m_squared'),       'm_squared_value', III ,  III ,     III],
#  )
#===============================================================================

gui = Gui(

  [  'f objective (mm)',                M('plot'),  ___   , ___ ,   ___ ],
  [  '__f_objective_value__',           III ,       III ,   III ,   III ],
  [  'f collimation (mm)',              III,        III ,   III ,   III ],
  [  '__f_laser_collimation_value__',   III ,       III ,   III,    III ],
  [  'f laser waist (um)',              III,        III ,   III ,   III ],
  [  '__laser_beam_waist_value__' ,     III,        III ,   III ,   III ],
  [  'M^2',                             III,        III ,   III ,   III ],
  [  '__m_squared_value__',             III,        III ,   III ,   III ],
  [  'Wavelength blue (nm)',            III,        III ,   III ,   III ],
  [  '__Wavelength_blue_value__',       III,        III ,   III ,   III ],
  [  'Wavelength green (nm)',           III,        III ,   III ,   III ],
  [  '__Wavelength_green_value__',      III,        III ,   III ,   III ],
  [  'Wavelength red (nm)',             III,        III ,   III ,   III ],
  [  '__Wavelength_red_value__',        III,        III ,   III ,   III ],
  [  'Wavelength NIR (nm)',             III,        III ,   III ,   III ],
  [  '__Wavelength_NIR_value__',        III,        III ,   III ,   III ],
  [  _,                                 'Beam waist (um)',        '__beam_waist_value__' ,   _ ,   _ ],
  exceptions = Exceptions.POPUP
 )
gui.events(

    [  _             ,      _,  _ , _ ,   _     ],
    [  ('editingFinished',on_f_objective),      _,  _ , _ ,   _     ],
    [  _             ,      _,  _ , _ ,   _     ],
    [  ('editingFinished',on_f_collimation),    _,  _ , _ ,   _     ],
    [  _             ,      _,  _ , _ ,   _     ],
    [  ('editingFinished',on_laser_waist),      _,  _ , _ ,   _     ],
    [  _             ,      _,  _ , _ ,   _     ],
    [  ('editingFinished',on_M2)           ,    _,  _ , _ ,   _     ],
    [  _             ,      _,  _ , _ ,   _     ],
    [  ('editingFinished',on_WL)           ,    _,  _ , _ ,   _     ],
    [  _             ,      _,  _ , _ ,   _     ],
    [  ('editingFinished',on_WL)           ,    _,  _ , _ ,   _     ],
    [  _             ,      _,  _ , _ ,   _     ],
    [  ('editingFinished',on_WL)           ,    _,  _ , _ ,   _     ],
    [  _             ,      _,  _ , _ ,   _     ],
    [  ('editingFinished',on_WL)           ,    _,  _ , _ ,   _     ], )

create_figure(gui)

optics_dict = {"f_objective" : 3E-3, "f_laser_collimation" : 5.5E-3, "f_laser_collimation_dist":5.5E-3,"laser_to_objective_dist":0.4}
laser_dict = {"laser_beam_waist":1.55E-6,"m_squared" : 1,"wavelength": 650E-9}
gui.f_objective_value = optics_dict['f_objective'] * 1000
#gui.f_objective = optics_dict['f_objective'] * 1000
gui.f_laser_collimation_value = optics_dict['f_laser_collimation']*1000
#gui.f_laser_collimation = optics_dict['f_laser_collimation'] * 1000 * 10
gui.laser_beam_waist_value = laser_dict['laser_beam_waist'] *1E6
#gui.laser_beam_waist = laser_dict['laser_beam_waist']* 1E6*10
gui.m_squared_value = laser_dict['m_squared']
#gui.m_squared = laser_dict['m_squared'] / 20
gui.Wavelength_blue_value = wl_dict['blue']
gui.Wavelength_green_value = wl_dict['green']
gui.Wavelength_red_value = wl_dict['red']
gui.Wavelength_NIR_value = wl_dict['NIR']

gui.beam_waist_value = f"{(result_dict['excitation_beam_waist']*1E6):.2f}"

gui.run()
