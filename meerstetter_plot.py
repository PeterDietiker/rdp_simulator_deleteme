# -*- coding: utf-8 -*-
"""
Demonstrates use of PlotWidget class. This is little more than a 
GraphicsView with a PlotItem placed in its center.
"""

from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
import sys
sys.path.append(r"C:\Daten\Python\MeComAPI")
from PyMeCom import TEC
import time

tec = TEC(address=2)
tec.connect()


#QtGui.QApplication.setGraphicsSystem('raster')
app = QtGui.QApplication([])
mw = QtGui.QMainWindow()
mw.setWindowTitle('Heatsink Temperature')
mw.resize(1000,300)
cw = QtGui.QWidget()
mw.setCentralWidget(cw)
l = QtGui.QVBoxLayout()
cw.setLayout(l)

pg.setConfigOption('background', 'w')
pw = pg.PlotWidget(name='Heatsink')  
l.addWidget(pw)

mw.show()

## Create an empty plot curve to be filled later, set its pen
p1 = pw.plot()
p1.setPen((0,0,0))


pw.setLabel('left', 'Temp', units='°C')
pw.setLabel('bottom', 'Time', units='s')
pw.setXRange(0, 10*60)
pw.setYRange(21.8, 23.2)

data = np.empty((0,2))
start_time = time.time()
def updateData():
    global data
    temp = tec.get_temp()
    t = time.time()-start_time
    data = np.vstack((data,np.array([t,temp])))
    p1.setData(y=data[:,1], x=data[:,0])

up = True
def updateTemp():
    global up
    if up:
        new_temp = 23
    else:
        new_temp = 22
    tec.set_target_temp(new_temp)
    up = not up
## Start a timer to rapidly update the plot in pw
t = QtCore.QTimer()
t.timeout.connect(updateData)
t.start(1000)
#updateData()
t1 = QtCore.QTimer()
t1.timeout.connect(updateTemp)
t1.start(5*60*1000)

t2 = QtCore.QTimer.singleShot(20*1000,updateTemp)


## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
