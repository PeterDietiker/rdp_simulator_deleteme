# NOTE: For msys2 you need to define target=mingw64 yourself!

ifndef target
	ifeq ($(OS),Windows_NT)
		ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
			target:=win64
		endif
		ifeq ($(PROCESSOR_ARCHITECTURE),x86)
			target:=win32
		endif
	else
		UNAME_S := $(shell uname -s)
		ifeq ($(UNAME_S),Linux)
			_TARGET:=linux
		endif
		ifeq ($(UNAME_S),Darwin)
			_TARGET:=osx
		endif
		UNAME_P := $(shell uname -p)
		ifeq ($(UNAME_P),x86_64)
			target:=$(_TARGET)64
		endif
		ifneq ($(filter %86,$(UNAME_P)),)
			target:=$(_TARGET)32
		endif
		ifneq ($(filter arm%,$(UNAME_P)),)
			target:=arm_$(_TARGET)
		endif
		undefine _TARGET
	endif
endif
