import os
import sys

import RDP_simulator.rdp_simulator

simulator_path = os.path.join(os.getcwd(),"RDP_simulator")
os.chdir(simulator_path)
print("python version: "+str(sys.version_info))
RDP_simulator.rdp_simulator.run()
